#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 13:44:49 2021

@author: malte
"""
import h5py
import matplotlib.pyplot as plt
import pandas as pd
plt.close('all')
import sys
# sys.path.insert(1, '../invmfit/')
# from peakfit_Zee import invMass
from tqdm import tqdm
import time
import numpy as np
import json
import tensorflow as tf
from tensorflow import keras
from keras import layers
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import keras as ks
from glob import glob
from os import listdir
from os.path import isfile, join
import os
from keras.models import Sequential
from keras.layers import (LSTM, Dense, TimeDistributed, SimpleRNN,
                          Embedding, Conv2D, BatchNormalization, Input, Masking,
                          Flatten, ReLU, Multiply, LeakyReLU)
# from keras.utils import to_categorical
import argparse
import numpy as np

class generator():
    def __init__(self, data, multiple_with, truth=[], zero_columns=None):
        self.data = data
        self.truth = truth
        self.multiple_with=multiple_with
        self.zero_columns = zero_columns
        
    def __call__(self):
        if len(self.truth) != 0:
            for d, j, i in zip(self.data, self.truth, self.multiple_with):
                j = np.array(j)
                i = np.array(i)
                j.shape = (1,)
                i.shape = (1,)
                d = np.array(d)
                # d.shape = (1, shape)
                if self.zero_columns >= 0:
                    d[:,self.zero_columns] = 0
                print('shape', j.shape, d.shape, i.shape)
                yield (d, i), j
        else:
            for d, i in zip(self.data, self.multiple_with):
                d = np.array(d)
                d.shape = (d.shape[0], shape)
                i = np.array(i)
                i.shape = (1,)
                if self.zero_columns != None:
                    d[:,self.zero_columns] = 0
                # d.shape = (None, shape)
                yield (d, i)
if __name__ == '__main__':
    from importlib import import_module
    from submodels import get_scalar_net

    parser = argparse.ArgumentParser()
    parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='-1', type=str)
    args = parser.parse_args()
    gpu_ids = args.gpu
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu_ids)
    
    scaler = StandardScaler()
    new_data = '../root2hdf5/output/root2hdf5/Zee/Zee_mc_time_gain_combine/small_Zee_mc_image_1611693005.3976343.h5'
    df = h5py.File(new_data, "r")
    particle = 'electrons' if 'ee' in new_data else 'photons'
    data_conf = import_module(f'..{particle}_variables_conf',  'variables_params.subpkg')
    data_params = data_conf.get_params()
    param_conf = import_module('..param_conf', 'variables_params.subpkg')
    params = param_conf.get_params()
    params['scalar_net'].pop('connect_to', None)
    # sys.exit()
    names = data_params['scalar_names']
    n=None
    rows = 10
    #%%
    def import_data(*,set_name, n):
        data=pd.DataFrame()
        p_truth_e = df[set_name]['p_truth_e'][:n]
        p_e = df[set_name]['p_e'][:n]
        mask_remove_bad_events = p_truth_e != -999 # properly no events
        p_truth_e = p_truth_e[mask_remove_bad_events]
        p_e = p_e[mask_remove_bad_events]
        ratio = p_e/p_truth_e   
        for i in names:
            d = df[set_name][i][:n]
            d = (d-np.mean(np.hstack(d)))/np.std(np.hstack(d))
            data[i] = d
        # preprocessing removing bad event 
        data = data.values[mask_remove_bad_events]
        mask_bad_ratio = ratio<5  # p_e/truth
        return data[mask_bad_ratio], ratio[mask_bad_ratio], p_truth_e[mask_bad_ratio], p_e[mask_bad_ratio]
    #%%
    if True:
        train_data, train_ratio, train_truth, train_p_e = import_data(set_name='train', n=n)
        val_data, val_ratio, val_truth, val_p_e = import_data(set_name='val', n=n)
        test_data, test_ratio, test_truth, test_p_e = import_data(set_name='test', n=n)
        
        #%%
        # =============================================================================
        # Model HUSK AT FJENE -999
        # =============================================================================
        shape = len(names) # all_data.shape[1]
    
    
        #%%
        checkpoint_filepath = f'models/{time.time()}/'
        names = [None]+names # +names
        for nr, name in enumerate(names):
            print(name)

            performance = {}
            folder_path = checkpoint_filepath + f'{name}/'
            # model_in = Input(shape=(rows, shape, 1), dtype='float32')
            model_in_multiply = Input(shape=(None, ), dtype='float32',
                                      name='multiple_with')
            
            model = get_scalar_net(input_shape = train_data.shape[1:], 
                                   **params['scalar_net'],
                                   multiple_with = model_in_multiply)
            model.summary()
            # sys.exit()
            model.compile(loss='mean_absolute_error',
                          metrics=['logcosh'],
                          optimizer='Nadam')
            model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(filepath=folder_path,#+'/{epoch:04d}-{val_loss:.4f}',
                                                                           save_best_only=True,
                                                                           save_weights_only=True,
                                                                           monitor='val_loss',
                                                                           )
            earlystopping = ks.callbacks.EarlyStopping(min_delta=0.005, patience=20)
            
            #%%
            if True:
                # sys.exit()
                try:
                    model.fit(x = [train_data, train_p_e], y =train_truth , validation_data=([val_data, val_p_e], val_truth),epochs=100,
                                        verbose=1, callbacks=[model_checkpoint_callback, earlystopping],
                                        batch_size=2048)
                except KeyboardInterrupt:
                    print('Testing!')
                    sys.exit()
                #%%
                # folders = os.listdir(checkpoint_filepath)
                # best_model = np.argmin([float(folder.split('-')[1]) for folder in folders])
                model.load_weights(folder_path)
                pred=[]
                # for i in test_generator:
                pred.append(model.predict([test_data, test_p_e]))
                pred=np.ravel(pred)
                pred = np.vstack(pred).T
                print('p_e:', np.mean(np.abs(test_p_e-test_truth)))
                print('DeepCalo', np.mean(np.abs(pred-test_truth)))
                out_file = open(f"{folder_path}scalar_performance_{name}.json", "w") 
          
                performance['name'] = name
                performance['DeepCalo'] = str(np.mean(np.abs(pred-test_truth)))
                performance['p_e'] = str(np.mean(np.abs(test_p_e-test_truth)))
                print(performance)
                json.dump(performance, out_file, indent = 6) 
                out_file.close() 
                
            else:
                print('')
                for i,_ in train_generator:
                    i= i[0].numpy()
    else:
        #%%
        mypath = 'track_net_models/1621506539.476215/*/performance*'
        onlyfiles = glob(mypath)
        data =[]
        for i in onlyfiles:
            with open(i) as f:
                data.append(json.load(f))
        name=[]
        MAE=[]
        for i in data:
            if i['name'] == None:
                benchmark = float(i['MAE after'])
            else:
                name.append(i['name'])
                MAE.append(float(i['MAE after']))
        plt.plot(name, MAE, '*', label='Varying model')
        plt.plot(name, np.ones(len(name))*benchmark, 'b--', label='Benchmark')
        plt.plot(name, np.ones(len(name))*float(i['MAE before']), 'g--', label='p_e')
        plt.legend()
