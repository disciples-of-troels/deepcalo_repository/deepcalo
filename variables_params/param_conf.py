def get_params():
    """
    Returns a dictionary containing parameters to be passed to the model
    container.

    Please see the README for documentation.
    """
    BWxCB_fit = False
    step = 100_000 # Zee_mc: 3000, Zmumugam_mc: 800
    if BWxCB_fit:
        batch = int(512*11) # 11
    else:
        batch = 2048
    params = {
          # Training
          'n_train'                    : 21_000_000, # maximum of 50 files
          'n_val'                      : 4_000_000, # maximum of 50 files
          'n_test'                     : 100, #8700
          'epochs'                     : 1000, #########################################
          'batch_size'                 : batch,
          'cut_ratio'                  : 0.8, ########################################
          'ext_energy_range'           : 45, ########################################
          'loss'                       : 'logcosh', #'logcosh', cls : binary_crossentropy
          'metrics'                    : ['mae'], #cls: accuracy
          'optimizer'                  : 'Nadam',
          'lr_finder'                  : {'use':False,   #########################
                                          'scan_range':[1e-5, 1e0],
                                          'epochs':5,
                                          'Number_of_files':2,
                                          'prompt_for_input':False, 
                                          'scale': 'linear'},
          'lr_schedule'                : {'name':'OneCycle', # CLR OneCycle
                                          'range':[5e-4, 7e-2], # [3e-5, 1e-3]
                                          'step_size_factor': step,
                                          'kwargs': {
                                                # 'mode': 'exp_range',
                                                # 'gamma': 0.95,
                                               }
                                              },
          'auto_lr'                    : True,
          'profiler'                   : False,

          # Misc.
          'data_generator'            : { 
                                          'use':True, # really not using a datagen any more
                                          'filesize': [-1,-1], #[[0.1, 0.1], [0.1, 0.1]], #[[1,1], [1,1]],#[-1, -1], # ########################## [-1, 10] #Zee [[0.2,0.2], [1,1]]
                                          'n_workers':10,
                                          'max_queue_size':10
                                           },

          'use_earlystopping'          : {'use': True,
                                          'patience': 50,
                                          'delta': 0.005},
          'restore_best_weights'       : False,

          'pretrained_model'           : {'use':False,
                                          'weights_path':'./logs/1/Zee_mc_2021-05-11T13:12:58.243724_100_epochs_w_ECAL_FiLM_track_time/saved_models/0/weights.0007-2.9393.hdf5',
                                          'params_path':'./logs/1/Zee_mc_2021-05-11T13:12:58.243724_100_epochs_w_ECAL_FiLM_track_time/hyperparams_0.pkl',
                                          'layers_to_load':['top', 'cnn', 'FiLM_generator', 'scalar_net', 'tracks'],
                                          'freeze_loaded_layers':False},


          'upsampling'                 : {'use':True,
                                          'wanted_size':(56,55)},

          # Submodels
          'track_net'                  : {'use': True,  ######################
                                          'output_size': 32, 
                                          'connect_to': 'scalar_net'}, 
          
          'top'                        : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[256,256,1],
                                          'final_activation':'relu', ##### relu regression - sigmoid classification
                                          'probability_layer' : False,
                                          'BWxCB_fit': BWxCB_fit}, ################################
          'cnn'                        : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'block_depths':[1,2,2,2,2],
                                          'n_init_filters':16,
                                          'downsampling':'maxpool',
                                          'min_size_for_downsampling':6},
          'scalar_net'                 : {'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[256],
                                          'connect_to':['FiLM_gen', 'top']}, # ['FiLM_gen', 'top']
          'gate_net'                   : {
                                          'initialization':'orthogonal',
                                          'activation':None,
                                          'normalization':None,
                                          'layer_reg':{},
                                          'dropout':None,
                                          'units':[],
                                          'use_res':False,
                                          'final_activation':None,
                                          'final_activation_init':[1.0],
                                          'connect_to':'concatenate' # concatenate  multiply Before called merge_method
                                          },
          'FiLM_gen'                   : {'use':True,
                                          'activation':'leakyrelu',
                                          'normalization':'batch',
                                          'units':[512,1024]}, # 512,1024
          }

    return params
