def get_params():
    """
    Returns a dictionary containing parameters to be passed to the loading
    function.
    """

    params = {'target_name'            : 'p_truth_e',
              'img_names'              : ['em_barrel'], # NOTE: Should actually be the added barrel and endcap layers!
              'gate_img_prefix'        : ['time_em_barrel', 'gain_em_barrel'],
              'scalar_names'           : [ # Set to None if you don't want to use scalars
                                          'p_eAccCluster',
                                          'p_cellIndexCluster',
                                          'p_f0Cluster',
                                          'p_R12',
                                          'p_eta',
                                          'p_etaModCalo',
                                          'NvtxReco',
                                          'averageInteractionsPerCrossing',
                                          # 'p_poscs2', # zero for photons
                                          'p_dPhiTH3',
                                          'p_fTG3',
                                          'tile_gap_Lr1'
                                         ],
            'track_names'            : [ # Set to None if you don't want to use tracks.
                                        # It is recommended to also add p/q and d0/sigma_d0
                                         'tracks_dR', 'tracks_pt', 'tracks_d0',
                                         # 'tracks_sigmad0',
                                         'tracks_trthits',
                                         'tracks_vertex', 'tracks_theta',
                                         'tracks_z0',
                                         # 'tracks_charge',
                                         'tracks_eta', 'tracks_phi',
                                         'tracks_pixhits', 'tracks_scthits'
                                        ],
              'max_tracks'             : None,
              'multiply_output_name'   : 'p_eAccCluster', #'p_eAccCluster',
              'sample_weight_name'     : None,
              'additional_info'        : True,
              }

    return params
