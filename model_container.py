import os
import sys
import copy
import h5py
import traceback
import argparse
import numpy as np
from glob import glob
import pickle
import pandas as pd
from skhep.math import vectors
import tensorflow as tf
# tf.executing_eagerly()
import keras as ks
# from tensorflow import keras as ks
import keras.backend as K
import matplotlib.pyplot as plt
import matplotlib
from keras.callbacks import LearningRateScheduler

# from tensorflow.contrib.compiler import xla # need to be added at some point
import tensorflow_model_optimization as tfmot
# from keras.mixed_precision import experimental as mixed_precision
from tensorboard.plugins.hparams import api as hp
from keras.losses import Loss

# Internal imports
from .utils import save_dict, query_yes_no, drop_key, boolify
from .model_components import get_loss_function, get_optimizer, upsample_img, flatten_tns
from .submodels import get_gate_net, get_cnn, get_top, get_network_in_network, get_scalar_net, get_track_net, get_FiLM_generator
# from .data_generator import DataPipeline, generator
from .callbacks import ModelCheckpoint, LRFinder, SGDRScheduler, CyclicLR, OneCycleLR
from .tfrecord_load_data import load_dataset, clean_batch
from .track_net import track_net
np.set_printoptions(threshold=sys.maxsize)

matplotlib.rcParams.update( #for lr finder
    {
        'text.usetex': False,
        'font.family': 'stixgeneral',
        'mathtext.fontset': 'stix',
    }
)   

@tf.function()
def take_data(train):
    return [i[0] for i in train.take(1)][0]

class ModelContainer:
    """
    A class for organizing the creation, training and evaluation of models.

    Please see the [README](https://gitlab.com/ffaye/deepcalo/blob/master/README.md)
    for documentation.
    """
    def __init__(self, data, params, dirs, data_path, nr_of_cores, data_params,
                 save_figs=True,verbose=True, hparams=None, datatype=None,
                 train_lr_finder=None,session_num=''):
        self.nr_of_cores = nr_of_cores
        self.train, self.val = data
        self.session_num = session_num
        # print(tf.__file__)
        self.data = [i[0] for i in self.train.take(1)][0] #take_data(self.train)
        # print(self.data)
        self.datatype = datatype
        self.params = params
        self.dirs = dirs
        self.save_figs = save_figs
        self.verbose = 0 #verbose
        self.data_path=data_path
        self.hparams = hparams
        self.data_params = data_params
        self.train_lr_finder = train_lr_finder
        assert (self.params['top']['probability_layer'])+(self.params['top']['BWxCB_fit']) != 2, "Both probability_layer and BWxCB_fit are actived"
        if isinstance(data_path, list):
            data_path =data_path[0]
        if data_path.split('/')[-1] == '':
            tag = data_path.split('/')[-2]
        else:
            tag = data_path.split('/')[-1]   
            
        self.channel_tag = 'Zee' if 'Zee' in tag else 'Zmumugam' if 'Zmumugam' in tag else 'Hyy'
        self.tag = 'electron' if 'Zee' in tag else 'photon'
        # Load specific layers of previously trained model
        if self.params['pretrained_model']['use']:
            self._collect_pretrained_weights(**drop_key(self.params['pretrained_model'],'use'))

        if os.getcwd() == '/home/malte/hep':
            os.chdir('./gpulab/work/Master/run/train_model_malteal')
            
        # # Apply mixed precision OP_REQUIRES failed at summary_kernels.cc:242 : Invalid argument: Infinity in summary histogram for: batch_normalization_5/moving_mean_0
        if False:#self.hparams != None:
            print('%'*20)
            policy = mixed_precision.Policy('mixed_float16')
            mixed_precision.set_policy(policy)
            print('Appling mixed precision doing hyperparameter optimization')
            print('Compute dtype: %s' % policy.compute_dtype)
            print('Variable dtype: %s' % policy.variable_dtype)
            print('%'*20)
        
        # # XLA 
        tf.config.optimizer.set_jit(True)
        tf.config.experimental.enable_tensor_float_32_execution(False)
        tf.keras.backend.set_floatx('float64')
        # tf.enable_eager_execution()
        # Create model
        if (self.params['n_gpus'] > 1):
            # gpus = tf.config.experimental.list_physical_devices('GPU')
            gpus = tf.config.experimental.list_physical_devices('GPU')
            for gpu in gpus:
              tf.config.experimental.set_memory_growth(gpu, True)

            strategy = tf.distribute.MirroredStrategy() # tf.distribute.MirroredStrategy(devices=["/gpu:0", "/gpu:1"]) MultiWorkerMirroredStrategy
            print('#'*5,' Number of devices: {}'.format(strategy.num_replicas_in_sync), '#'*5)
                
            with strategy.scope():
                self.model = self.get_model(self.params)
                # Plot the models
                self._plot_models()
                self.train_model()

        else:
            self.model = self.get_model(self.params)
            # Plot the models
            self._plot_models()
            self.train_model()

        try:
            if save_figs:
                self._plot_models()
        except Exception:
            if self.verbose:
                print('Plot of model failed. Continuing. '
                      'Here is the traceback:\n')
                traceback.print_exc()


    def get_model(self, params):

        self.gate_img_prefix = 'gate'

        # Creating inputs dimensions 
        inputs = {}

        # Images
        self._non_gate_img_names = []
        for name in self.data:
            # if name in self.data and boolify(self.data['em_barrel'].numpy()):
            dimensions = self.data[name].shape[1:]
            if dimensions == ():
                dimensions = (1,)
            inputs[name] = ks.layers.Input(shape=dimensions, name=name) # , dtype = self.data[name].dtype
            if (len(dimensions) >= 2) and (name != 'tracks') and (not 'time' in name): # Assume image is above 2D ---- not name.startswith(self.gate_img_prefix):
                print('Adding', name, 'to images_names')
                self._non_gate_img_names.append(name)
                    
        # Make copy that can be overwritten, such that inputs are not
        tns_branches = {name:inputs[name] for name in inputs}
        # ----------------------------------------------------------------------
        # Track net
        # ----------------------------------------------------------------------
        if 'tracks' in self.data and boolify(self.data['tracks'].numpy()) and params['track_net']['use']:
            # track net is an cnn imported from track_net.py, which output its own estimate of the energy 
            # this should be concat to the output layer from the rest of the deepcalo
            # might want to use multiple with for close 1 values
            rows, shape = tns_branches['tracks'].shape[1:3]
            self.track_model  = track_net(shape=shape, rows=rows, compile_model=True,
                                          output_size=params['track_net']['output_size'],
                                          nr_neurons=params['track_net']['nr_neurons'],
                                          conv1d=params['track_net']['conv1d'],
                                          verbose=False)
            self.track_output = self.track_model(inputs['tracks'])

            # Apply track network
            tns_branches['tracks'] = self.track_output

        # Create submodels and connect them
        # ----------------------------------------------------------------------
        # Scalar net
        # ----------------------------------------------------------------------
        if ('scalars' in self.data and boolify(self.data['scalars'].numpy())
            and params['scalar_net']['connect_to']  != []):
            # Get scalar network
            if ('scalar_net' in params['track_net']['connect_to']) and (params['track_net']['use']):
                tns_branches['scalar_input']  = ks.layers.concatenate([tns_branches['scalars'], tns_branches['tracks']])
            else:
                tns_branches['scalar_input'] = tns_branches['scalars'] 
                

            self.scalar_net = get_scalar_net(input_shape=tns_branches['scalar_input'].shape[1:], # tns_branches['scalars']
                                                 **drop_key(params['scalar_net'],['connect_to', 'use']))

            # Apply scalar network
            if params['scalar_net']['use']:
                tns_branches['scalars'] = self.scalar_net(tns_branches['scalar_input'])#tns_branches['scalars'])

        # ------------------------------------------------------------------
        # Gate net for time on energy
        # ------------------------------------------------------------------
        if 'time_em_barrel' in self.data and boolify(self.data['time_em_barrel'].numpy()):
            # Get gate
            # print('%'*10, 'This has been changed from the original and needs reedit when adding gate images', '%'*10)
            self.gate_net = get_gate_net(**drop_key(params['gate_net'],'connect_to'))

            # Apply gate net to non-gated images
            # for img_name in self._non_gate_img_names:
            if 'FiLM_gen' not in params['gate_net']['connect_to']:
                tns_branches['em_barrel'] = self._apply_gate_net(tns_branches['em_barrel'], tns_branches['time_em_barrel'],
                                                              connect_to=params['gate_net']['connect_to'])
            elif 'FiLM_gen' in params['gate_net']['connect_to']:
                self._non_gate_img_names.append('time_em_barrel')
            else:
                print('connect_to is unknown! - Not using it!!!')
                # tns_branches.pop('time_em_barrel', None)
                
        if 'em_barrel' in self.data and boolify(self.data['em_barrel'].numpy()) or self._non_gate_img_names:
            # ------------------------------------------------------------------
            # Upsampling (do as late in the chain as possible, to save computation)
            # ------------------------------------------------------------------
            if params['upsampling']['use']:
                wanted_height, wanted_width = params['upsampling']['wanted_size']

                # Determine upsampling size for each non-gated image
                for img_name in self._non_gate_img_names:
                    shape = tns_branches[img_name].shape
                    size = (int(np.max((1,np.round(wanted_height/shape[1])))), int(np.max((1,np.round(wanted_width/shape[2])))))

                    # Upsample with found size
                    tns_branches[img_name] = upsample_img(tns_branches[img_name], normalize=True, size=size,
                                                          **drop_key(params['upsampling'],['use','wanted_size']))

            # Concatenate images into a single tensor before passing it to the CNN
            # this should not be done for 4 sereparate inputs
            img = [tns_branches[img_name] for img_name in self._non_gate_img_names]
            if 'FiLM_gen' not in params['gate_net']['connect_to']:
                img = self._unpack_tns_list(img)
            else:
                gate_img = img[1:]
                img = img[0]

            # ------------------------------------------------------------------
            # FiLM generator
            # ------------------------------------------------------------------
            if params['FiLM_gen']['use']:
                # Prepare input
                FiLM_gen_input = []

                # Define booleans
                scalars_to_FiLM = 'scalars' in self.data and boolify(self.data['scalars'].numpy()) and 'FiLM_gen' in params['scalar_net']['connect_to']
                
                # I have remove tracks from film layer and added it to the cnn instead
                tracks_to_FiLM = 'tracks' in self.data and boolify(self.data['tracks'].numpy()) and 'FiLM_gen' in params['track_net']['connect_to']

                gate_to_FiLM = 'time_em_barrel' in self.data and boolify(self.data['time_em_barrel'].numpy()) and 'FiLM_gen' in params['gate_net']['connect_to']

                # Scalars
                if scalars_to_FiLM:
                    FiLM_gen_input.append(tns_branches['scalars'])

                # Tracks
                if tracks_to_FiLM:
                    FiLM_gen_input.append(tns_branches['tracks'])

                # FiLM
                if gate_to_FiLM:
                    for i in gate_img:
                        self.cnn_gate = get_cnn(input_shape=i.shape[1:],
                                           **params['cnn'], name='cnn_gate')
        
                        # Apply CNN
                        cnn_gate_output = self.cnn_gate((i))
                        FiLM_gen_input.append(cnn_gate_output)
                            
                # Concatenate inputs into a single tensor before passing it to the FiLM generator
                FiLM_gen_input = self._unpack_tns_list(FiLM_gen_input, flatten=True)

                # Get FiLM generator
                self.FiLM_gen = get_FiLM_generator(FiLM_gen_input.shape[1:],
                                                   n_blocks=len(params['cnn']['block_depths']),
                                                   n_init_filters=params['cnn']['n_init_filters'],
                                                   **drop_key(params['FiLM_gen'],'use'))

                # Apply FiLM generator
                FiLM_gen_output_list = self.FiLM_gen(FiLM_gen_input)

            # ------------------------------------------------------------------
            # CNN
            # ------------------------------------------------------------------
            if 'em_barrel' in self.data and boolify(self.data['em_barrel'].numpy()) or self._non_gate_img_names:
                # Get CNN
                self.cnn = get_cnn(input_shape=img.shape[1:],
                                   FiLM_input_shapes=([out.shape[1:] for out in FiLM_gen_output_list] if params['FiLM_gen']['use'] else None),
                                   **params['cnn'])
                
                tracks_to_scalar_net = 'tracks' in self.data and boolify(self.data['tracks'].numpy()) and 'scalar_net' in params['track_net']['connect_to']

                # Apply CNN
                cnn_output = self.cnn((img if not params['FiLM_gen']['use'] else [img] + FiLM_gen_output_list))
                # --------------------------------------------------------------
                # Collect CNN model that includes the upsampling
                # --------------------------------------------------------------
                # Collect image inputs
                inputs_cnn_with_upsampling = []

                # Images
                for img_name in self._non_gate_img_names:
                    inputs_cnn_with_upsampling.append(inputs[img_name])

                if params['FiLM_gen']['use']:
                    # Scalars
                    if scalars_to_FiLM:
                        inputs_cnn_with_upsampling.append(inputs['scalars'])

                    # Tracks
                    if tracks_to_FiLM:
                        inputs_cnn_with_upsampling.append(inputs['tracks'])
                        
                    if 'time_em_barrel' in self.data and boolify(self.data['time_em_barrel'].numpy()):
                        inputs_cnn_with_upsampling.append(inputs['time_em_barrel'])
                        
                self.cnn_with_upsampling = ks.models.Model(inputs=inputs_cnn_with_upsampling, outputs=cnn_output)
                # --------------------------------------------------------------
                # Network in network
                # --------------------------------------------------------------

                if self.params['network_in_network']['use']:
                    # Get network in network
                    self.network_in_network = get_network_in_network(input_shape=cnn_output.shape[1:],
                                                                     **drop_key(params['network_in_network'],'use'))

                    # Apply network in network
                    cnn_output = self.network_in_network(cnn_output)
                    

            
        # ----------------------------------------------------------------------
        # Top
        # ----------------------------------------------------------------------
        # Prepare input
        top_input = []

        # Add CNN output
        if 'em_barrel' in self.data and boolify(self.data['em_barrel'].numpy()) or self._non_gate_img_names:
            top_input.append(cnn_output)

        # Add scalars
        if 'scalars' in self.data and boolify(self.data['scalars'].numpy()) and 'top' in params['scalar_net']['connect_to']:
            top_input.append(tns_branches['scalar_input'])

        # Add tracks
        if  ('tracks' in self.data and boolify(self.data['tracks'].numpy()) and params['track_net']['use']
             and 'top' in params['track_net']['connect_to']):#'tracks' in self.data and boolify(self.data['tracks'].numpy()) and params['track_net']:
            top_input.append(tns_branches['tracks'])

        # Concatenate inputs into a single tensor before passing it to the top
        top_input = self._unpack_tns_list(top_input, flatten=True)
        # Get top
        self.top = get_top(input_shape=top_input.shape[1:],
                           multiply_layer = tns_branches['multiply_output_name'],
                           **params['top'])

        if (('multiply_output_with' in self.data) | ('multiply_output_name' in self.data)) and boolify(self.data['multiply_output_name'].numpy()):
            top_input = [top_input, tns_branches['multiply_output_name']]
    
        # Apply top
        output = self.top(top_input)

        # if 'tracks' in self.data and boolify(self.data['tracks'].numpy()) and params['track_net']:
        #     output = ks.layers.multiply([output, track_output])

            
        if self.params['top']['BWxCB_fit'] and 'event_info' in self.data:
            # output = tf.keras.layers.Concatenate(axis=1)([output, inputs['eventnumber']])
            output = tf.keras.layers.Concatenate(axis=1)([output, inputs['event_info']])
        # ----------------------------------------------------------------------
        # Full model
        # ----------------------------------------------------------------------
        # Make the full model
        # try:
        #     del inputs['event_info']
        # except KeyError:
        #     pass
        model = ks.models.Model(inputs=[inputs[name] for name in inputs], outputs=output)

        if params['pretrained_model']['use']:
            # Set the pretrained weights
            for name in params['pretrained_model']['layers_to_load']:
                # The gate_net weights are inside a TimeDistributed layer,
                # and are therefore a bit troublesome
                if name in ['gate_net', 'time_net']: # 'time_net' is a name from older versions
                    time_distributed_layer_name = next(layer.name for layer in model.layers if layer.name.startswith('time_distributed'))
                    model.get_layer(time_distributed_layer_name).set_weights(self.gate_weights)
                else:
                    model.get_layer(name).set_weights(self.pretrained_weights[name])
                if self.verbose:
                    print(f'Loaded pretrained weights for layer with name {name} into current model.')

            # Freeze the set layers
            if type(params['pretrained_model']['freeze_loaded_layers']) == bool:
                if params['pretrained_model']['freeze_loaded_layers']:
                    for name in params['pretrained_model']['layers_to_load']:
                        model.get_layer(name).trainable = False
                        if self.verbose:
                            print(f'Froze layer with name {name}.')

            else: # 'freeze_loaded_layers' should be a list of booleans of same length as 'layers_to_load'
                assert(len(params['pretrained_model']['layers_to_load'])==len(params['pretrained_model']['freeze_loaded_layers']))
                for name,freeze in zip(params['pretrained_model']['layers_to_load'],params['pretrained_model']['freeze_loaded_layers']):
                    model.get_layer(name).trainable = not freeze
                    if self.verbose and freeze:
                        print(f'Froze layer with name {name}.')

        if self.verbose:
            model.summary()

        # Distribute batch across n_gpus if n > 1
        # if self.params['n_gpus'] > 1:
        #     model = ks.utils.multi_gpu_model(model, gpus=self.params['n_gpus'])

        # Choose weighted or unweighted metric kwargs, depending on use of sample_weights

        if self.params['metrics'] is not None:
            metrics = [get_loss_function(name) for name in params['metrics']]
            if 'sample_weights' in self.data and boolify(self.data['sample_weights'].numpy()):
                metrics_kwargs = {'weighted_metrics':metrics}
            else:
                metrics_kwargs = {'metrics':metrics}
        else:
            metrics_kwargs = {}
        
        # loss function
        if self.params['top']['probability_layer']:
            # loss_function = lambda y, rv_y: -rv_y.log_prob(y) # negative log likelihood
            def loss_function(y_true, y_pred): # aleatoric_loss
                # tf.print('mean', y_pred[:,0])
                # tf.print(y_pred[:,1])
                mean = y_pred[:,0]
                sigma = y_pred[:,1]
                # N = y_true.shape[0]
                # se = tf.keras.losses.logcosh(91**2, y_true[:,0]-mean)
                se = K.pow((y_true[:,0]-mean),2)
                # se1 = K.pow(tf.math.abs(y_true[:,0]-mean),0.25)
                inv_std = K.exp(-sigma)
                mse = inv_std*se
                # value = 0.5*(mse + sigma)
                
                #
                value = 1/2*(se/(0.001+sigma**2)+2*K.log(0.001+sigma))
                mask_nan = tf.math.is_nan(value)
                if tf.math.reduce_any(mask_nan):    
                    tf.print('mean', mean[mask_nan])
                    tf.print('sigma', sigma[mask_nan])
                # tf.print(len(value[~mask_nan]))
                return value[~mask_nan]
            def mae(y_true, y_pred):
                mean = y_pred[:,0]
                sigma = y_pred[:,1]
                return tf.math.abs(y_true[:,0]-mean)
            def mse(y_true, y_pred):
                mean = y_pred[:,0]
                sigma = y_pred[:,1]
                return K.pow((y_true[:,0]-mean),2)      
            metrics_kwargs['metrics'] = [mae,  mse]
        elif self.params['top']['BWxCB_fit']:
            class cls_loss_function(ks.losses.Loss): # aleatoric_loss
                def __init__(self, batch_number, nr_of_cores, tag, real_data=True, IQR=68):
                    import tensorflow_probability as tfp
                    super().__init__()
                    self.batch_number=batch_number
                    self.real_data=real_data
                    self.nr_of_cores = nr_of_cores
                    self.tag = tag
                    self.IQR = IQR
                                                
                def call(self, y_true, y_pred):
                    """
                    sqrt(mass) can not be optimized. Do not known why.

                    Parameters
                    ----------
                    y_true : Tensor
                        DESCRIPTION.
                    y_pred : Tensor
                        Electrons: with columns ['E_pred', 'p_e', 'eventNumber', 'p_eta', 'p_phi', 'p_eAccCluster'].
                        The same events must be right next to eachother
                        
                        Photons:
                            All events combined:
                                ['E_pred', 'p_e', 'p_eta', 'p_phi', 'eventNumber', 'p_charge', 
                                 'p_e', 'p_eta', 'p_phi', 'eventNumber', 'old_event_mass', 
                                 'p_e', 'p_eta', 'p_phi', 'eventNumber', 'p_charge']
                        
                    Returns
                    -------
                    TYPE
                        MAE of the error.

                    """
                    
                    # tf.print(y_pred)
                    if self.real_data:
                        if self.tag == 'electron':
                            y_pred = clean_batch(y_pred, number_of_cores=self.nr_of_cores)
    
                                
                            y_pred = tf.reshape(y_pred, (int(len(y_pred)/2), 12), name='pred')
                            mass_atlas = 2*(tf.math.abs(y_pred[:,1]/tf.math.cosh(y_pred[:,3]))*
                                            tf.math.abs(y_pred[:,7]/tf.math.cosh(y_pred[:,9]))*
                                            (tf.math.cosh(y_pred[:,3]-y_pred[:,9])-
                                             tf.math.cos(y_pred[:,4]-y_pred[:,10])))
                            # tf.print(tf.math.sqrt(mass_atlas))
                            # tf.print(y_pred[:,2], y_pred[:,8])
                            mask_eventNumber = (y_pred[:,2] == y_pred[:,8])
                            mask_centered = (mass_atlas >= 86**2) & (mass_atlas <= 97**2) # last mask for centered events
                            mask = mask_eventNumber & mask_centered
                            # tf.print(mask_eventNumber)
                            # tf.print('mass', tf.math.reduce_sum(tf.dtypes.cast(mask_centered, dtype = tf.int16)))
                            # tf.print('eventnumber', tf.math.reduce_sum(tf.dtypes.cast(mask_eventNumber, dtype = tf.int16)))
                            y_pred = tf.boolean_mask(y_pred, mask, axis=None, name='boolean_mask')
    
                            mass = 2*y_pred[:,0]*y_pred[:,6]*(tf.math.cosh(y_pred[:,3]-y_pred[:,9])-tf.math.cos(y_pred[:,4]-y_pred[:,10]))
                            if self.IQR:
                                tf.print(mass)
                                tf.print(mass_atlas)
                                IQR_CNN = tfp.stats.percentile(mass, q=self.IQR) - tfp.stats.percentile(mass, q=100-self.IQR)
                                IQR_ATLAS = tfp.stats.percentile(mass_atlas, q=self.IQR) - tfp.stats.percentile(mass_atlas, q=100-self.IQR)
                                tf.print(IQR_CNN)
                                tf.print(IQR_ATLAS)
                                return (IQR_CNN/IQR_ATLAS)
                            # tf.print(' length of mass events  ',len(mass))
                        elif self.tag == 'photon':
                            mask_centered = (y_pred[:,10] >= 86) & (y_pred[:,10] <= 97)
                            y_pred = tf.boolean_mask(y_pred, mask_centered, axis=None, name='boolean_mask')
                            px1, px2, px3 = (y_pred[:,0]*tf.math.cos(y_pred[:,3])/tf.math.cosh(y_pred[:, 2]),
                                             y_pred[:,6]*tf.math.cos(y_pred[:,8])/tf.math.cosh(y_pred[:, 7]),
                                             y_pred[:,11]*tf.math.cos(y_pred[:,13])/tf.math.cosh(y_pred[:, 12]))
                            py1, py2, py3 = (y_pred[:,0]*tf.math.sin(y_pred[:,3])/tf.math.cosh(y_pred[:, 2]),
                                             y_pred[:,6]*tf.math.sin(y_pred[:,8])/tf.math.cosh(y_pred[:, 7]),
                                             y_pred[:,11]*tf.math.sin(y_pred[:,13])/tf.math.cosh(y_pred[:, 12]))
                            pz1, pz2, pz3 = (y_pred[:,0]*tf.math.sinh(y_pred[:,2])/tf.math.cosh(y_pred[:, 2]),
                                             y_pred[:,6]*tf.math.sinh(y_pred[:,7])/tf.math.cosh(y_pred[:, 7]),
                                             y_pred[:,11]*tf.math.sinh(y_pred[:,12])/tf.math.cosh(y_pred[:, 12]))
                            
                            mass = (tf.math.reduce_sum([y_pred[:,0],y_pred[:,6], y_pred[:,11]], axis=0)**2
                                            -tf.math.reduce_sum([px1, px2, px3], axis=0)**2
                                            -tf.math.reduce_sum([py1, py2, py3], axis=0)**2
                                            -tf.math.reduce_sum([pz1, pz2, pz3], axis=0)**2)
                            # tf.print('length', len(mass))
                        value = tf.keras.losses.logcosh(91**2, mass)
                        
                        return value
                    else:
                        if False:
                            y_true = tf.reshape(y_true, (int(len(y_true)/2), 2), name='truth')
                            pt1_truth = tf.math.abs(y_true[:,0])/tf.math.cosh(y_pred[:, 3])
                            pt2_truth = tf.math.abs(y_true[:,1])/tf.math.cosh(y_pred[:,9])
                            mass_truth = 2*pt1_truth*pt2_truth*(tf.math.cosh(y_pred[:,3]-y_pred[:,9])-tf.math.cos(y_pred[:,4]-y_pred[:,10]))
                            
                            return tf.keras.losses.logcosh(tf.math.sqrt(mass_truth), tf.math.sqrt(mass)) 
                        else:
                            if y_true[0,0] == -999:
                                # print('run in data')
                                return tf.keras.losses.logcosh(tf.math.abs(y_pred[:,1]/tf.math.cosh(y_pred[:,3])), y_pred[:,0])     
                            else:
                                return tf.keras.losses.logcosh(y_true[:,0], y_pred[:,0]) 
                            
            ## Metric to compare to energy ER from ATLAS
            metric_loss  = cls_loss_function(self.params['batch_size'], nr_of_cores=self.nr_of_cores, tag=self.tag, real_data=False)
            ## Regress to peak
            loss_function= cls_loss_function(self.params['batch_size'], nr_of_cores=self.nr_of_cores, tag=self.tag, real_data=True)
            metrics_kwargs['metrics'] = [metric_loss]
            
        else:
            loss_function = get_loss_function(params['loss'])
        if self.verbose: print('Compiling!')
        # Compile
        model.compile(loss=loss_function,
                      optimizer=get_optimizer(params['optimizer']),
                      **metrics_kwargs
                      # # metrics=[
                      #          tf.keras.metrics.AUC(),
                      #          # tf.keras.metrics.Accuracy()
                      #          ]
                      )
        if self.verbose: print('Compiling done!')        
     
        # Save initial weights
        if self.dirs is not None:
            if self.params['n_gpus'] > 1:
                model.save_weights(self.dirs['saved_models'] + f'weights.{0:04d}-nan.hdf5')
            else:
                model.save_weights(self.dirs['saved_models'] + f'weights.{0:04d}-nan.hdf5')
            # model.save(self.dirs['saved_models'] + f'model.{0:04d}-nan.hdf5')

        return model


    def _unpack_tns_list(self, tns_list, flatten=False):

        assert(type(tns_list) in [list, tuple])
        assert(len(tns_list) > 0)

        if flatten:
            for i,tns in enumerate(tns_list):
                tns_list[i] = flatten_tns(tns)

        if len(tns_list) > 1:
            return ks.layers.Concatenate()(tns_list)
        else:
            return tns_list[0]


    def _apply_gate_net(self, img, img_gate, connect_to='concatenate'):

        # Flatten input
        # input_shape = img_gate.shape[1:] # Batch size (None) should not be included
        # flattened_img_gate = ks.layers.Reshape((np.prod(input_shape),1))(img_gate)

        # # Let all pixels pass through the function defined by the gate model
        # img_gate = ks.layers.TimeDistributed(self.gate_net)(flattened_img_gate)

        # # Convert back to original shape
        # img_gate = ks.layers.Reshape(input_shape)(img_gate)

        # # Apply the gate by doing element-wise multiplication
        # img_gate = ks.layers.multiply([img, img_gate])

        if connect_to == 'multiply':
            # img_out = img_gate
            img_out = ks.layers.multiply([img, img_gate])

        elif connect_to == 'concatenate':
            img_out = ks.layers.concatenate([img, img_gate])

        return img_out


    def _collect_pretrained_weights(self, weights_path, params_path=None,
                                    layers_to_load=['cnn'],
                                    freeze_loaded_layers=True):

        # First we build the pretrained model, so we can extract the desired weights
        # If not given, assume that params_path is in parent folder to weights_path
        if params_path is None:
            params_path = weights_path.rsplit('/',2)[0] + '/hyperparams.pkl'

        # Load parameters
        print(os.getcwd())
        with open(params_path, "rb") as f:
            params = pickle.load(f)

        # Avoid recursion
        params['pretrained_model']['use'] = False

        # Construct model
        if self.verbose:
            print('Getting pretrained model.')
        # Note that this will save plots of architectures, but that these will
        # be overwritten when the new model is created
        pretrained_model = self.get_model(params)

        # Load trained weights into the model
        # if params['n_gpus'] > 1:
        #     pretrained_model.layers.load_weights(weights_path)
        # else:
        pretrained_model.load_weights(weights_path)

        # A bit of a hacky way to access the weights in the gate_net layer, which
        # is located inside the time_distributed layer - this will have to be changed
        # if another time_distributed layer is introduced to the full model
        if 'time_net' in layers_to_load or 'gate_net' in layers_to_load: # 'time_net' only for older versions
            if 'time_net' in layers_to_load:
                gate_layer_name = 'time_net'
            else:
                gate_layer_name = 'gate_net'
            layers_to_load = [name for name in layers_to_load if name!=gate_layer_name]
            time_distributed_layer_name = next(layer.name for layer in pretrained_model.layers if layer.name.startswith('time_distributed'))
            self.gate_weights = pretrained_model.get_layer(time_distributed_layer_name).get_weights()

        # Put the wanted pretrained (non-gate) weights into dictionary
        self.pretrained_weights = {name:pretrained_model.get_layer(name).get_weights() for name in layers_to_load}
    
    
    def configure_for_performance(self, ds, repeat = False, cache=False):
        ds = ds.shuffle(buffer_size=10)
        ds = ds.batch(self.params['batch_size'], drop_remainder=True)
        if repeat:
            ds = ds.repeat()
        if cache:
            ds = ds.cache()
        ds = ds.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
        return ds

    def train_model(self):
        if self.verbose: print('Staring training!')

        # Prepare weights
        if 'sample_weights' in self.data:
            self.sample_weights = self.train['sample_weights']
            self.sample_weights_val = (self.val['sample_weights'],)
        else:
            self.sample_weights = None
            self.sample_weights_val = ()
            
        # Use learning rate finder
        if self.params['lr_finder']['use']:
            for nr in range(self.params['lr_finder']['number_of_runs']):
                if self.verbose: print('Lr Finder!')
                lr_range = self._use_lr_finder()
                if self.params['lr_schedule']['name'] is not None and lr_range is not None:
                    self.params['lr_schedule']['range'] = lr_range
                    save_dict(dict={'input_lr_range' : lr_range}, path=self.dirs['log'] + 'hyperparams.txt')
                # Plot learning rate finder results, with chosen upper and lower learning rate
                # print(self.params['lr_schedule']['range'])
                self.params['lr_schedule']['range'] = self._lrf.get_lr_range()
                self._lrf.plot_loss_vs_lr(chosen_limits=self.params['lr_schedule']['range'], session_num=nr)#self.session_num)
                self._lrf.plot_lr(session_num=self.session_num)
                print('##### lr range', self.params['lr_schedule']['range'])
            if self.params['lr_finder']['number_of_runs'] > 1:
                sys.exit()

        # Get callbacks
        callbacks = self._get_callbacks()

        try:
            self.history = self.model.fit(self.train,   
                            epochs=self.params['epochs'],
                            # steps_per_epoch = 3148,
                            batch_size = self.params['batch_size'],
                            validation_data=self.val,
                            # validation_steps=976, #self._steps_per_epoch['val']*10,
                            max_queue_size=self.params['data_generator']['max_queue_size'],
                            workers=self.params['data_generator']['n_workers'],
                            use_multiprocessing=(self.params['data_generator']['n_workers'] > 1),
                            verbose=self.verbose,
                            callbacks=callbacks)
            # print(self.history.keys())
            save_dict(self.lr_callback.history, self.dirs['log'] + f'learning_rate_history_{self.session_num}.txt', save_pkl=True)
            save_dict(self.history.history, self.dirs['log'] + f'training_history_{self.session_num}.txt', save_pkl=True)
            plt.figure(figsize = (20,10))
            plt.plot(self.lr_callback.history['lr'], label='learning rate')
            plt.xlabel('Iteration')
            plt.ylabel('Learning rate')
            plt.savefig(self.dirs['fig']+'learning_rate_distribution.png')
            
        except KeyboardInterrupt:
            if query_yes_no('Evaluate model?'):
                self.evaluate()
            else:
                print('Exiting without evaluating.')
                sys.exit()


    def _organize_data(self, set_name):
        '''
        Data should be added in the same order as inputs. Have been made to
        tf.data.Dataset
        '''

        x = {} # x = []

        # Images
        if 'images' in self.data[set_name] and boolify(self.data[set_name]['images']):
            for img_name in self.data[set_name]['images']:
                # x.append(self.data[set_name]['images'][img_name])
                x['images'] = self.data[set_name]['images'][img_name]
        # Scalars
        if 'scalars' in self.data[set_name] and boolify(self.data[set_name]['scalars']):
            # x.append(self.data[set_name]['scalars'])
            x['scalars'] = self.data[set_name]['scalars']

        # Tracks
        if 'tracks' in self.data[set_name] and boolify(self.data[set_name]['tracks']):
            # x.append(self.data[set_name]['tracks'])
            x['tracks'] = self.data[set_name]['tracks']
        # Multiply output with
        if 'multiply_output_with' in self.data[set_name] and boolify(self.data[set_name]['multiply_output_with']):
            # x.append(self.data[set_name]['multiply_output_with'])
            x['multiply_output_with'] = self.data[set_name]['multiply_output_with']

        # Targets
        y = self.data[set_name]['targets']

        if set_name != 'test': ## dataset for training
            return tf.data.Dataset.from_tensor_slices((x, y))
        else: ## dataset for testing
            return tf.data.Dataset.from_tensor_slices(x), tf.data.Dataset.from_tensor_slices(y)
        


    def _use_lr_finder(self):

        epochs = self.params['lr_finder']['epochs']
        # print(self.params['lr_finder']['scale'])
        self._lrf = LRFinder(min_lr=self.params['lr_finder']['scan_range'][0],
                       max_lr=self.params['lr_finder']['scan_range'][1],
                       scale=self.params['lr_finder']['scale'],
                       epochs=epochs,
                       fig_dir=self.dirs['lr_finder'])

        if self.verbose:
            print('Finding best learning rate(s)...')
        history = self.model.fit(self.train_lr_finder,
                        epochs=epochs,
                        max_queue_size=self.params['data_generator']['max_queue_size'],
                        workers=self.params['data_generator']['n_workers'],
                        use_multiprocessing=(self.params['data_generator']['n_workers'] > 1),
                        verbose=self.verbose,
                        callbacks=[self._lrf]
                        )
        # save_dict(history, self.dirs['lr_finder'] + f'training_history_{self.session_num}.txt', save_pkl=True)
        
        if self.verbose:
            print(f"Saved learning rate finder results in {self.dirs['lr_finder']}.")

        # Restore initial model
        self._load_best_weights()

        if self.params['lr_finder']['prompt_for_input'] and self.params['lr_schedule']['name'] is not None:

            successfully_entered = False

            while not successfully_entered:
                try:
                    min_lr = float(eval(input("Please enter min_lr: ")))
                    print("You entered " + str(min_lr))
                    max_lr = float(eval(input("Please enter max_lr: ")))
                    print("You entered " + str(max_lr))
                    successfully_entered = True
                except Exception:
                    print("Invalid input. Please try again. A valid input would be e.g. '3*1e-2'.")

            return min_lr, max_lr


    def _get_callbacks(self):

        history = ks.callbacks.History()
        modelcheckpoint = ModelCheckpoint(filepath=self.dirs['saved_models'] +
                                          '/{epoch:04d}-{val_loss:.4f}',
                                          save_best_only=True,
                                          multi_gpu_model=(self.params['n_gpus'] > 1))

        callbacks = [history, modelcheckpoint]
        if self.params['profiler']:
            tensorboard = tf.keras.callbacks.TensorBoard(log_dir=self.dirs['tensorboard'],
                                                          histogram_freq = 1,
                                                          update_freq = 1,
                                                          profile_batch=(1,10)) 
            callbacks.append(tensorboard)
        def my_learning_rate(epoch, lrate):
            print(lrate)
            return lrate

                
        callbacks.append(LearningRateScheduler(my_learning_rate))
        
        if self.params['use_earlystopping']['use']:
            earlystopping = ks.callbacks.EarlyStopping(min_delta=self.params['use_earlystopping']['delta'], patience=self.params['use_earlystopping']['patience'])
            callbacks.append(earlystopping)
            
        if self.hparams != None:
            hyperparameter_opt = hp.KerasCallback(self.dirs['tensorboard'], self.hparams)
          
        print('########### learning rate', self.params['lr_schedule'])
        if self.params['lr_schedule']['name'] == 'SGDR':
            self.lr_callback = SGDRScheduler(min_lr=self.params['lr_schedule']['range'][0],
                                        max_lr=self.params['lr_schedule']['range'][1],
                                        steps_per_epoch=self.params['lr_schedule']['step_size_factor']*self._steps_per_epoch['train'], 
                                        cycle_length=self.params['lr_schedule']['step_size_factor'],
                                        **self.params['lr_schedule']['kwargs'])

        elif self.params['lr_schedule']['name'] == 'CLR':
            self.lr_callback = CyclicLR(base_lr=self.params['lr_schedule']['range'][0],
                                   max_lr=self.params['lr_schedule']['range'][1],
                                   step_size=self.params['lr_schedule']['step_size_factor'], 
                                   **self.params['lr_schedule']['kwargs'])

        elif self.params['lr_schedule']['name'] == 'OneCycle':
            self.lr_callback = OneCycleLR(base_lr=self.params['lr_schedule']['range'][0],
                                     max_lr=self.params['lr_schedule']['range'][1],
                                     step_size=self.params['lr_schedule']['step_size_factor'], 
                                     **self.params['lr_schedule']['kwargs'])
        else:
            print('No learning rate scheduler.... Error!')
        callbacks.append(self.lr_callback)

    
        return callbacks
        


    def evaluate(self, evaluate=True, predict=True):

        # Signal that evaluation has been attempted
        if not hasattr(self, 'evaluation_scores'):
            self.evaluation_scores = None

        # Load best model
        if self.params['restore_best_weights']:
            self._load_best_weights()

        if self.verbose:
            print(f'Predicting!')
        
        preds = None

        # predicting
        # Instantiate data generator
        # test_files = [self.data_path+self.channel_tag+'_test_mc'+'/'+f for f in os.listdir(self.data_path+'test') if os.path.isfile(os.path.join(self.data_path+'test', f))]
        print(self.channel_tag)
        print(self.data_path+self.channel_tag+'_test_mc'+'/*')
        test_files = glob(self.data_path+self.channel_tag+'_test_mc'+'/*')
        print(self.data_params)
        mask_time = any(['time' in i for i in self.data_params['gate_img_prefix']])
        test = load_dataset(filenames = test_files, tag = self.tag,
                            multiple_with=True,
                            load_single_file=self.params['top']['BWxCB_fit'],
                            additional_info=True, batch_size = self.params['batch_size'],
                            cut_ratio=self.params['cut_ratio'],
                            time_lr=mask_time,
                            data_params=self.data_params,
                            ext_energy_range=self.params['ext_energy_range'])
        print('test', len(test_files))

        # Predict
        if predict:
            preds = self.model.predict(test,
                                        max_queue_size=self.params['data_generator']['max_queue_size'],
                                        workers=self.params['data_generator']['n_workers'],
                                        use_multiprocessing=(self.params['data_generator']['n_workers'] > 1),
                                        verbose=self.verbose).flatten()

            # Save predictions
            np.save(self.dirs['log'] + 'predictions', preds)
            
        # Evaluate
        if evaluate:
            scores = self.model.evaluate(test,
                                        max_queue_size=self.params['data_generator']['max_queue_size'],
                                        workers=self.params['data_generator']['n_workers'],
                                        use_multiprocessing=(self.params['data_generator']['n_workers'] > 1),
                                        verbose=self.verbose)
    
            # Collect evaluation scores
            # If there are no metrics, make score iterable
            if not self.params['metrics']:
                scores = [scores]
            self.evaluation_scores = {name:score for name,score in zip(self.model.metrics_names,scores)}

        if self.verbose:
            print(f"Results saved in {self.dirs['log']}.")
        return preds


    def _load_best_weights(self):

        pathnames = glob(self.dirs['saved_models'] + '*.hdf5')

        if len(pathnames) == 0:
            print(f"No models found in {self.dirs['saved_models']}. Continuing without loading model.")
        else:
            losses = [float(pathname.split('/')[-1].split('-',2)[1].split('.hdf5',1)[0]) for pathname in pathnames]
            best_path = pathnames[np.argsort(losses)[0]]
            if self.params['n_gpus'] > 1:
                self.model.load_weights(best_path)
            else:
                self.model.load_weights(best_path)
            if self.verbose:
                if len(pathnames) > 1:
                    print(f'Loaded best weights for evaluation from {best_path}.')
                else:
                    print(f'Loaded initial weights from {best_path}.')


    def _plot_models(self):

        # Define booleans
        use_scalars = 'scalars' in self.data and boolify(self.data['scalars'].numpy())
        use_tracks = 'tracks' in self.data and boolify(self.data['tracks'].numpy()) and self.params['track_net']['use']

        # Full
        tf.keras.utils.plot_model(self.model, to_file=self.dirs['fig'] + 'architecture_full.png', show_shapes=True)

        # Top
        tf.keras.utils.plot_model(self.top, to_file=self.dirs['fig'] + 'architecture_top.png', show_shapes=True)

        if 'em_barrel' in self.data and boolify(self.data['em_barrel'].numpy()):
            # CNN
            tf.keras.utils.plot_model(self.cnn, to_file=self.dirs['fig'] + 'architecture_cnn.png', show_shapes=True)

            # Network in network
            if self.params['network_in_network']['use']:
                tf.keras.utils.plot_model(self.network_in_network, to_file=self.dirs['fig'] + 'architecture_network_in_network.png', show_shapes=True)

            # FiLM generator
            if self.params['FiLM_gen']['use'] and (use_scalars or use_tracks):
                tf.keras.utils.plot_model(self.FiLM_gen, to_file=self.dirs['fig'] + 'architecture_FiLM_gen.png', show_shapes=True)

            # Gate net
            if any(img_name.startswith(self.gate_img_prefix) for img_name in self.data): 
                tf.keras.utils.plot_model(self.gate_net, to_file=self.dirs['fig'] + 'architecture_gate_net.png', show_shapes=True)

        # Scalar net
        if use_scalars:
            tf.keras.utils.plot_model(self.scalar_net, to_file=self.dirs['fig'] + 'architecture_scalar_net.png', show_shapes=True)

        # Track net
        if use_tracks:
            tf.keras.utils.plot_model(self.track_model, to_file=self.dirs['fig'] + 'architecture_track_net.png', show_shapes=True)
            
