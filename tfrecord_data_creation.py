#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 16:07:27 2020

@author: malte
"""

import tensorflow as tf
import numpy as np
import h5py
from tqdm import tqdm
import os
import concurrent.futures
import sys
from collections import Counter
import matplotlib.pyplot as plt
import pandas as pd
import math
from os import listdir
import random
from os.path import isfile, join
import time
from glob import glob
from importlib import import_module
from numba import njit
from skhep.math import vectors
np.seterr(divide='ignore', invalid='ignore')
import matplotlib
sys.path.insert(1,'../deepcalo')
from sympy.solvers import solve
from sympy import Symbol
try:
    from .utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel, import_track
except (ModuleNotFoundError, ImportError):
    from utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel, import_track
    

def create_dir(*, dirs, quit_script):
    if not os.path.exists(dirs):
        print('Creating folder:')
        print(dirs)
        os.makedirs(dirs)
    else:
        print('Folder already existing!')
        if quit_script:
            sys.exit()
        return False

def cal_invM(e1, e2, eta1, eta2, phi1, phi2, e3=None, eta3=None, phi3=None, third = True, et_conversion=True):
    vecFour1 = vectors.LorentzVector()
    vecFour2 = vectors.LorentzVector()
    vecFour1.setptetaphim(e1/np.cosh(eta1) if et_conversion else e1, eta1, phi1, 0)
    vecFour2.setptetaphim(e2/np.cosh(eta2) if et_conversion else e2, eta2, phi2, 0)
    if third:
        vecFour3 = vectors.LorentzVector()
        vecFour3.setptetaphim(e3/np.cosh(eta3), eta3, phi3, 0)
        vecFour = vecFour1+vecFour2+vecFour3
    else:
        vecFour = vecFour1+vecFour2
    return vecFour.mass

def true_events_in_data(path, set_name, isMC, n=-1, tag='Zee', mass_cut=False):
    if n==None:
        min=0
        max=None
    elif isinstance(n, int):
        min = 0
        max= n
    elif isinstance(n, list):
        min, max = n
    else:
        print('n not correctly defined list(int, int) or int - EXITING!')
        sys.exit()
    data = h5py.File(path, "r")
    all_evtnr = data[set_name]['eventNumber'][min:max]
    if not isMC:
        eta = np.cosh(data[set_name]['p_eta'][min:max])
        phi = np.cosh(data[set_name]['p_phi'][min:max])
        E_T = data[set_name]['p_e'][min:max]/eta
        mask_E_T = (E_T>9.5)
        if tag == 'Zee':
            mass = np.array([all_evtnr,
                             data[set_name]['p_e'][min:max],
                             data[set_name]['p_eta'][min:max],
                             data[set_name]['p_phi'][min:max]]
                             ).T
            mass = mass[mass[:,1]/np.cosh(mass[:,2]) >= 9.5]
            mass = mass[np.argsort(mass[:,0])]
            index, counts = np.unique(mass[:,0], return_counts=True)
            mass = mass[np.in1d(mass[:,0], index[counts == 2])]
            if mass_cut:
                mass = np.c_[mass[::2], mass[1::2]]
                total_mass = []
                for i in mass:
                    total_mass.append([i[0], cal_invM(e1=i[1], e2=i[5], e3=None, eta1=i[2],eta2=i[6],eta3=None, phi1=i[3],phi2=i[7], phi3=None, third = False)])
                total_mass = np.array(total_mass)
                mask_true_events = (total_mass[:,1] >= 86) & (97 >= total_mass[:,1]) & mask_E_T
                # evtnr = total_mass[mass_cut,0]
            else:
                mask_true_events = mask_E_T
                
                # print('need to create mask_true_events')
        elif tag == 'Zmumugam':
            unique_evtnr, counts = np.unique(all_evtnr, return_counts=True)
            p_charge = data[set_name]['p_charge'][min:max]
            index = np.arange(len(p_charge))
            index_muons_all = []
            index_photons_all = []
            for nr in unique_evtnr:#tqdm(unique_evtnr, total = len(unique_evtnr)): 
                mask_photon_loose = (all_evtnr == nr) & (data[set_name]['p_PhotonLHLoose'][min:max]) & (E_T > 9.5)
                if not mask_photon_loose.any():
                    continue
                elif np.sum(mask_photon_loose) == 1:
                    index_photons_all.append(index[mask_photon_loose])
                    continue
                mask_photon_tight = (all_evtnr == nr) & (data[set_name]['p_PhotonLHTight'][min:max]) & (E_T > 9.5)
                if (np.sum(mask_photon_tight) == 0) | (np.sum(mask_photon_tight) > 0):
                    continue
                elif np.sum(mask_photon_tight) == 1:
                    index_photons_all.append(index[mask_photon_tight])

                
            for nr in unique_evtnr:#tqdm(unique_evtnr, total = len(unique_evtnr)): ## muon loop
                mask_muon_loose = (all_evtnr == nr) & (data[set_name]['p_MuonLHLoose'][min:max]) & (E_T > 9.5)
                if (not np.sum(p_charge[mask_muon_loose] != 0)>1) | ((np.sum(p_charge[mask_muon_loose]) != 0) &
                                                                     (len(p_charge[mask_muon_loose]) == 2)):
                    #index_muons_all.append([False]*mask_muon) # not enough muons
                    continue
                elif (np.sum(p_charge[mask_muon_loose]) == 0) & (np.sum(p_charge[mask_muon_loose] != 0)==2):
                    if 20<cal_invM(e1=E_T[mask_muon_loose][0],
                                     e2=E_T[mask_muon_loose][1],
                                     eta1=eta[mask_muon_loose][0],
                                     eta2=eta[mask_muon_loose][1],
                                     phi1=phi[mask_muon_loose][0],
                                     phi2=phi[mask_muon_loose][1], 
                                     third=False, et_conversion=False) < 82:
                        index_muons_all.append(index[mask_muon_loose]) # only two muons
                    continue
                else: # more than two muon
                    # calculate invariant mas - assume only 3 particles
                    sorted_index = np.argsort(p_charge[mask_muon_loose] == -np.sign(p_charge[mask_muon_loose].sum()))
                    if len(sorted_index) > 3:
                        # print('Problem too many particles')
                        # print(p_charge[mask_muon_loose])
                        pass
                    if (np.sum(p_charge[mask_muon_loose] == -1) == 1) | (np.sum(p_charge[mask_muon_loose] == 1) == 1):
                        looped_index = []
                        for i in sorted_index[:-1]:
                            mass_calculated = cal_invM(e1=E_T[mask_muon_loose][sorted_index[i]],
                                             e2=E_T[mask_muon_loose][sorted_index[-1]],
                                             eta1=eta[mask_muon_loose][sorted_index[i]],
                                             eta2=eta[mask_muon_loose][sorted_index[-1]],
                                             phi1=phi[mask_muon_loose][sorted_index[i]],
                                             phi2=phi[mask_muon_loose][sorted_index[-1]], 
                                             third=False, et_conversion=False)
                            if (mass_calculated<82) & (mass_calculated>40):
                                looped_index.append([sorted_index[i], sorted_index[-1]])
                            else:
                                continue
                        if len(looped_index) == 0:
                            continue
                        elif len(looped_index) == 1:
                            if 20<cal_invM(e1=E_T[mask_muon_loose][0],
                                             e2=E_T[mask_muon_loose][1],
                                             eta1=eta[mask_muon_loose][0],
                                             eta2=eta[mask_muon_loose][1],
                                             phi1=phi[mask_muon_loose][0],
                                             phi2=phi[mask_muon_loose][1], 
                                             third=False, et_conversion=False) < 82:
                                index_muons_all.append(index[mask_muon_loose][looped_index[0]])
                            continue
                        else:
                            pass
                    else:
                        continue

                    event_index = index[mask_muon_loose]
                    mask_muon_tight = (all_evtnr == nr) & (data[set_name]['p_MuonLHTight'][min:max]) & (E_T > 9.5) # should be p_MuonLHTight, but it was missing
                    
                    if (len(index[mask_muon_tight]) == 1):
                        mask_muon_loss = -p_charge[mask_muon_tight] == p_charge[mask_muon_loose]
                        if (np.sum([mask_muon_loss]) == 1):
                            if 20<cal_invM(e1=E_T[mask_muon_tight][0],
                                             e2=E_T[mask_muon_loose][mask_muon_loss][0],
                                             eta1=eta[mask_muon_tight][0],
                                             eta2=eta[mask_muon_loose][mask_muon_loss][0],
                                             phi1=phi[mask_muon_tight][0],
                                             phi2=phi[mask_muon_loose][mask_muon_loss][0], 
                                             third=False, et_conversion=False) < 82:
                                index_muons_all.append(index[mask_muon_tight])
                                index_muons_all.append(index[mask_muon_loose][mask_muon_loss])
                        else:
                            print('Problem')
                    elif (len(index[mask_muon_tight]) == 2) & (np.sum(p_charge[mask_muon_tight])==0):
                        if 20<cal_invM(e1=E_T[mask_muon_tight][0],
                                         e2=E_T[mask_muon_tight][1],
                                         eta1=eta[mask_muon_tight][0],
                                         eta2=eta[mask_muon_tight][1],
                                         phi1=phi[mask_muon_tight][0],
                                         phi2=phi[mask_muon_tight][1], 
                                         third=False, et_conversion=False) < 82:
                            index_muons_all.append(index[mask_muon_tight])
                    elif (len(index[mask_muon_tight]) == 3):
                        #np.in1d(event_info[:,3], evtnr[counts == 3])
                        pass
            # print('Muons:', len(np.hstack(index_muons_all)))
            # print('Photons:', len(np.hstack(index_photons_all)))
            mask_true_events = np.in1d(index, np.sort(np.hstack(index_muons_all+index_photons_all)))
           # correct_charge = np.hstack([[np.sum(p_charge[all_evtnr == i]) == 0]*nr for i,nr in zip(unique_evtnr[0], unique_evtnr[1])])
            #muonsp_MuonLHLoose
            # mask_muon = (data[set_name]['p_MuonLHLoose'][min:max]) & (np.array([np.sum(p_charge[all_evtnr == i]) == 0 for i, counts in np.unique(all_evtnr, return_counts=True)]))
            
            # #%%
            # mask_photon = (
            #                 (data[set_name]['p_PhotonLHLoose'][min:max])  # using Tight in photons
            #                  & ((data[set_name]['p_ptvarcone20'][min:max]+data[set_name]['p_ptvarcone30'][min:max]+data[set_name]['p_ptvarcone40'][min:max])/E_T < 0.3)
            #                  & ((data[set_name]['p_topoetcone20'][min:max]+data[set_name]['p_topoetcone30'][min:max]+data[set_name]['p_topoetcone40'][min:max])/E_T < 0.3)
            #                 )
            # mask_true_events = (mask_muon | mask_photon) & mask_E_T
            # index, counts = np.unique(all_evtnr[mask_true_events], return_counts =True)
            # mask_true_events = np.in1d(all_evtnr,index[counts >= 3]) & (mask_muon | mask_photon) & mask_E_T# det samme cut igen, lidt sjovt men burde give mening
            # number of events
            # evtnr = [item for item, count in Counter(all_evtnr[mask_true_events]).items() if count >= 3] 
            # mask_duplicates = np.in1d(all_evtnr,duplicates)
            
            # evtnr = all_evtnr[mask_duplicates]
        else:
            print('New tag needed!')
            sys.exit()
    else:
        pdgid = data[set_name]['p_truth_pdgId'][min:max]
        truthOrigin = data[set_name]['p_truthOrigin'][min:max]
        E_T = data[set_name]['p_e'][min:max]/np.cosh(data[set_name]['p_eta'][min:max])
        mask_E_T = E_T >= 4.5
        if 'single' in tag:
            # pdgid_mask = np.array([True]*len(mask_E_T))
            # mask_truthOrigin = np.array([True]*len(mask_E_T))
            return np.array([True]*len(mask_E_T))
        elif 'Zmumugam' in tag:
            pdgid_mask = (pdgid ==22) | (np.abs(pdgid) == 13)
            mask_truthOrigin = (truthOrigin == 3) | (truthOrigin == 13)
        elif 'Hyy' in tag:   
            pdgid_mask = (pdgid ==22) 
            mask_truthOrigin = (truthOrigin == 14)
        elif 'Zee' in tag:
            pdgid_mask = (np.abs(pdgid) ==11) 
            mask_truthOrigin = (truthOrigin == 13)
        else:
            print('new tag needed!')
            sys.exit()
        index, counts = np.unique(all_evtnr[mask_truthOrigin & pdgid_mask & mask_E_T],
                                  return_counts=True)
        # evtnr = all_evtnr[mask_truthOrigin & pdgid_mask & mask_E_T]
        # mask_true_events = mask_truthOrigin & pdgid_mask & mask_E_T
        particle_counts = 3 if tag=='Zmumugam' else 2
        print('counts', particle_counts)
        mask_true_events = np.in1d(all_evtnr, index[counts==particle_counts])

    # true_event_index = np.in1d(all_evtnr, evtnr)
    return mask_true_events

@njit(cache=True)
def histogram_equalization(arr: np.array, target: np.array):
    arr_sorted = np.sort(arr)
    target_sorted = np.sort(target)
    new_array = []
    p_arr = 1. * np.arange(len(arr_sorted)) / (len(arr_sorted) - 1)
    p_target = 1. * np.arange(len(target_sorted)) / (len(target_sorted) - 1)
    for i in arr:
        p = p_arr[arr_sorted >= i]
        new_array.append(target_sorted[p_target >= p[0]][0])
    return new_array

def standardization(*, data: dict, data_params: dict,
                    sample_name: str, dirs: str, tag: str,
                    save: bool, renormalize=True) -> dict:
    """
    Parameters
    ----------
    data : dict
        Scalar data
    data_params : dict
        Name of the scalar in correct order
    sample_name : str
        DESCRIPTION.
    dirs : str
        path to normalized models

    Returns
    -------
    dict
        normalized data
    """
    
    if save:
        print('Create new standardization')
        create_dir(dirs=dirs, quit_script=True)
        
    else:
        files=[]
        for file in os.listdir(dirs):
            files.append(dirs+file)
        files = {path.split('_')[-1].split('.')[0]:path for path in files}
        
    scalars_to_quantile = ['p_eAccCluster', 'averageInteractionsPerCrossing',
                           'p_f0Cluster', 'p_R12', 'p_pt_track', 'p_deltaPhiRescaled2',
                           'p_poscs2', 'p_deltaEta2', 'p_fTG3',
                            'tracks_dR', 'tracks_pt',
                            'tracks_d0', 'tracks_z0'
                            ]
    scalars_to_standard = []#['p_fTG3', 'tile_gap_Lr1']
    
    for dataset_name in ['scalars', 'tracks']:
        if dataset_name=='scalars':
            data_param_name = 'scalar_names'
        elif dataset_name=='tracks':
            data_param_name = 'track_names'
        if data_params[data_param_name] is None:
            continue
        # Make directory for saving scalers
            
        for name in data_params[data_param_name]:
            scaler_name = 'Quantile' if name in scalars_to_quantile else 'Standard' if name in scalars_to_standard else 'Robust'
            var_ind = data_params[data_param_name].index(name)

            if save:
                standardize(data,
                            name,
                            scaler_name=scaler_name,
                            save_path=os.path.join(dirs, f'scaler_{scaler_name}_{name}.jbl'),
                            renormalize=renormalize)
            else:
                path = files[name.split('_')[-1]]
                standardize(data,
                            dataset_name,
                            variable_index=var_ind,
                            scaler_name=scaler_name,
                            sample_name = sample_name,
                            load_path = path,
                            renormalize=renormalize)
    return data

def chunks(lst, n):
    if type(lst) == int:
        lst = range(lst)
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]
#%% =============================================================================
# Create tfrecords files
# =============================================================================
class TfrecordsFileCreation():
    def __init__(self, paths: list, data_params: dict, tag: str, norm_size: int = 1_000_000, data_paths:str =None):
        
        def set_name_w_train(paths):
            nr = 0
            while True: # make sure the first file in paths has train sample
                random.shuffle(paths)
                file = paths[0]
                # print(file)
                df = h5py.File(file, "r")
                try:
                    if nr > 200:
                        n_points = int(df['test']['p_eta'].shape[0]-1)
                    else:
                        n_points = int(df['train']['p_eta'].shape[0]-1)
                    break
                except:
                    nr+=1
                    continue 
                df.close()
            return paths
        
        self.norm_size = norm_size
        
        paths = set_name_w_train(paths)
            
        if data_paths == None:
            self.data_paths = paths
        else:
            data_paths = set_name_w_train(data_paths)
            self.data_paths = data_paths
        self.paths = paths
        self.data_params = data_params
        self.tag = tag
        self.isMC = 'mc' in self.paths[0]
        
        
        self.transformation_data = False

    @staticmethod
    def _bytes_feature(value):
      """Returns a bytes_list from a string / byte."""
      if isinstance(value, type(tf.constant(0))):
        value = value.numpy() # BytesList won't unpack a string from an EagerTensor.
      return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))
    @staticmethod
    def _float_feature(value):
      """Returns a float_list from a float / double."""
      # value=np.array(value)
      return tf.train.Feature(float_list=tf.train.FloatList(value=value))
    @staticmethod
    def _int64_feature(value):
      """Returns an int64_list from a bool / enum / int / uint."""
      return tf.train.Feature(int64_list=tf.train.Int64List(value=value))
    
    def serialize_example(self, images:dict=None, targets: float=None, scalars:np.array = None,
                          multiply_output: np.array = None, event_info: np.array=None,
                          track: np.array=None):
        """
        Creates a tf.train.Example message ready to be written to a file.
        """
        # Create a dictionary mapping the feature name to the tf.train.Example-compatible
        feature = {}
        
        # MC or data
        sample_type = self.isMC*1
        feature['type'] = self._float_feature([sample_type])
        
        if scalars is not None:
            for nr, name in enumerate(self.data_params['scalar_names']): 
                feature[name] = self._float_feature([scalars[nr]])
        if targets is not None:   
            if self.classification == -1:
                feature['targets'] = self._float_feature([targets])
            else:
                feature['targets'] = self._int64_feature([targets])
        if images is not None:
            for name in images:
                feature[name+'_Lr0'] = self._float_feature(np.ravel(images[name][:,:, 0]))
                feature[name+'_Lr1'] = self._float_feature(np.ravel(images[name][:,:, 1]))
                feature[name+'_Lr2'] = self._float_feature(np.ravel(images[name][:,:, 2]))
                feature[name+'_Lr3'] = self._float_feature(np.ravel(images[name][:,:, 3]))
        if multiply_output is not None:
            feature['multiply_output_name'] = self._float_feature([multiply_output])
        if event_info is not None:
            feature['event_info'] = self._float_feature(event_info)
        if track is not None:
            # feature['tracks'] = self._float_feature(np.ravel(track))
            for nr, name in enumerate(self.data_params['track_names']):
                feature[name] = self._float_feature(track[:,nr])
        
            # Create a Features message using tf.train.Example.
        return tf.train.Example(features=tf.train.Features(feature=feature))
    
    def perform_transformation(self, data, columns):
        transformed_data = []
        print('Performing transformation - Might take long.')
        for nr, i in tqdm(enumerate(columns)): # ['averageInteractionsPerCrossing']):
            # if i=='averageInteractionsPerCrossing':
            print(nr, i)
            array = histogram_equalization(arr=data[:,nr], target=self.transformation_data[i])
            transformed_data.append(array)
        transformed_data = np.array(transformed_data)
        return transformed_data
        

    def create_single_tfrecord(self, args):
        points, set_name, path, iterations, data_params, save_path, rows = args
        # try: # if there is an error in paralell it will not stop the run
        chunk_points = chunks(points, math.ceil(len(points)/iterations))
        name = f'{path.split("/")[-1].split(".")[0]}_{set_name}_{self.tag}_{np.max(points)}_images_{time.time()}.tfrecords'
    
        track_variables = self.data_params['track_names']
        number_of_events_saved=0
        # print('#'*10, path)
        with tf.io.TFRecordWriter(save_path+name, tf.io.TFRecordOptions(compression_type='GZIP')) as writer:
            with h5py.File(path, "r") as df:
                for ch in tqdm(chunk_points, disable=False):
                    #scalars
                    # print(data_params['multiply_output_name'])
                    scalars = [df[set_name][i][np.min(ch):np.max(ch)] for i in data_params['scalar_names']]
                    scalars = np.vstack(scalars).T
                    if self.transformation_data:
                        scalars = self.perform_transformation(data=scalars,
                                                         columns=data_params['scalar_names'])
                        scalars=scalars.T
                    scalars[scalars == -999] = 0
                    scalars[scalars == -76923] = 0
                    scalars = standardization(data=scalars, data_params={'scalar_names': data_params['scalar_names'], 'track_names':None}, sample_name=set_name,
                                    dirs=self.norm_path, tag=self.tag, save=False)

                    # track variables
                    tracks, track_names = import_track(track_names = data_params['track_names'], df=df, set_name=set_name, norm_size=[np.min(ch), np.max(ch)], rows=rows)
                    # data_params['track_names'] = track_names
                    tracks = standardization(data=tracks, data_params={'scalar_names': None, 'track_names': data_params['track_names']}, sample_name=set_name,
                                    dirs=self.norm_path, tag=self.tag, save=False)
                    
                    # Adding multiply output
                    if False:#data_params['multiply_output_name'] != None:
                        multiply_output = df[set_name][data_params['multiply_output_name']][np.min(ch):np.max(ch)] 
                        multiply_output = np.array(multiply_output)
                    elif self.classification == -1:
                        multiply_output = df[set_name]['p_eAccCluster'][np.min(ch):np.max(ch)] 
                        multiply_output = np.array(multiply_output)
                    elif self.classification != -1:
                        multiply_output = [None]*len(scalars)
                        
                        
                    #images
                    images = {}
                    em_barrel_names = [data_params['img_names'][0]+f'_Lr{i}' for i in range(4)] 
                    em_barrel_images = np.array([df[set_name][i][np.min(ch):np.max(ch)] for i in em_barrel_names])
                    images['em_barrel'] = np.moveaxis(em_barrel_images, 0, -1)
                    if data_params['gate_img_prefix'] != None:
                        if not isinstance(data_params['gate_img_prefix'], list):
                            gate_img_name = list(data_params['gate_img_prefix'])
                        else: 
                            gate_img_name = data_params['gate_img_prefix']
                        for name in gate_img_name:
                            gate_image_names = [name+f'_Lr{i}' for i in range(4)] 
                            gate_image = np.array([df[set_name][i][np.min(ch):np.max(ch)] for i in gate_image_names])
                            images[name] = np.moveaxis(gate_image, 0, -1)
                    else:
                        time_images = None
                    
                    # Inputing all event info need for the custom loss function (and also addisional p_e)
                    # Because Zmumugam is shuffled the same eventnumbers are not grouped.
                    number_of_particles_in_event = 3 if (self.tag == 'Zmumugam') else 2 # 
                    if (number_of_particles_in_event == 3) & (not self.isMC):
                        event_info = [df[set_name][i][np.min(ch):np.max(ch)] for i in ['p_e', 'p_eta', 'p_phi', 'eventNumber', 'p_charge']]
                        # Photonloose = df[set_name]['p_PhotonLHLoose'][np.min(ch):np.max(ch)]
                        # Muonloose = df[set_name]['p_MuonLHLoose'][np.min(ch):np.max(ch)]
                        # p_charge = df[set_name]['p_charge'][np.min(ch):np.max(ch)]
                        # event_info.append(Photonloose*0+Muonloose*(-1))
                        event_info = np.vstack(event_info).T
                        # event_info[:,0][Muonloose] = event_info[:,0][Muonloose]*np.array(p_charge)[Muonloose]
                        # print(event_info)
                    else:
                        event_info = [df[set_name][i][np.min(ch):np.max(ch)] for i in ['p_e', 'p_eta', 'p_phi', 'eventNumber','p_eAccCluster']]
                        event_info = np.vstack(event_info).T    
                        
                    if not (self.tag == 'Zmumugam'):
                        duplicates = [item for item, count in Counter(event_info[:,1]).items() if count == number_of_particles_in_event] # find duplicates change to 3 if 3 particles in event
                        mask_duplicates = np.in1d(event_info[:,1],duplicates)
                    
                    
                    # targets
                    if ('ATLAS_mass' in df[set_name].keys()): #(data_params['target_name'] == 'ATLAS_91_energy') & (set_name != 'test'):    
                        if (set_name != 'test'):
                            mask_atlas_energy_predict = (96>=df[set_name]['ATLAS_mass'][np.min(ch):np.max(ch)]) & (df[set_name]['ATLAS_mass'][np.min(ch):np.max(ch)] >= 86)
                            mass_cut = True
                            targets = df[set_name]['ATLAS_91_energy'][np.min(ch):np.max(ch)]
                        else:
                            targets = df[set_name][data_params['target_name']][np.min(ch):np.max(ch)]
                            mask_atlas_energy_predict = np.array([True]*len(targets))
                        mask_truth = np.array([True]*len(targets))
                    else:
                        targets = df[set_name][data_params['target_name']][np.min(ch):np.max(ch)]
                        mask_atlas_energy_predict = np.array([True] * len(targets))
                        mass_cut = False
                        
                        
                        ## remove any any particle that should be used in the energy regression
                        if self.isMC:
                            pdgid_value = 22 if ('Zmumugam' in self.tag) or ('Hyy' in self.tag ) else 11
                            
                        mask_truth = true_events_in_data(path=path, set_name=set_name, isMC=self.isMC, n=[np.min(ch),np.max(ch)], tag=self.tag, mass_cut=mass_cut)
                    mask_truth = mask_truth & mask_atlas_energy_predict
                    # print(f'Acceptance: {np.sum(mask_truth)}/{len(mask_truth)}')  
                    eventNumber = df[set_name]['eventNumber'][np.min(ch):np.max(ch)][mask_truth]
                    pdgid = np.abs(df[set_name]['p_truth_pdgId'][np.min(ch):np.max(ch)])[mask_truth]
                    targets = targets[mask_truth]
                    for i in images.keys():
                        images[i] = images[i][mask_truth]
                    multiply_output = multiply_output[mask_truth]
                    event_info = event_info[mask_truth]
                    tracks = tracks[mask_truth]
                    scalars = scalars[mask_truth]
                    mass_lst = []
                    number_of_duplicates_targets = 0
                    
                    # =============================================================================
                    # import data  into the file
                    # =============================================================================
                    for i in tqdm(range(len(scalars)), disable=False):
                        all_event_info = event_info[event_info[i,3] == event_info[:,3]]
                        if 'single' not in self.tag:
                            if (self.isMC and (all(all_event_info[:,4] == -999) or (pdgid[i] != pdgid_value))) or (not len(all_event_info) >= number_of_particles_in_event): continue
                            
                            if self.isMC: mask_eventNumber = pdgid[eventNumber == all_event_info[0,3]] != pdgid_value
                            
                            if (self.tag == 'Zmumugam') & (self.isMC) & (len(all_event_info) > number_of_particles_in_event):
                                photon_event= event_info[i]
                                lepton_events_mask = all_event_info[:,0]!=event_info[i,0]
                                lepton_events_mask_2nd = all_event_info[lepton_events_mask][:,4] == -999
                                muon_events = all_event_info[lepton_events_mask][lepton_events_mask_2nd]
                                all_event_info = np.vstack((photon_event, muon_events))
                            elif (self.tag == 'Zmumugam') & (not self.isMC):
                                if event_info[i,4] == -1: continue ## only take in the photons
                                muon_event = all_event_info[np.abs(all_event_info[:,4]) == 1]
    
                                all_event_info  = np.vstack((muon_event, event_info[i:i+1]))
                                all_event_info[:,0] = np.abs(all_event_info[:,0])
    
                                mass_all = cal_invM(e1=all_event_info[0,0], e2=all_event_info[1,0], e3=all_event_info[2,0],
                                                    eta1 = all_event_info[0,1],eta2=all_event_info[1,1],eta3=all_event_info[2,1],
                                                    phi1 = all_event_info[0,2],phi2=all_event_info[1,2], phi3=all_event_info[2,2])
    
                                if False:#(86<mass_all<97):
                                    # print(mass, '-----', mass_all, '----', event_info[i,3])
                                    px1, px2 = muon_event[:,0]*np.cos(muon_event[:,2])/np.cosh(muon_event[:, 1])
                                    py1, py2 = muon_event[:,0]*np.sin(muon_event[:,2])/np.cosh(muon_event[:, 1])
                                    pz1, pz2 = muon_event[:,0]*np.sinh(muon_event[:,1])/np.cosh(muon_event[:, 1])
                                    # mass = (np.sum(all_event_info[:,0], axis=0)**2
                                    #                     -np.sum([px1, px2, px3], axis=0)**2
                                    #                     -np.sum([py1, py2, py3], axis=0)**2
                                    #                     -np.sum([pz1, pz2, pz3], axis=0)**2)
                                    mask_photon = all_event_info[:,4]==0
                                    E = Symbol('E')
                                    # print(i, px1, px2)
                                    # print(i, py1, py2)
                                    # print(i, pz1, pz2)
                                    # print(i, all_event_info)
                                    if np.sum(np.abs([px1, px2, py1, py2, pz1, pz2]))>700:
                                        continue
                                        
                                    flag = {'simplify':False, 'rational':False}
                                    E_photon = solve((np.sum(muon_event[:,0])+E)**2
                                          -(px1+px2+E*np.cos(all_event_info[mask_photon][:,2])/np.cosh(all_event_info[mask_photon][:, 1]))**2
                                          -(py1+py2+E*np.sin(all_event_info[mask_photon][:,2])/np.cosh(all_event_info[mask_photon][:, 1]))**2
                                          -(pz1+pz2+E*np.sinh(all_event_info[mask_photon][:,1])/np.cosh(all_event_info[mask_photon][:, 1]))**2-91**2, E,
                                          flag=flag)
                                    # print(i, 'finish solving for e_photon')
                                    # print('find particle')
                                    # print(1, E_photon)
                                    E_photon = np.array([float(i[0]) for i in E_photon])
                                    E_photon = E_photon[(0<E_photon) & (E_photon< 250)]
                                    # print(2, E_photon)
                                    if len(E_photon) == 0:
                                        continue
                                    else:
                                        targets[i] = E_photon
                                else:
                                    # targets[i] = 0
                                    pass
                                    # only_muon_event = np.sum(muon_event, axis =0)
                                    # e = [all_event_info[2,0], only_muon_event[0]]
                                    # eta = [all_event_info[2,1], only_muon_event[1]]
                                    # phi = [all_event_info[2,2], only_muon_event[2]]
                                    
                                    # vecFour1 = vectors.LorentzVector()
                                    # vecFour2 = vectors.LorentzVector()
                                    # vecFour3 = vectors.LorentzVector()
                                    # vecFour1.setptetaphim(muon_event[0,0]/np.cosh(muon_event[0,1]), muon_event[0,1], muon_event[0,2], 0)
                                    # vecFour2.setptetaphim(muon_event[1,0]/np.cosh(muon_event[1,1]), muon_event[1,1], muon_event[1,2], 0)
                                    # # vecFour3.setptetaphim(all_event_info[2,0]/np.cosh(all_event_info[2,1]), all_event_info[2,1], all_event_info[2,2], 0)
                                    # vecFour = vecFour1+vecFour2#+vecFour3
                                    
                                    # vecFour1 = vectors.LorentzVector()
                                    # vecFour2 = vectors.LorentzVector()
                                    # vecFour1.setptetaphim(vecFour.pt, vecFour.eta, vecFour.phi(), vecFour.mass)
                                    # vecFour2.setptetaphim(e[0]/np.cosh(eta[0]), eta[0], phi[0], 0)
                                    # # vecFour3.setptetaphim(all_event_info[2,0]/np.cosh(all_event_info[2,1]), all_event_info[2,1], all_event_info[2,2], 0)
                                    # vecFour = vecFour1+vecFour2#+vecFour3
                                    
                                    # k = (np.cosh(eta[0]-vecFour.eta)-np.cos(phi[0]-vecFour.phi()))
                                    # energy0 = 91**2*np.cosh(eta[0])*np.cosh(vecFour.eta)/( 2 * k * vecFour.e)
                                    # mass = np.sqrt(2 * np.prod(e)/np.prod(np.cosh(eta)) * k)
                                    
                                    # k = (np.cosh(eta[0]-eta[1])-np.cos(phi[0]-phi[1]))
                                    # energy0 = 91**2*np.prod(np.cosh(eta))/( 2 * k * e[1])
                                    # mass = np.sqrt(2 * np.prod(e)/np.prod(np.cosh(eta)) * k)
                                    # # targets[i] = energy0
                                    # mass_with_new_photon = cal_invM(e1=all_event_info[0,0], e2=all_event_info[1,0], e3=E_photon[0],
                                    #                         eta1 = all_event_info[0,1],eta2=all_event_info[1,1],eta3=all_event_info[2,1],
                                    #                         phi1 = all_event_info[0,2],phi2=all_event_info[1,2], phi3=all_event_info[2,2])
                                    # # print(mass_with_new_photon)
                                # else:
                                #     mass_with_new_photon = 0
                                #     targets[i] = 0
                                mass_lst.append(np.float32(mass_all))
                                all_event_info[2, 4] = np.float32(mass_all) ## 
                            if not len(all_event_info) == number_of_particles_in_event: continue
                        image={}
                        for name in images.keys():
                            image[name] = images[name][i,:,:,:]
                        mask_event = event_info[:,3][i] == event_info[:,3]
                        mask_check_duplicates = np.sum(event_info[event_info[:,3][i] == event_info[:,3]] == event_info[i], 1) == 5
                        if ((len(np.unique(targets[event_info[:,3][i] == event_info[:,3]])) == 1) and self.isMC and not 'single'
                             in self.tag or ((np.sum(targets[i] == targets) > 1) and (np.sum(mask_event) > 1))):
                            try:
                                if not (np.abs(event_info[mask_event][mask_check_duplicates][0,0]-targets[i]) < 
                                    np.abs(event_info[mask_event][~mask_check_duplicates][0,0]-targets[i])):
                                    # check if both target values are the same
                                    number_of_duplicates_targets +=1
                                    continue
                            except IndexError:
                                print('whaat')
                        tf_example = self.serialize_example(targets=targets[i] if self.classification == -1 else self.classification,
                                                            scalars=scalars[i],
                                                            multiply_output = multiply_output[i],
                                                            event_info=np.hstack(all_event_info) if (self.tag == 'Zmumugam') else event_info[i], 
                                                            track = tracks[i, :, :], 
                                                            images = image
                                                            )
                        writer.write(tf_example.SerializeToString())
                        number_of_events_saved += 1
        #             print('number_of_events_saved:', number_of_events_saved)
        print(f'Number of events saved in file {path}: {number_of_events_saved}')
        print(f'Number of duplicate targets remove from {path}: {number_of_duplicates_targets}')
        # except Exception as e:
        #     print('#'*10, path)
        #     print('######## Error:', e)

        return
    def create_normalization(self, data_paths, track_rows=15, transformation_file =False, truth_mask = []):
        # os.path.exists(dirs)
        self.norm_path = self.save_path+'/norm/'
        if (os.path.exists(self.save_path+'/norm/')) and ( not transformation_file):
            print('Normalization folder exists!')
            return None
        data={}
        for i in  self.data_params['scalar_names']:
            data[i] = np.array([])
        for i in  self.data_params['track_names']:
            data[i] = np.array([])
        for file in tqdm(data_paths, total = len(data_paths)):
            with h5py.File(file, "r") as df: # use the transformation_file to standization
                if not ('train' in df.keys()):
                    print('Not train sample in file - EXITING!')
                    sys.exit()
                elif not len(df['train']):
                    continue
                mask_true_events = true_events_in_data(path=file, set_name='train',
                                                       isMC='mc' in file, n=self.norm_size,
                                                       tag=self.tag, mass_cut=False)
                for i in self.data_params['scalar_names']: # import scalar variables
                    data[i] = np.r_[data[i], df['train'][i][:self.norm_size][mask_true_events]] # concat previous data
                tracks, _ = import_track(track_names =self.data_params['track_names'] ,df=df, set_name='train', norm_size=self.norm_size, rows=track_rows)
    
                for nr, i in enumerate(self.data_params['track_names']):
                    data[i] = np.r_[data[i], tracks[:,nr][mask_true_events]]
            if len(data[i]) >= self.norm_size:
                break
        print('Create new standardization')
        if False:
            for i in data.keys():
                print(i)
                if i.startswith('track'):
                    continue
                else:
                    data[i] = data[i][(data[i] > np.percentile(data[i], 1)) &
                                      (data[i] < np.percentile(data[i], 99))]
        if transformation_file:
            self.transformation_data = data
        else:
            standardization(data=data, data_params=self.data_params,
                    sample_name='train', dirs=self.save_path+'/norm/', tag=self.tag,
                    save=True)
        del data
        
    def tfrecord_parallel(self, set_name, size, data_path, save_path,
                          run_normalize=0, track_rows = 15, verbose=0):
        
        if verbose: print('%'*10, data_path, '%'*10)
        # This is an example observation from the dataset.t
        chunk_points = chunks(size, math.ceil(np.max(size)/self.nr_files))

        args = [[i, set_name, data_path, self.iterations, data_params, save_path, track_rows] for i in chunk_points]

        if self.nr_files>1: ## multi processing
            if self.nr_files > 20:
                cpu_cores = 20
            else:
                cpu_cores = self.nr_files
            with concurrent.futures.ProcessPoolExecutor(max_workers=cpu_cores) as executor:
                results = executor.map(self.create_single_tfrecord, args)
                [ _ for _ in results]
        else:
            for i in args:
                self.create_single_tfrecord(i)
                
    def transform_data(self,transformation_path):
        mask_true_data = true_events_in_data(path=transformation_path, set_name='train', isMC=self.isMC, n=-1, tag='Zee')
        self.create_normalization(data_paths=[transformation_path],
                                         transformation_file =True,
                                         track_rows=15, 
                                         truth_mask=mask_true_data)
        # self.transformation_data = data
                
    def create_tfrecords(self, save_path, nr_files, 
                         transformation_path:str=None,
                         iterations=1, parallel=False, nr_of_cores=10,
                         classification=False):
        self.nr_files = nr_files
        self.classification = 1 if classification=='mc' else 0 if classification=='data' else -1
        self.iterations = iterations
        self.save_path = save_path
        self.transformation_path = transformation_path
        self.transformation_data = transformation_path!=None
        self.create_normalization(data_paths=self.data_paths if not self.transformation_data else transformation_path,
                                  track_rows=15) # creating normalization folder
        if transformation_path != None:
            self.transform_data(transformation_path)
        if parallel:
            pool = concurrent.futures.ProcessPoolExecutor(nr_of_cores)
            future = []
        for nr, data_path in enumerate(self.paths):
            print(data_path)

            df = h5py.File(data_path, "r")
            var='p_fTG3'
            n_points = {}
            for i in ['train', 'test', 'val']:
                try:
                    n_points[i] = int(df[i][var].shape[0]-1) 
                except:
                    continue 
            df.close()

            print(n_points)
            for i in tqdm(n_points, disable=parallel):  
                name = f'{self.tag}_{i}'
                # if transformation_file:
                #     i +='_trans'
                if self.isMC:
                    save_path_sample = save_path+f'/{name}_mc/' if self.classification == -1 else save_path+f'/{i}_mc_classification/'
                else:
                    save_path_sample = save_path+f'/{name}_data/' if self.classification == -1 else save_path+f'/{i}_data_classification/'
                size = n_points[i]
                cpu_cores = nr_files
                if cpu_cores > 20:
                    self.cpu_cores = 20
                create_dir(dirs=save_path_sample, quit_script = False)
                if parallel:
                    future.append(pool.submit(self.tfrecord_parallel, i, n_points[i], data_path,
                                           save_path_sample,
                                           nr))
                else:
                    self.tfrecord_parallel(set_name=i, size=n_points[i], data_path=data_path,
                                           save_path=save_path_sample,
                                           run_normalize=nr)
        if parallel:
            for r in tqdm(concurrent.futures.as_completed(future), total = len(future)):
                r.result()
#%%
if __name__ == '__main__':  
    try:
        from .utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel, import_track
    except (ModuleNotFoundError, ImportError):
        from utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel, import_track
        
    sys.path.insert(1,'../invmfit')
    from peakfit_functions import invariant_mass

    os.environ["CUDA_VISIBLE_DEVICES"] = "-1" # -1 disable
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # cuda warnings

    plt.close('all')
    sys.path.append("../rundeepcalo/")
    data_path = '../root2hdf5/output/root2hdf5/Zee/Zee_data_2500e_combine_atlas_reco_mass/'# Zee_mc_GNN_images_combine Hyy/Hyy_both_combine/' # Hyy_mc_time_gain_combine Zee_data_2500e_combine_atlas_reco_mass
    # data_path = '../root2hdf5/output/root2hdf5/single_electron/Zee_mc_single_electron_combine/' # Hyy_mc_time_gain_combine
    # path = '../root2hdf5/output/root2hdf5/single_electron/Zee_mc_single_electron_combine/'# Zee_mc_time_gain_combine 'Zee_data_2500e_combine_atlas_reco_mass/' Zee_mc_time_gain_combine
    # path = '../root2hdf5/output/root2hdf5/Zee/Zee_mc_time_gain_combine/'# Zee_mc_time_gain_combine 'Zee_data_2500e_combine_atlas_reco_mass/' Zee_mc_time_gain_combine
    path = '../root2hdf5/output/root2hdf5/Zee/Zee_data_2500e_combine_atlas_reco_mass/'#Hyy/Hyy_both_combine/' # Zmumugam_mc_combine #Hyy_mc_time_gain_combine/' # Hyy Zee_data_image.h5  Hyy_mc_image
    # transformation_path = '../root2hdf5/output/root2hdf5/Zee/Zee_data_2500e_combine_atlas_reco_mass/Zee_data_image_1617206863.09759.h5'
    tag = 'Zee_single' if 'single' in path else 'Zee' if 'Zee' in path else 'Hyy' if 'Hyy' in path else 'Zmumugam' if 'Zmumugam' in path else print('problem with tag')
    isMC = 'mc' if 'mc' in path else 'data'
     
    save_path = f'../tfrecords_data/Zee_histogram_equalization'# Zee_w_new_track_var Zee_histogram_equalization Zmumugam_data Hyy Zee_mc_w_time_large_reweighted {tag}_{isMC}_classification' ###### folder name change {tag}_{isMC}_w_time_large_reweighted
    particle = 'electrons' if 'ee' in save_path else 'photons'
    data_conf = import_module(f'..{particle}_variables_conf',  'variables_params.subpkg')
    data_params = data_conf.get_params()
    file_or_folder = '.h5' in path
    if not file_or_folder:  
        paths = [path+f for f in listdir(path) if isfile(join(path, f))][:] #### 
        data_paths = [data_path+f for f in listdir(data_path) if isfile(join(data_path, f))][:] #### 
    #%%
    # =============================================================================
    #     creating datafiles
    # =============================================================================
    if True:
        data_creation = TfrecordsFileCreation(paths=paths,
                                              data_params=data_params,
                                              tag=tag,
                                              norm_size=1_000_000,
                                              data_paths=data_paths)
        data_creation.create_tfrecords(save_path=save_path, nr_files=1, 
                                        transformation_path = None, #transformation_path,
                                        iterations=10, parallel=True, nr_of_cores=20,    
                                        classification=False)
    else:
        df = h5py.File(glob(path+'*')[0], 'r+')
        
        
        
        
        
