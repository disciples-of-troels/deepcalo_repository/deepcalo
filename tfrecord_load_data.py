#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 16:07:27 2020

@author: malte
"""
import tensorflow as tf
import numpy as np
import h5py
from tqdm import tqdm
import os
import concurrent.futures
import sys
from importlib import import_module
import matplotlib.pyplot as plt
import pandas as pd
from collections import Counter
from sklearn.preprocessing import StandardScaler,RobustScaler, PowerTransformer, QuantileTransformer

import matplotlib.pylab as pylab
params = {
            'axes.labelsize': 'x-large',
            'axes.titlesize':'x-large',
          'xtick.labelsize':'x-large',
          'ytick.labelsize':'x-large'}
pylab.rcParams.update(params)

sys.path.insert(1,'../deepcalo')
try:
    from .utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel
except (ModuleNotFoundError, ImportError):
    from utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel


def create_dir(*, dirs):
    if not os.path.exists(dirs):
        print('Creating folder:')
        print(dirs)
        os.makedirs(dirs)
    else:
        print('Folder already existing!')
        sys.exit()
        
def chunks(lst, n):
    if type(lst) == int:
        lst = range(lst)
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def cal_invM(e1, e2, eta1,eta2, phi1,phi2, e3=None, eta3= None, phi3=None):
    from skhep.math import vectors
    vecFour1 = vectors.LorentzVector()
    vecFour2 = vectors.LorentzVector()
    vecFour1.setptetaphim(e1/np.cosh(eta1), eta1, phi1, 0)
    vecFour2.setptetaphim(e2/np.cosh(eta2), eta2, phi2, 0)
    if (e3 != None) & (phi3 != None) & (eta3 != None):
        vecFour3 = vectors.LorentzVector()
        vecFour3.setptetaphim(e3/np.cosh(eta3), eta3, phi3, 0)
        vecFour = vecFour1+vecFour2+vecFour3
    else:
        vecFour = vecFour1+vecFour2
    return vecFour.mass


def sigmoid(x, sigma =0.2, mean= 35):
    return 1/(1+(tf.math.exp(-sigma*(-x+mean))))
# =============================================================================
# Load tfrecords
# =============================================================================
def func(args, target):
    args['scalars'] = args['scalars']
    return args, target

def load_dataset(filenames, tag,data_params, batch_size=None, load_single_file=False, shuffle=0, merge=True,
                 additional_info=True, cache=False, multiple_with=True, cut_ratio=None,
                 training_in_data=False, autotune = tf.data.experimental.AUTOTUNE,
                 time_lr: bool = False, ext_energy_range=None): # https://www.tensorflow.org/api_docs/python/tf/data/experimental/OptimizationOptions

    options = tf.data.Options()

    options.experimental_deterministic = not shuffle!=0
    options.experimental_threading.private_threadpool_size = 16
    
    file_path = tf.data.Dataset.list_files(filenames, shuffle=shuffle!=0
                                           ).with_options(options)
    # print(file_path.cardinality().numpy())
    dataset = tf.data.TFRecordDataset(
        file_path, compression_type='GZIP', num_parallel_reads=1 if load_single_file else autotune #autotune
        ).prefetch(buffer_size=autotune)  # automatically interleaves reads from multiple files tf.data.experimental.AUTOTUNE
    
    if ('Zee_mc_w_time_large_reweighted' in filenames[0]):
        dataset = dataset.map(lambda x: read_tfrecord_old(x, tag, merge=merge,
                                                      additional_info=additional_info,
                                                      multiple_with=multiple_with,
                                                      time_lr=time_lr),
                              num_parallel_calls=autotune)
        if (cut_ratio != None) & (ext_energy_range == None):
            dataset = dataset.filter(lambda x, label: tf.math.abs(x['event_info'][0]/label-1) < cut_ratio)
            
    else:
        if len(data_params.keys()) == 1:
            dataset = dataset.map(lambda x: read_tfrecord_single(x, tag, merge=merge,data_params=data_params),
                                                  num_parallel_calls=autotune)            
        else:
            dataset = dataset.map(lambda x: read_tfrecord_new(x, tag, merge=merge,data_params=data_params),
                                                              num_parallel_calls=autotune)
    

        if ext_energy_range != None:
            if ext_energy_range > 0:
                dataset = dataset.filter(lambda x, label: (sigmoid(x['event_info'][4]/tf.math.cosh(x['event_info'][1])) >= tf.random.uniform([1]) if x['event_info'][5] == 0 else True)
                                          and (tf.math.abs(x['event_info'][0]/label-1) < cut_ratio if x['event_info'][5] == 1 else True))
            else:
                dataset = dataset.filter(lambda x, label: x['event_info'][4]/tf.math.cosh(x['event_info'][1]) >= np.abs(ext_energy_range)+5)
                
        if (cut_ratio != None) & (ext_energy_range == None):
            if (tag == 'Zmumugam'):
                print('THERE MIGHT BE PROBLEMS WITH ZMUMUGAM Data!!!!!!!')
                dataset = dataset.filter(lambda x, label: tf.math.abs(x['event_info'][0]/label-1) < cut_ratio if x['event_info'][-1] == 1 else True)
            else:
                dataset = dataset.filter(lambda x, label: tf.math.abs(x['event_info'][0]/label-1) < cut_ratio if x['event_info'][5] == 1 else True)
            
    if shuffle:
        if isinstance(shuffle, bool):
            dataset = dataset.shuffle(12, reshuffle_each_iteration=True)
        else:
            dataset = dataset.shuffle(shuffle, reshuffle_each_iteration=True)
        # elif ext_energy_range > 0:
        #     dataset = dataset.filter(lambda x, label: label/tf.math.cosh(x['event_info'][1]) < ext_energy_range if x['event_info'][5] == 0 else True)
        # elif ext_energy_range < 0:
        #     print('Getting E_T >:', -ext_energy_range)
        #     dataset = dataset.filter(lambda x, label: label/tf.math.cosh(x['event_info'][1]) > np.abs(ext_energy_range))

    if batch_size is not None: dataset = dataset.batch(batch_size, drop_remainder=True)
    if cache:
        dataset = dataset.cache()    
    return dataset

def read_tfrecord_single(example, tag, data_params, merge=True, log=False):
    tfrecord_format = {}
    for i in data_params.keys():
        for sca in data_params[i]:
            tfrecord_format[sca] = tf.io.FixedLenFeature([], tf.float32)

    dataset = tf.io.parse_single_example(example, tfrecord_format)
    data = {name: tf.cast(dataset[name], tf.float32, name=name) for name in tfrecord_format}
    
    for i in ['track_names', 'scalar_names']:
        if i in data_params.keys():
            tracks = [data[i] for i in data_params[i]]
            data[i.split('_')[0]+'s']  = tf.stack(tracks, axis=-1 if i=='track_names' else 0)
            [data.pop(i) for i in data_params[i]]
    # print(data)
    return data['scalars']#, data['scalars']

def read_tfrecord_new(example, tag, data_params, merge=True, log=False):
    data ={}
    tfrecord_format = {
                    "em_barrel_Lr0": tf.io.FixedLenFeature([56,11,], tf.float32),
                    "em_barrel_Lr1": tf.io.FixedLenFeature([56,11,], tf.float32),
                    "em_barrel_Lr2": tf.io.FixedLenFeature([56,11,], tf.float32),
                    "em_barrel_Lr3": tf.io.FixedLenFeature([56,11,], tf.float32),
                    "targets": tf.io.FixedLenFeature([], tf.float32),
                    "multiply_output_name": tf.io.FixedLenFeature([], tf.float32),
                    "type": tf.io.FixedLenFeature([ ], tf.float32)
                        }
    for sca in data_params['scalar_names']:
        if sca == 'type':
            continue
        tfrecord_format[sca] = tf.io.FixedLenFeature([], tf.float32)
    for tra in data_params['track_names']:
        tfrecord_format[tra] = tf.io.FixedLenFeature([15,], tf.float32)
    for gate in data_params['gate_img_prefix']:
        names = ['_Lr0', '_Lr1', '_Lr2', '_Lr3']
        for i in names:
            tfrecord_format[gate+i] = tf.io.FixedLenFeature([56,11,], tf.float32)
    if data_params['additional_info'] is not None:
        if (tag=='Zmumugam'):
            tfrecord_format['event_info'] = tf.io.FixedLenFeature([15,], tf.float32)
        else:
            tfrecord_format['event_info'] = tf.io.FixedLenFeature([5,], tf.float32)
    dataset = tf.io.parse_single_example(example, tfrecord_format)
    data = {name: tf.cast(dataset[name], tf.float32, name=name) for name in tfrecord_format.keys()}
    
    # add variables to event_info
    if data_params['additional_info'] is not None:
        types = tf.reshape(data['type'],[1])
        data['event_info'] = tf.concat([data['event_info'], types], axis=-1)
        if not any([i == 'type' for i in data_params['scalar_names']]):
            data.pop('type')
    # create target
    label = data['targets']
    data.pop('targets')
    
    if merge: # merge all the images together so the channel = 4
        if log:
            data['em_barrel']  = tf.stack([tf.math.log(data['em_barrel_Lr0']+10e-15),
                                           tf.math.log(data['em_barrel_Lr1']+10e-15),
                                           tf.math.log(data['em_barrel_Lr2']+10e-15),
                                           tf.math.log(data['em_barrel_Lr3']+10e-15)], axis=-1)            
        else:
            data['em_barrel']  = tf.stack([data['em_barrel_Lr0'], data['em_barrel_Lr1'], data['em_barrel_Lr2'], data['em_barrel_Lr3']], axis=-1)
        [data.pop(i) for i in ["em_barrel_Lr0", "em_barrel_Lr1", "em_barrel_Lr2", "em_barrel_Lr3"]]
        # if 'time_em_barrel_Lr0' in tfrecord_format.keys():     
        for gate in data_params['gate_img_prefix']:
            data[gate]  = tf.stack([data[gate+'_Lr0'], data[gate+'_Lr1'], data[gate+'_Lr2'], data[gate+'_Lr3']], axis=-1)
            [data.pop(i) for i in [gate+'_Lr0', gate+'_Lr1', gate+'_Lr2', gate+'_Lr3']]
            # data[gate] = tf.math.abs(data[gate])# <= 0.05 ## abs
    for i in ['track_names', 'scalar_names']:
        if data_params[i]:
            tracks = [data[i] for i in data_params[i]]
            data[i.split('_')[0]+'s']  = tf.stack(tracks, axis=-1 if i=='track_names' else 0)
            [data.pop(i) for i in data_params[i]]
    
    return data, label


def read_tfrecord_old(example, tag, merge=True, multiple_with=True,
                  additional_info = True, eventnumber=False,
                  time_lr:bool=False, use_type=False):
    
    if (tag=='Hyy') | (tag=='Zmumugam'):
        shapes = [11,]
    elif tag=='Zee':
        shapes = [16,] # w type 17 ellers 16
    else:
        print('Tag should be [Zee, Hyy, Zmumugam]')
        print('Tag not known - EXITING!')
        sys.exit()
    data ={}
    tfrecord_format = {
                    "em_barrel_Lr0": tf.io.FixedLenFeature([56,11,], tf.float32),
                    "em_barrel_Lr1": tf.io.FixedLenFeature([56,11,], tf.float32),
                    "em_barrel_Lr2": tf.io.FixedLenFeature([56,11,], tf.float32),
                    "em_barrel_Lr3": tf.io.FixedLenFeature([56,11,], tf.float32),
                    "scalars": tf.io.FixedLenFeature(shapes, tf.float32),
                    # "targets": tf.io.FixedLenFeature([], tf.int64),
                    "targets": tf.io.FixedLenFeature([], tf.float32),
                    "multiply_output_name": tf.io.FixedLenFeature([], tf.float32)
                        }
    if use_type:
        tfrecord_format['type'] = tf.io.FixedLenFeature([], tf.float32)
    if time_lr:
        time_image_names = ['time_em_barrel_Lr0', 'time_em_barrel_Lr1', 'time_em_barrel_Lr2', 'time_em_barrel_Lr3']
        for i in time_image_names:
            tfrecord_format[i] = tf.io.FixedLenFeature([56,11,], tf.float32)
    if additional_info:
        # adding the event info to the tfrecord format
        tfrecord_format['tracks'] = tf.io.FixedLenFeature([10,13,1,], tf.float32)
        if (tag=='Zmumugam'):
            tfrecord_format['event_info'] = tf.io.FixedLenFeature([15,], tf.float32)
        else:
            tfrecord_format['event_info'] = tf.io.FixedLenFeature([5,], tf.float32)
        dataset = tf.io.parse_single_example(example, tfrecord_format)
        data['event_info'] = tf.cast(dataset["event_info"], tf.float32, 'event_info')
        data['tracks']= tf.cast(dataset["tracks"], tf.float32, 'tracks')
    else:
        dataset = tf.io.parse_single_example(example, tfrecord_format)  
    if use_type:
        data['type']= tf.cast(dataset["type"], tf.float32, 'type')
        
    names = ['em_barrel_Lr0', 'em_barrel_Lr1', 'em_barrel_Lr2', 'em_barrel_Lr3']
    label = tf.cast(dataset["targets"], tf.float32, 'targets')
    # label = tf.cast(dataset["targets"], tf.int64, 'targets')
    # images = tf.io.decode_raw(dataset["image_raw"], 'double')
    for i in names:
        data[i] = tf.cast(dataset[i], tf.float32, name=i)
    if 'time_em_barrel_Lr0' in tfrecord_format.keys():      
        for i in time_image_names:
            data[i] = tf.cast(dataset[i], tf.float32, name=i)
            
    if merge: # merge all the images together so the channel = 4
        data['em_barrel']  = tf.stack([data['em_barrel_Lr0'], data['em_barrel_Lr1'], data['em_barrel_Lr2'], data['em_barrel_Lr3']], axis=-1)
        [data.pop(i) for i in names]
        if 'time_em_barrel_Lr0' in tfrecord_format.keys():                
            data['time_em_barrel']  = tf.stack([data['time_em_barrel_Lr0'], data['time_em_barrel_Lr1'], data['time_em_barrel_Lr2'], data['time_em_barrel_Lr3']], axis=-1)
            [data.pop(i) for i in time_image_names]
            
            data['time_em_barrel'] = data['time_em_barrel']#tf.math.abs()# <= 0.05 ## abs
    if multiple_with:
        data['multiply_output_name'] = tf.cast(dataset["multiply_output_name"], tf.float32, 'multiply_output_name')
        
    data['scalars'] = tf.cast(dataset["scalars"], tf.float32, 'scalars')
    return data, label

# @tf.function()
def clean_batch(y_pred, number_of_cores=1, nr=0):
    if y_pred.shape[1] == 6:
        col_nr = 2
    else:
        col_nr = 1
        # if nr == 281:
        #     print('problems')
        test = y_pred
    # mask_eventNumber2 = y_pred[0,col_nr] != y_pred[number_of_cores,col_nr]
    # # tf.print(y_pred.shape)
    # # tf.print(y_pred[0], y_pred[number_of_cores])    
    # if mask_eventNumber2:
    #     tf.print('smal prob')
    #     y_pred = y_pred[number_of_cores:len(y_pred)-number_of_cores,:]
    # tf.print('--', y_pred[:2])      
    mask_eventNumber1 = y_pred[0,col_nr] != y_pred[1,col_nr]
    if (number_of_cores > 1) and (mask_eventNumber1):
        # if mask_eventNumber1:
        # tensors = []
        # for i in tf.range(number_of_cores):
        #     tf.print('iter', i)
        if (y_pred[0,col_nr] != y_pred[1,col_nr]) and (y_pred[0,col_nr] != y_pred[2,col_nr]):
            y_pred = y_pred[1:-1,:]
        # k1 = y_pred[0::number_of_cores]
        # k2 = y_pred[1::number_of_cores]
        # k3 = y_pred[2::number_of_cores]
        # k4 = y_pred[3::number_of_cores]
            
        # print('number', tensors)
        # tf.print('length', len(tensors))
        # # tf.print(tensors)
        # if len(tensors) > 1:
        #     tf.print(tensors)
        # y_pred = tf.concat([k1,k2,k3,k4], axis=0)
        
    if tf.argmax(~(y_pred[:,col_nr][::2] == y_pred[:,col_nr][1::2])) != 0:
        while tf.argmax(~(y_pred[:,col_nr][::2] == y_pred[:,col_nr][1::2])) != 0:
            remove_bad_events = ~(tf.range(len(y_pred), dtype=tf.int64) == tf.argmax(~(y_pred[:,col_nr][::2] == y_pred[:,col_nr][1::2]))*2)
            y_pred = y_pred[remove_bad_events]
            y_pred = y_pred[:-3]

        
    if y_pred.shape[1] == 5:
        y_pred = tf.reshape(y_pred, (int(len(y_pred)/2), 10), name='pred')
        test2 = y_pred.numpy()
        mask_eventNumber = (y_pred[:,1] == y_pred[:,6])
        tf.print('eventnumber', tf.math.reduce_sum(tf.dtypes.cast(mask_eventNumber, dtype = tf.int16)))
        if tf.math.reduce_sum(tf.dtypes.cast(mask_eventNumber, dtype = tf.int16)) < 2000:
            print('Problem')
    return y_pred

#%%
if __name__ == '__main__': 

    from glob import glob
    plt.close('all')
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1" #p -1 disable
    from tfrecord_data_creation import *
    sys.path.insert(1,'../root2hdf5')
    from from_event2row import reconstruct_E_truth
    if True:
        if os.getcwd() == '/home/malteal/work/Master/deepcalo':
            sys.path.append("/home/malteal/work/Master/run/train_model_malteal/")
        else:
            sys.path.append("../run/train_model_malteal/")
            
        sys.path.append("../rundeepcalo/")
        scalars = {}
        tracks={}
        targets = {}
        ET={}
        event_info = {}
        paths = ['Zee_w_new_track_var']#['Zee_new_standardization_v1', 'Zee_mc_w_time_large_reweighted', 'Zee_normalization_from_data']#['Zee_mc_w_time_large_reweighted', 'Zee_mc_w_time_large_reweighted', 'Zee_new_standardization']#, 'Zee_mc_w_time_large_reweighted', 'Zee_mc_w_time_large_reweighted'] # Zee_mc_w_time_large_reweighted
        color = ['b','b','g','g',]
        # fig, axes = plt.subplots(1,2)
        ext_range = [None, None, None, None]
        set_names = ['Zee_train_mc', 'Zmumugam_test_mc', 'Zee_train_data', 'Zee_train_mc']#['Zee_train_mc', 'Zee_testing_dist_train_mc', 'Zee_train_mc']#, 'Zee_train_mc', 'Zee_train_data']
        iterations=0
        for set_name, path in zip(set_names, paths):
            files = glob(f'../tfrecords_data/{path}/{set_name}/*.tfrecords') ## change dir
            # files = [f'../tfrecords_data/{path}/{set_name}/small_Zee_mc_image_1611693870_train_Zee_407753_images_1623358282.9865642.tfrecords']
            tag = 'Zee' if 'Zee' in set_name else 'Hyy' if 'Hyy' in set_name else 'Zmumugam'
            
            # data_path = f'../tfrecords_data/{tag}'
            particle = 'electrons' if tag=='Zee' else 'photons'
            data_conf = import_module(f'..{particle}_variables_conf',  'variables_params.subpkg')
            data_params = data_conf.get_params()
            data_params['gate_img_prefix'] = []
            if True:
                print(set_name, path)
                size = 0
                target = []
                total_size=[]
                data=[]
                scalar = []
                nr_of_cores= 8
                track = []
                energy=[]
                images=[]

                train= load_dataset(files,#files[:1],
                                    tag=tag, shuffle=False, merge=True, load_single_file=True,
                                    additional_info=True, autotune = nr_of_cores,
                                    batch_size=512, time_lr = data_params['gate_img_prefix'], 
                                    data_params=data_params, cut_ratio=0.6,
                                    ext_energy_range=ext_range[iterations]).prefetch(buffer_size=tf.data.experimental.AUTOTUNE)

                n=10000
                nr =0
                time=[]
                
                for t, e in tqdm(train.take(n), total = n):
                    size += len(e)
                    evt_info =t['event_info'].numpy()
                    target.append(e.numpy())
                    energy.append(evt_info[:,4]/np.cosh(evt_info[:,1]))
                    data.append(evt_info)
                    scalar.append(t['scalars'].numpy())
                    track.append(t['tracks'].numpy())
                    images.append(t['em_barrel'].numpy())
                    nr +=1
                    #%%
                print('#####', size)
                total_size.append(size)
                data = np.vstack(data)
                scalar = np.vstack(scalar)
                energy = np.hstack(energy)
                target = np.hstack(target)
                track= np.vstack(np.vstack(track))
                track.shape = (len(track), track.shape[1])
                scalars[path+'_'+set_name] = scalar
                tracks[path+'_'+set_name] = track
                targets[path+'_'+set_name] = target
                event_info[path+'_'+set_name] = data
                ET[path+'_'+set_name] = energy
                ### calculate invariant mass of Zmumugam
                print(total_size)
                print(np.sum(total_size))
                #%%
                if False:
                    colors = matplotlib.rcParams["axes.prop_cycle"].by_key()['color'][1:]
                    value = event_info[list(event_info.keys())[0]][:,0]/target
                    style = {'bins': 150, 'range': [0, 3.5]}
                    style1 = {'bins': 150, 'range': [0, 400], 'linewidth':1.5}
                    if False:
                        fig, ax = plt.subplots(2,1, figsize=(5, 7))
                        ax[0].hist(event_info['Zee_w_new_track_var_Zee_single_train_mc'][:,1], **style, histtype='step', color='#aa0233', label=r'$\eta$')
                        ax[0].legend()
                        ax[0].set_xlabel(r'$\eta$')
                        ax[0].set_ylabel('#')
                        ax[1].hist(target, **style1, histtype='step',color='#aa0233', label=r'$E_{truth}$', density=False)
                        ax[1].legend()
                        ax[1].set_xlabel(r'$E_{truth}$ [GeV]')
                        ax[1].set_ylabel('#')
                        plt.tight_layout()
                    
                    import matplotlib 
                    fig, ax = plt.subplots(1,2, figsize=(10, 5))
                    ax[0].hist(value, **style, histtype='step', linewidth=2)
                    ax[0].set_yscale('log')
                    ax[1].hist(target, **style1, histtype='step')
                    ax[1].set_yscale('log')
                    ax[1].set_ylabel('#')
                    ax[1].set_xlabel(r'$E_{truth}$')
                    ax[0].set_xlabel(r'$|1-\frac{E_{ACC}}{E_{truth}}|$')
                    ax[0].set_ylabel('#')
                    for nr, i in enumerate([0.6, 0.4, 0.2, 0.1]):
                        mask = np.abs(1-event_info[list(event_info.keys())[0]][:,0]/target)<i
                        ax[0].hist((event_info[list(event_info.keys())[0]][:,0]/target)[mask], **style, color=colors[nr])
                        ax[1].hist(target[mask], **style1, histtype='step', color=colors[nr], label=f'k={i}')
                        ax[1].legend(loc='center left', bbox_to_anchor=(1, 0.5))
                    fig.tight_layout()
                
                # sys.exit()
                mass_func = []
                # axes[iterations%2].hist(energy, bins = 300, range=(0, 100),
                #                         alpha = 0.5, label=set_name+', median: '+str(np.median(energy)),
                #                         color = color[iterations], density=True)
                # if iterations%2 !=1:
                #     axes[iterations%2].vlines(np.median(energy), 0, 2, linestyle='dashed', color = color[iterations])
                
                # sys.exit()
                if tag == 'Zmumugam':
                    if True:
                        for i in [0,5,10]:
                            data[:,i] = np.abs(data[:,i])
                        name = ['recon']
                        for nr, energy in enumerate([data[:,0]]): # targets['Zee_w_new_track_var_Zmumugam_test_mc'], 
                            if set_name.split("_")[-1] == 'data':
                                px1, px2= data[:,5]*np.cos(data[:,7])/np.cosh(data[:, 6]), data[:,10]*np.cos(data[:,12])/np.cosh(data[:, 11])
                                py1, py2 =data[:,5]*np.sin(data[:,7])/np.cosh(data[:, 6]), data[:,10]*np.sin(data[:,12])/np.cosh(data[:, 11])
                                pz1, pz2 = data[:,5]*np.sinh(data[:,6])/np.cosh(data[:, 6]), data[:,10]*np.sinh(data[:,11])/np.cosh(data[:, 11])
                                mass = np.sqrt((np.sum([data[:,5], data[:,10]], axis=0)**2
                                                -np.sum([px1, px2 ], axis=0)**2
                                                -np.sum([py1, py2], axis=0)**2
                                                -np.sum([pz1, pz2], axis=0)**2))
                                mask = (20<mass)&(mass < 82)
                            else: 
                                mask = np.array([True]*len(data))
                            px1, px2, px3 = energy*np.cos(data[:,2])/np.cosh(data[:, 1]), data[:,5]*np.cos(data[:,7])/np.cosh(data[:, 6]), data[:,10]*np.cos(data[:,12])/np.cosh(data[:, 11])
                            py1, py2, py3 = energy*np.sin(data[:,2])/np.cosh(data[:, 1]), data[:,5]*np.sin(data[:,7])/np.cosh(data[:, 6]), data[:,10]*np.sin(data[:,12])/np.cosh(data[:, 11])
                            pz1, pz2, pz3 = energy*np.sinh(data[:,1])/np.cosh(data[:, 1]), data[:,5]*np.sinh(data[:,6])/np.cosh(data[:, 6]), data[:,10]*np.sinh(data[:,11])/np.cosh(data[:, 11])
                            mass = (np.sum([energy,data[:,5], data[:,10]], axis=0)**2
                                            -np.sum([px1, px2, px3], axis=0)**2
                                            -np.sum([py1, py2, py3], axis=0)**2
                                            -np.sum([pz1, pz2, pz3], axis=0)**2)
                        
                
                        # for i in tqdm(range(len(data))):
                        #     mass_func.append(cal_invM(e1=data[i,0], e2=data[i,5], e3=data[i,10],
                        #                               eta1=data[i,1],eta2=data[i,6],eta3=data[i,11],
                        #                               phi1=data[i,2],phi2=data[i,7], phi3=data[i,12]))
                            # plt.hist(np.sqrt(mass), range = (60, 120), bins  = 100, alpha = 0.5, label=name[nr])
                        plt.legend()
                    #%%
                    # plt.figure()
                    energy = data[:,0]
                    plt.hist(energy[mask]/np.cosh(data[:, 1][mask]), bins = 200, range=[0, 100], label = r'$E$calib$^{(BDT)}$'+f' {set_name.split("_")[-1]}',
                             # color = '#0b80c3'
                             alpha = 0.5, density = True)
                    plt.ylabel('#')
                    plt.xlabel(r'$E_T$ [GeV]')
                    # plt.vlines(10, 0, 8600, color='black', linestyle='dashed')
                    # plt.vlines(35, 0, 8600, color='black', linestyle='dashed', label = r'$E_T$ = [10, 35]')
                    # plt.vlines(70, 0, 8600, color='red', linestyle='dashed')
                    # plt.vlines(140, 0, 8600, color='red', linestyle='dashed', label = r'$E_T$ = [70, 140]')
                    plt.legend(title=r'Z$\rightarrow \mu \mu \gamma$')
                    # plt.yscale('log')
                    #%%
                elif False:#tag == 'Zee':
                    probe1 = data[::2]
                    probe = data[1::2]
                    for i in tqdm(range(len(probe1))):
                        mass_func.append(cal_invM(e1=probe1[i,0], e2=probe[i,0],
                                                  eta1=probe1[i,2],eta2=probe[i,2],
                                                  phi1=probe1[i,3],phi2=probe[i,3]))
                else:
                    print('tag unknown')
                    # sys.exit()
            iterations += 1
        # for i in range(2):
        #     axes[i].legend()#title=r'E$_{ACC}$')
        #     axes[i].set_xlabel(r'E$_{ACC}$ [GeV]')
        #     axes[i].set_ylabel('#')
            # axes[i].set_yscale('log')
        #%%
        if False: ## plot variables
            plt.close('all')
            
            style = {'bins': 100, 'density': True, 'histtype': 'step',
                     'linewidth': 3, 'alpha': 0.5,}
            value = scalars.copy()
            for i in range(scalars[list(scalars.keys())[0]].shape[1]):
                plt.figure()
                keys = list(scalars.keys())
                # style['range'] = (np.percentile(value[keys[0]][:,0],i), np.percentile(value[keys[0]][:,i],100))
                for name, type in zip(keys, ['solid', 'dotted', 'dashed']):
                    plt.hist(value[name][:,i], label=name, **style, linestyle=type)
                plt.title(data_params['track_names'][i])
                try:
                    plt.yscale('log')
                except:
                    pass
                plt.legend()
                # plt.show()
                #%%
        if False:
            plt.close('all')
            from sklearn.decomposition import PCA
            pca = PCA(3)
            X_test = scalars['Zee_new_standardization_v1_Zee_train_mc']
            X_train = scalars['Zee_new_standardization_v1_Zee_train_data']
            
            x_pca = pca.fit(X_train)
            X_train = pd.DataFrame(x_pca.transform(X_train))
            # X_train.columns=['PC1','PC2', 'PC3']
            X_test = pd.DataFrame(x_pca.transform(X_test))
            # X_test.columns=['PC1','PC2', 'PC3']
            style = {'bins': 100, 'density': True, 'histtype': 'step',
                     'linewidth': 3, 'alpha': 0.5}
            for i in X_test.columns:
                plt.figure()
                plt.hist(X_test[i], label='mc', color = 'red', **style)
                plt.hist(X_train[i], label = 'data', color = 'green', **style) 
                plt.yscale('log')
                plt.legend()
            # Plot
            # import matplotlib.pyplot as plt
            # plt.scatter(X_train.iloc[:,2], X_train.iloc[:,1], c='green', alpha=0.8)
            # plt.scatter(X_test.iloc[:,2], X_test.iloc[:,1], c='red', alpha=0.8)
            # plt.title('Scatter plot')
            # plt.xlabel('x')
            # # plt.yscale('log')
            # # plt.xscale('log')
            # plt.show()
        if True:
            reco_mass, reco_energy, mask_reco = reconstruct_E_truth(eventNumber=event_info[list(event_info.keys())[0]][:,3], 
                                                         p_eta=event_info[list(event_info.keys())[0]][:,1],
                                                         p_phi=event_info[list(event_info.keys())[0]][:,2],
                                                         p_e=event_info[list(event_info.keys())[0]][:,0], number_of_decay_particles=None)
            #%%
            from matplotlib.colors import LogNorm
            fig, ax = plt.subplots(1,2, figsize=(12, 7), sharey=True)
            reco_energy = np.array(reco_energy).flatten()
            mask_reco=np.array(mask_reco).flatten()
            eta= event_info[list(event_info.keys())[0]][:,1][mask_reco]
            figsize = (12,8)
            # nr=0
            e_max=100
            e_min=5
            max_events = []
            for energy in [event_info[list(event_info.keys())[0]][:,4][mask_reco],
                           reco_energy[mask_reco]]:   
                mask = (energy/np.cosh(eta) < e_max) & (targets[list(event_info.keys())[0]][mask_reco]/np.cosh(eta) < e_max)
                extent = np.array([[e_min, e_max], [e_min, e_max]])
                heatmap, xedges, yedges = np.histogram2d(targets[list(event_info.keys())[0]][mask_reco]/np.cosh(eta),
                                                         energy/np.cosh(eta),
                                                         bins=100,
                                                         range = extent)    
                max_events.append(np.max(heatmap))
            # fig, ax = plt.subplots(1,1,figsize=figsize, sharey=True) 
            for nr, energy in enumerate([event_info[list(event_info.keys())[0]][:,4][mask_reco],
                           reco_energy[mask_reco]]):   
                mask = (energy/np.cosh(eta) < e_max) & (targets[list(event_info.keys())[0]][mask_reco]/np.cosh(eta) < e_max)
                extent = np.array([[e_min, e_max], [e_min, e_max]])
                heatmap, xedges, yedges = np.histogram2d(targets[list(event_info.keys())[0]][mask_reco]/np.cosh(eta),
                                                         energy/np.cosh(eta),
                                                         bins=100,
                                                         range = extent)
                # for i,j in product(xedges, yedges)
                if nr:
                    name = 'Correlation between $E_{label, mc}$ and $E_{truth}$'
                    name1 = 'Reconstructed energy'
                    name2 = r'$E_{T, truth, mc}$ [GeV]'
                else:
                    name = 'Correlation between $E_{acc}$ and $E_{truth}$'
                    name1 = 'Accordion energy'
                    name2 = r'$E_{T, acc}$ [GeV]'
                heatmap +=1
                neg = ax[nr].imshow(heatmap.T, extent=extent.flatten(), origin='lower',
                              label=name, #norm=LogNorm(vmin=1,vmax=np.max(heatmap)),
                              cmap = 'tab10')
                # if nr:
                #     cbar = fig.colorbar(neg, location='right')
                #     cbar.set_label('Log scale - Number of events in bins',size=18)
                ax[nr].plot([e_min, e_max], [e_min, e_max], color = 'black',
                            linestyle = 'dashed')
                    
                ax[nr].legend(title=name1, loc=2, title_fontsize=20)
                ax[nr].set_xlabel(r'$E_{T,truth}$ [GeV]', fontsize=20)
                ax[nr].set_ylabel(name2, fontsize=20)
                fig.tight_layout()
            cbar = fig.colorbar(neg,  ax=ax.ravel().tolist(), aspect=50, pad=0.1,
                                orientation='horizontal')
            cbar.set_label('# of events + 1')
                # nr+=1
                #%%
            plt.figure()
            energy = (reco_energy)/targets[list(event_info.keys())[0]]
            energy_acc = (event_info[list(event_info.keys())[0]][:,4])/targets[list(event_info.keys())[0]]
            plt.hist(energy[mask_reco], bins=200, range=(1-0.25, 1+0.25), label=f'Reconstruct E, IQR: {round(np.percentile(energy, 75)-np.percentile(energy, 25),3)}', alpha=0.7)
            plt.hist(energy_acc[mask_reco], bins=200, range=(1-0.25, 1+0.25), label=f'Accordion E, IQR: {round(np.percentile(energy_acc, 75)-np.percentile(energy_acc, 25),3)}', alpha=0.7)
            plt.legend()
            plt.xlabel('RE')
            plt.ylabel('#')
            fig.tight_layout()
        #%%
    elif False:
        #%%
# =============================================================================
#         check distribution of variables
# =============================================================================
        # plt.close('all')
        files = glob.glob('../root2hdf5/output/root2hdf5/Zee/Zee_mc_time_gain_combine/*.h5')
        files_data = glob.glob('../root2hdf5/output/root2hdf5/Zee/Zee_data_2500e_combine_atlas_reco_mass/*.h5')        
        tag = 'Zee' if 'Zee' in files[0] else 'Hyy' if 'Hyy' in files[0] else 'Zmumugam'
        
        # data_path = f'../tfrecords_data/{tag}'
        particle = 'electrons' if tag=='Zee' else 'photons'
        data_conf = import_module(f'..{particle}_variables_conf',  'variables_params.subpkg')
        data_params = data_conf.get_params()
        data_params['gate_img_prefix'] = []
        df = h5py.File(files[0])['train']
        df_data = h5py.File(files_data[0])['train']
        style = {'bins': 100, 'density': True,
                 'histtype': 'step', 'linewidth':2}
        n=100_00
        mask_mc = true_events_in_data(files[0], set_name='train', isMC=True, n=n)
        mask_data = true_events_in_data(files_data[0], set_name='train',
                                        isMC=False,n=n)
        fig, axes = plt.subplots(5,3, figsize=(20,10))
        fig.tight_layout()
        axes = axes.flatten()
        data_params['track_names'].append('tile_gap_Lr1')
        for i, ax in zip(data_params['track_names'], axes):
            # plt.hist(df[i][:n][mask_mc], label='mc', **style)
            # plt.hist(df_data[i][:n][mask_data], label='data', **style)
            mc = np.hstack(df[i][:n][mask_mc])
            data = np.hstack(df_data[i][:n][mask_data])
            ax.hist(mc, label='MC', **style, range=[np.quantile(mc, 0.01), np.quantile(mc, 0.99)], color='blue')
            ax.hist(data, label= 'Data', **style, range=[np.quantile(mc, 0.01), np.quantile(mc, 0.99)], color='red')
            # print(i, np.max(data))
            ax.set_title(i)
            ax.set_yscale('log')
            # plt.xscale('log')
            ax.legend()
            # plt.show()
        # fig.savefig('../Master Thesis/pics/dataproc/scalar_variable_distributions.eps', format='eps')
    #%%
    elif False:
        files = glob.glob('../root2hdf5/output/root2hdf5/Zmumugam/Zmumugam_data_combine/*.h5')
        tag = 'Zee' if 'Zee' in files[0] else 'Hyy' if 'Hyy' in files[0] else 'Zmumugam'
        
        # data_path = f'../tfrecords_data/{tag}'
        particle = 'electrons' if tag=='Zee' else 'photons'
        data_conf = import_module(f'..{particle}_variables_conf',  'variables_params.subpkg')
        data_params = data_conf.get_params()
        for i in ['../root2hdf5/output/root2hdf5/Zmumugam/Zmumugam_data_combine/Zmumugam_data_image_1619682153.9944117.h5']:#files:
            df = h5py.File(i)
            print(df.keys())

    # plt.hist(mass_func, range = (0, 150), bins  = 100, alpha = 0.5, label='True')
    # plt.legend()
    #%%
    elif False:
        df = h5py.File('../root2hdf5/output/root2hdf5/Zmumugam/Zmumugam_data_combine_training_sample/Zmumugam_data_combine_training_sample_data_image_1615370963.8071277.h5')
        n = 100_000
        set_name = 'train'
        p_e = df['train']['p_e'][:n]
        p_eta = df['train']['p_eta'][:n]
        p_phi = df['train']['p_phi'][:n]
        eventNumber = df['train']['eventNumber'][:n]
        index, counts = np.unique(eventNumber, return_counts=True, )
        mask_counts = np.in1d(eventNumber, index[counts >=3])
        E_T = df[set_name]['p_e'][:n]/np.cosh(df[set_name]['p_eta'][:n])
        mask_photon = (
                        (df[set_name]['p_PhotonLHLoose'][:n]) 
                         & ((df[set_name]['p_ptvarcone20'][:n]+df[set_name]['p_ptvarcone30'][:n]+df[set_name]['p_ptvarcone40'][:n])/E_T < 0.3)
                         & ((df[set_name]['p_topoetcone20'][:n]+df[set_name]['p_topoetcone30'][:n]+df[set_name]['p_topoetcone40'][:n])/E_T < 0.3)
                        )
        mask_muon = df[set_name]['p_MuonLHLoose'][:n]
        mask_true_events = (mask_muon | mask_photon) & (mask_counts)
        mass_original = []
        mass_original_func =[]
        for i in tqdm(np.unique(eventNumber[mask_true_events][np.argsort(eventNumber[mask_true_events])])):
            mask = eventNumber == i
            if np.sum(mask) == 3:
                energy = p_e[mask]
                eta = p_eta[mask]
                phi = p_phi[mask]
                mass_original.append(cal_invM(e1=energy[0], e2=energy[1], e3=energy[2],
                                     eta1=eta[0],eta2=eta[1],eta3=eta[2],
                                     phi1=phi[0],phi2=phi[1], phi3=phi[2]))
                px1, px2, px3 = energy[0]*np.cos(phi[0])/np.cosh(eta[0]), energy[1]*np.cos(phi[1])/np.cosh(eta[1]), energy[2]*np.cos(phi[2])/np.cosh(eta[2])
                py1, py2, py3 = energy[0]*np.sin(phi[0])/np.cosh(eta[0]), energy[1]*np.sin(phi[1])/np.cosh(eta[1]), energy[2]*np.sin(phi[2])/np.cosh(eta[2])
                pz1, pz2, pz3 = energy[0]*np.sinh(eta[0])/np.cosh(eta[0]), energy[1]*np.sinh(eta[1])/np.cosh(eta[1]), energy[2]*np.sinh(eta[2])/np.cosh(eta[2])
                mass_original_func.append([np.sqrt((np.sum([energy[0],energy[1], energy[2]])**2
                        -np.sum([px1, px2, px3])**2
                        -np.sum([py1, py2, py3])**2
                        -np.sum([pz1, pz2, pz3])**2)), i])
        mass_original_func = np.array(mass_original_func)#[np.argsort(mass_original_func[:,1])]
        # plt.figure()
        plt.hist(mass_original, range = (80, 120), bins  = 50, alpha = 0.5, label='true')
        plt.hist(mass_original_func[:,0], range = (80, 120), bins  = 50, alpha = 0.5, label='true func')
        plt.legend()
    elif False:
# =============================================================================
#         sigmoid
# =============================================================================
        # sigmoid(x, sigma =5, mean= 45)
        plt.figure()
        for i in [1, 0.5, 0.25, 0.2]:
            plt.plot(np.linspace(20,70, 100), sigmoid(np.linspace(20,70, 100),
                                         sigma=i), label=i)
        plt.legend()

    #%%
    elif False:
    # =============================================================================
    #         Time distribution in time for mc/data
    # =============================================================================
        path = '../root2hdf5/output/root2hdf5/Zee/Zee_data_raw/Zee_data_raw_0000.h5'#'../root2hdf5/output/root2hdf5/Zee_mc_image.h5' # Hyy
        new_data = '../root2hdf5/output/root2hdf5/Zee/Zee_mc_time_gain_combine/small_Zee_mc_image_1611693005.3976343.h5'
        df1 = h5py.File(new_data)
        # paths= glob.glob('../root2hdf5/output/root2hdf5/Zee/Zee_data_combine_w_ATLAS_mass_and_reco_energy/*')#'../root2hdf5/output/root2hdf5/Zee_mc_image.h5' # Hyy
        # df2= h5py.File(new_data)
        df1 = df1['val']
        n = 5_000
        #%%
        fig,axes = plt.subplots(1,3)
        nr=0
        style = {'range': (-5, 25), 'bins': 200, 'histtype':'step'}
        axes[nr].hist(df1['em_barrel_Lr0'][:n].flatten(), label = 'Layer: 0', **style)
        axes[nr].hist(df1['em_barrel_Lr1'][:n].flatten(), label = 'Layer: 1', **style)
        axes[nr].hist(df1['em_barrel_Lr2'][:n].flatten(), label = 'Layer: 2', **style)
        axes[nr].hist(df1['em_barrel_Lr3'][:n].flatten(), label = 'Layer: 3', **style)
        axes[nr].set_yscale('log')
        axes[nr].legend(title='Raw data')
        axes[nr].set_xlabel('[ns]')
        axes[nr].set_ylabel('#')
        nr+=1
        style = {'range': (-50, 5), 'bins': 200, 'histtype':'step'}
        axes[nr].hist(np.log(df1['em_barrel_Lr0'][:n].flatten()+10e-15), label = 'Layer: 0', **style)
        axes[nr].hist(np.log(df1['em_barrel_Lr1'][:n].flatten()+10e-15), label = 'Layer: 1', **style)
        axes[nr].hist(np.log(df1['em_barrel_Lr2'][:n].flatten()+10e-15), label = 'Layer: 2', **style)
        axes[nr].hist(np.log(df1['em_barrel_Lr3'][:n].flatten()+10e-15), label = 'Layer: 3', **style)
        axes[nr].set_yscale('log')
        axes[nr].legend(title='Raw data')
        axes[nr].set_xlabel('[ns]')
        axes[nr].set_ylabel('#')    
        nr+=1
        style = {'range': (-5, 25), 'bins': 200, 'histtype':'step'}
        em0= df1['em_barrel_Lr0'][:n].flatten()
        em0.shape = (-1,1)
        em1= df1['em_barrel_Lr1'][:n].flatten()
        em1.shape = (-1,1)
        em2= df1['em_barrel_Lr2'][:n].flatten()
        em2.shape = (-1,1)
        em3= df1['em_barrel_Lr3'][:n].flatten()
        em3.shape = (-1,1)
        scaler = StandardScaler().fit(em3)
        axes[nr].hist(scaler.transform(em0), label = 'Layer: 0', **style)
        axes[nr].hist(scaler.transform(em1), label = 'Layer: 1', **style)
        axes[nr].hist(scaler.transform(em2), label = 'Layer: 2', **style)
        axes[nr].hist(scaler.transform(em3), label = 'Layer: 3', **style)
        axes[nr].set_yscale('log')
        axes[nr].legend(title='Raw data')
        axes[nr].set_xlabel('[ns]')
        axes[nr].set_ylabel('#')
        #%%
        # eventNumber = df2['eventNumber'][:2*n][df2['p_ElectronLHLoose'][:2*n]]
        # duplicates = [item for item, count in Counter(eventNumber).items() if count == 2] # find duplicates change to 3 if 3 particles in event
        # mask_duplicates = np.in1d(eventNumber,duplicates)
        # axes[1].hist(df2['time_em_barrel_Lr0'][:2*n][df2['p_ElectronLHLoose'][:2*n]][mask_duplicates].flatten(), label = 'Layer: 0', **style)
        # axes[1].hist(df2['time_em_barrel_Lr1'][:2*n][df2['p_ElectronLHLoose'][:2*n]][mask_duplicates].flatten(), label = 'Layer: 1', **style)
        # axes[1].hist(df2['time_em_barrel_Lr2'][:2*n][df2['p_ElectronLHLoose'][:2*n]][mask_duplicates].flatten(), label = 'Layer: 2', **style)
        # axes[1].hist(df2['time_em_barrel_Lr3'][:2*n][df2['p_ElectronLHLoose'][:2*n]][mask_duplicates].flatten(), label = 'Layer: 3', **style)
        # axes[1].set_yscale('log')
        # axes[1].legend(title='Combined data')
        # axes[1].set_xlabel('[ns]')
        # axes[1].set_ylabel('#')
