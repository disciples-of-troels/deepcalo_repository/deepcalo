# Deepcalo
This is only the deepcalo package. This packge should be in a folder by itself and then:
``` python 
import deepcalo as dpcal
```
can be used in a python script outside the deepcalo folder.
##### Example:
``` bash
├── deepcalo # this packages
│   ├── callbacks
│   │   ├── cyclic_lr_schedule.py
│   │   ├── __init__.py
│   │   ├── lr_finder.py
│   │   ├── model_checkpoint.py
│   │   ├── one_cycle_lr_schedule.py
│   │   ├── __pycache__
│   │   │   ├── cyclic_lr_schedule.cpython-37.pyc
│   │   │   ├── __init__.cpython-37.pyc
│   │   │   ├── lr_finder.cpython-37.pyc
│   │   │   ├── model_checkpoint.cpython-37.pyc
│   │   │   ├── one_cycle_lr_schedule.cpython-37.pyc
│   │   │   └── SGDR_lr_schedule.cpython-37.pyc
│   │   └── SGDR_lr_schedule.py
│   ├── data_generator.py
│   ├── __init__.py
│   ├── layers
│   │   ├── bias_correct.py
│   │   ├── film.py
│   │   ├── __init__.py
│   │   ├── pgauss.py
│   │   ├── __pycache__
│   │   │   ├── bias_correct.cpython-37.pyc
│   │   │   ├── film.cpython-37.pyc
│   │   │   ├── __init__.cpython-37.pyc
│   │   │   ├── pgauss.cpython-37.pyc
│   │   │   └── switch_normalization.cpython-37.pyc
│   │   └── switch_normalization.py
│   ├── model_components.py
│   ├── model_container.py
│   ├── __pycache__
│   │   ├── data_generator.cpython-37.pyc
│   │   ├── __init__.cpython-37.pyc
│   │   ├── __init__.cpython-38.pyc
│   │   ├── model_components.cpython-37.pyc
│   │   ├── model_container.cpython-37.pyc
│   │   ├── model_container.cpython-38.pyc
│   │   ├── submodels.cpython-37.pyc
│   │   ├── utils.cpython-37.pyc
│   │   └── utils.cpython-38.pyc
│   ├── README.md
│   ├── submodels.py
│   └── utils.py
└── run_deepcalo.py # this will import deepcalo

```

## original package
Frederiks deepcalo page: https://gitlab.com/ffaye/deepcalo/-/tree/master/deepcalo