import tensorflow as tf
import numpy as np
import h5py
from tqdm import tqdm
import os
import concurrent.futures
import sys
from collections import Counter
import matplotlib.pyplot as plt
import pandas as pd
import math
from os import listdir
import random
from os.path import isfile, join
import time
from glob import glob
from importlib import import_module
from numba import njit
from tensorflow.keras.layers.experimental import preprocessing
from skhep.math import vectors
np.seterr(divide='ignore', invalid='ignore')
import matplotlib
sys.path.insert(1,'../deepcalo')
from tfrecord_data_creation import true_events_in_data, cal_invM
try:
    from .utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel, import_track
except (ModuleNotFoundError, ImportError):
    from utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel, import_track
    
sys.path.insert(1,'../invmfit')
from peakfit_functions import invariant_mass



if __name__ == '__main__':  
    os.environ["CUDA_VISIBLE_DEVICES"] = "-1" # -1 disable
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # cuda warnings
    plt.close('all')
    sys.path.append("../rundeepcalo/")
    path = '../root2hdf5/output/root2hdf5/Zmumugam/Zmumugam_data_combine_13000e/' # Hyy_mc_time_gain_combine
    # path = '../root2hdf5/output/root2hdf5/Zee/Zee_mc_time_gain_combine/'# Zee_mc_time_gain_combine 'Zee_data_2500e_combine_atlas_reco_mass/' Zee_mc_time_gain_combine
    # path = '../root2hdf5/output/root2hdf5/Zmumugam/Zmumugam_mc_combine_all_shuffled/'#Hyy_mc_time_gain_combine/' # Hyy Zee_data_image.h5  Hyy_mc_image
    transformation_path = '../root2hdf5/output/root2hdf5/Zee/Zee_data_combine_2500_events/Zee_combine_2500_events_data_image_1615818960.042395.h5'
    tag = 'Zee' if 'Zee' in path else 'Hyy' if 'Hyy' in path else 'Zmumugam' if 'Zmumugam' in path else print('problem with tag')
    isMC = 'mc' if 'mc' in path else 'data'
    
    save_path = f'../tfrecords_data/Zmumugam_data'# Hyy Zee_mc_w_time_large_reweighted {tag}_{isMC}_classification' ###### folder name change {tag}_{isMC}_w_time_large_reweighted
    particle = 'electrons' if 'ee' in save_path else 'photons'
    data_conf = import_module(f'..{particle}_data_conf',  'params.subpkg')
    data_params = data_conf.get_params()
    file_or_folder = '.h5' in path
    if not file_or_folder:
        paths = [path+f for f in listdir(path) if isfile(join(path, f))][:]
    #%%
    if False:
        mass = []
        remove_with_3_events=0
        for nr in range(3, 30):
            df = h5py.File(paths[nr], mode='r')
            set_name = [i for i in ['train', 'test', 'val'] if len(df[i]) != 0]
            mask_truth = true_events_in_data(paths[nr], set_name[0], isMC=False, tag='Zmumugam', n=None)
            print(nr ,np.sum(mask_truth), len(mask_truth))
            p_eta = df[set_name[0]]['p_eta'][:][mask_truth]
            p_phi = df[set_name[0]]['p_phi'][:][mask_truth]
            p_e = df[set_name[0]]['p_e'][:][mask_truth]
            p_charge = df[set_name[0]]['p_charge'][:][mask_truth]
            eventNumber = df[set_name[0]]['eventNumber'][:][mask_truth]
            p_charge = df[set_name[0]]['p_charge'][:][mask_truth]
            for evt in np.unique(eventNumber):
                mask_evt = eventNumber==evt
                e = p_e[mask_evt]
                phi = p_phi[mask_evt]
                eta = p_eta[mask_evt]
                charge = p_charge[mask_evt]
                if np.sum(mask_evt) != 3: 
                    remove_with_3_events+=1
                    if np.sum(np.abs(charge)) == 2:
                        continue
                        index = np.argsort(np.abs(charge))[charge == 0][-1]
                        e = np.delete(e, index)
                        phi = np.delete(phi, index)
                        eta = np.delete(eta, index)
                        # e= e[:index] + e[index+1 :]
                        # phi= phi[:index] + phi[index+1 :]
                        # eta= eta[:index] + eta[index+1 :]
                    else:
                        print(np.sum(np.abs(charge)))
                        continue
                # print(np.sum(mask_evt))
                mass_with_new_photon = cal_invM(e1=e[0], e2=e[1], e3=e[2],
                                                eta1 = eta[0],eta2=eta[1],eta3=eta[2],
                                                phi1 = phi[0],phi2=phi[1], phi3=phi[2])
                mass.append(mass_with_new_photon)
        mass = np.array(mass)
        print(remove_with_3_events)
        print('Total events:', np.sum((mass > 86) & (mass< 97)))
        plt.hist(mass, bins = 100, range=(50,120), label=f'peak: {np.sum((mass > 86) & (mass< 97))}')
        plt.legend()
    if True: 
        # =============================================================================
        #         Test new variables
        # =============================================================================
        if True:
            size = 0
            var = 'p_eta'
            for i in tqdm(path, total = len(path)):
                df  = h5py.File(i)
                for k in ['train', 'test', 'val']:
                    try:
                        t = df[k]['eventNumber'][:]
                        _, t = np.unique(t, return_index=False, return_inverse=False, return_counts=True)
                        size += np.sum(t==2)
                        print(k)
                    except:
                        pass # n_points[i] = 0

            
        # plt.hist(df['train']['ATLAS_mass'][:], range = (50, 150), bins = 100)
        if False:
            def calculate_pt(tag, probe):
                value = 1/2*91.1876**2/(tag.p_e*(np.cosh(probe.p_eta-tag.p_eta)-np.cos(probe.p_phi-tag.p_phi)))
                if value != np.inf :
                    return value
                else:
                    return 0
            n=100_000
            save=False
            set_name = 'train'
            if tag == 'Zee':
                data_path = '../root2hdf5/output/root2hdf5/Zee_mc_time_gain_combine/small_Zee_mc_image_1611693005.398009.h5'
            else:
                data_path = '../root2hdf5/output/root2hdf5/Hyy_data_image.h5'
                data_path = '../root2hdf5/output/root2hdf5/Hyy_mc_raw/Hyy_mc_0000.h5'
            data = h5py.File(data_path, "r")   
            sys.exit()
            mask_time_lr0 = (data['train']['time_em_barrel_Lr0'][:10] != 0)*1
            energy_lr0 = data['train']['em_barrel_Lr0'][:10]
            correction_energy_lr0 = data['train']['em_barrel_Lr0'][:10]*mask_time_lr0
            if False:
                data_f = pd.DataFrame(data = np.array([data['test']['p_e'][:n],
                                                       data['test']['eventNumber'][:n],
                                                       data['test']['p_phi'][:n],
                                                       data['test']['p_eta'][:n]]).T,
                                      columns=['p_e', 'eventNumber', 'p_phi', 'p_eta'])
                data_f = data_f.sort_values(by = 'eventNumber')
                data_f = data_f.groupby("eventNumber").filter(lambda x: len(x) == 2)
                new_energy = []
                for i in tqdm(data_f['eventNumber'].drop_duplicates(), total=len(data_f['eventNumber'].drop_duplicates())):
                    values = data_f[data_f['eventNumber'] == i]
                    tag = values.iloc[0]
                    probe = values.iloc[1]
                    new_energy.append(calculate_pt(tag=tag, probe=probe))
                    new_energy.append(calculate_pt(tag=probe, probe=tag))
                data_f['new_energy'] = np.array(new_energy)
        if True:
            d1 = df['train']['p_e'][:n]
            d2 = df['train']['p_truth_e'][:n]
            dist = np.abs(d1/d2-1)#np.abs(d1/d2-1)
            # =============================================================================
            #         removing badly reconstructed events
            # =============================================================================
            fig, axes = plt.subplots(1,2, figsize=(15,8))
            iterations = [0.6,0.4,0.2, 0.1]
            style = {'range':(-3, 4), 'density':True, 'bins':200}
            style1={'bins':100, 'histtype':'step', 'range': (0, 500)}
            axes[0].hist(np.log(d1/d2), **style, histtype='step')#, label = r'$log_{10}\left( \frac{E_{pred}}{E_{truth}} \right)$')
            axes[1].hist(d2, **style1)#, label=r'$E_{pred}$')
            for nr, i in enumerate(iterations):
                mask = dist<i
                axes[0].hist(np.log(d1[mask]/d2[mask]),**style, label=f'p={i}')
                axes[1].hist(d2[mask], **style1)#, label=f'events removed at p={i}')
                # plt.legend()
                if nr == len(iterations)-1:
                    axes[0].set_yscale('log') 
                    axes[1].set_yscale('log')
                    axes[0].set_ylabel('#')
                    axes[0].set_xlabel(r'$log_{10}\left( \frac{E_{pred}}{E_{truth}} \right)$')
                    axes[1].set_ylabel('#')
                    axes[1].set_xlabel(r'$E_{truth}$')
                print(i, np.sum(mask)/len(mask))
                
            fig.legend(loc="center right",   # Position of legend
                       borderaxespad=0.1,    # Small spacing around legend box
                       title="P values",  
                       bbox_to_anchor=(0.99,0.6),
                       fontsize=15)
            plt.tight_layout()
            plt.subplots_adjust(right=0.88)
            if save: plt.savefig('../Master Thesis/Report/pic/dataproc/remove_badly_reconstructed_event_mc.eps')
        if False:
            set_name = 'train'
            n = 50_000
            df = h5py.File('../root2hdf5/output/root2hdf5/Zee/Zee_mc_time_gain_combine/small_Zee_mc_image_1611693005.398009.h5', "r")   
            data = h5py.File('../root2hdf5/output/root2hdf5/Zee/Zee_data_combine_sorted/Zee_data_image_1615221560.1638408.h5', "r")
            if False: # layers hist
                data_em = np.array([data[set_name][i][:1000] for i in ['em_barrel_Lr0', 'em_barrel_Lr1', 'em_barrel_Lr2', 'em_barrel_Lr3']])
                df_em = np.array([df[set_name][i][:1000] for i in ['em_barrel_Lr0', 'em_barrel_Lr1', 'em_barrel_Lr2', 'em_barrel_Lr3']])
                #%%
                style = {'bins':100, 'range': (-0.05,0.1), 'alpha': 0.5}
                for i in range(4):
                    plt.hist(data_em[i].flatten(), label=f'MC Lr {i}', **style)
                    plt.hist(df_em[i].flatten(), label=f'data Lr {i}', **style)
                plt.yscale('log')
                plt.legend()
            mask_truth_data = true_events_in_data(path='../root2hdf5/output/root2hdf5/Zee/Zee_data_combine_sorted/Zee_data_image_1615221560.1638408.h5',
                                                  set_name='train', n=-1, tag='Zee')
            # sys.exit()
            
            df = np.array([df[set_name][i][:n] for i in data_params['scalar_names']]).T
            data = np.array([data[set_name][i][:-1] for i in data_params['scalar_names']]).T
            data = data[mask_truth_data]
            #%%
            names=['E_Acc',
               'nIndexCluster',
               'f0Cluster',
               'R12',
               'pt_track',
               'nTracks',
               'Eta',
               'dPhiRescaled2',
               'etaModCalo',
               'dEta2',
               'NvtxReco',
               '<mu>',
               'poscs2',
               'dPhiTH3',
               'fTG3',
               'tile_gap']
            transformed_data = pd.DataFrame()
            for nr, i in tqdm(enumerate(data_params['scalar_names']), total= len(data_params['scalar_names'])):
                transformed_data[names[nr]] = histogram_equalization(arr=df[:,nr], target=data[:,nr])

            df = pd.DataFrame(data=df, columns=names)
            data = pd.DataFrame(data=data, columns=names)
            # sys.exit()
            # fig, axes = plt.subplots(4, 4, figsize=(12, 48))
            # axes = axes.flatten()
            # for col, axis in zip(df.columns, axes):
            #     axis.hist(df[col], bins = 100, label='MC', color='r')
            #     axis.hist(data[col], bins = 100,label='Transformed MC', color='b')
            #     axis.legend()
            #%%
            import seaborn as sn
            annot_kws={'annot_kws': {'size':16}, 'linewidths':.5, 'vmin':0, 'vmax':0.25}
            fig, axes = plt.subplots(1,3, figsize=(50,20), sharey=True)
            corrMatrix_mc = df.corr()
            corrMatrix_data = data.corr()
            corrMatrix_trans_mc = transformed_data.corr()
            # plt.figure(figsize=(20,10))
            heatmap = sn.heatmap(np.abs(corrMatrix_mc-corrMatrix_data), annot=True, cbar=False, **annot_kws, ax=axes[0])
            heatmap.set_xticklabels(heatmap.get_xticklabels(), rotation=30, fontsize = 12) 
            axes[0].set_title('|MC-data|', size = 20)
            # plt.figure()
            heatmap= sn.heatmap(np.abs(corrMatrix_trans_mc-corrMatrix_data), cbar=False, annot=True, **annot_kws, ax=axes[1])
            heatmap.set_xticklabels(heatmap.get_xticklabels(), rotation=30, fontsize = 12) 
            axes[1].set_title('|MC$_{trans}$-data|', size = 20)
            # plt.figure()
            heatmap = sn.heatmap(np.abs(corrMatrix_mc-corrMatrix_data)-np.abs(corrMatrix_trans_mc-corrMatrix_data),
                                 annot=True, cbar=False, **annot_kws, ax=axes[2])
            heatmap.set_xticklabels(heatmap.get_xticklabels(), rotation=30, fontsize = 12) 
            axes[2].set_title(r'|MC-data|-|MC$_{trans}$-data|', size = 20)
            plt.savefig('figures/correlations.png',bbox_inches='tight')
            # fig.tight_layout(pad=-0.8)
            #%%
            size=20
            plt.rcParams['font.size'] = str(20)
            if True:
                plt.close('all')
                for j in tqdm([(0,4), (4,8), (8,12), (12,16)]):
                    fig, axes = plt.subplots(2,2, figsize=(15,15))
                    axes = axes.flatten()
                    for nr, i in enumerate(range(j[0],j[1])):
                        col = df.columns[i]
                        # if i == 2:
                        #     range_values = (0,0.05)
                        # else:
                        range_values = (np.quantile(df[col], 0.01), np.quantile(df[col], 0.99))
                        style={'bins':100, 'alpha': 0.6, 'density': True,
                               'linewidth': 1, 'range': range_values, 
                               'histtype': 'step', 'linewidth': 2}
                        axes[nr].hist(df[col], label='MC', **style, color='green')
                        axes[nr].hist(data[col], label='Data', **style, color = 'red') 
                        axes[nr].hist(transformed_data[col], label='reweighted MC', **style, color = 'blue') 
                        axes[nr].set_title(data_params['scalar_names'][i], fontsize=size)
                        axes[nr].legend(prop={"size":16})
                        axes[nr].set_yscale('log')
                        axes[nr].tick_params(axis='both', which='major', labelsize=size)
                        axes[nr].set_xlabel('', fontsize=size)
                        axes[nr].set_ylabel('', fontsize=size)
                    plt.savefig(f'figures/{i}_MC_transformed.eps',bbox_inches='tight')
                    fig.tight_layout(pad=0)

            # %%========================================================================with_p_e=====
            #  Reweight scalar variables       
            # =============================================================================
            if False:
                plt.close('all')
                fig, axes = plt.subplots(2,2)
                axes = axes.flatten()
                for i in range(0,4):
                    # if i == 2:
                    #     range_values = (0,0.05)
                    # else:
                    range_values = (np.quantile(df[:,i], 0.01), np.quantile(df[:,i], 0.99))
                    style={'bins':100, 'alpha': 0.6, 'density': True,
                           'linewidth': 1, 'range': range_values}
                    axes[i].hist(df[:, i], label='MC', **style, color='blue')
                    axes[i].hist(data[:, i], label='Data', **style, color = 'red') 
                    axes[i].set_title(data_params['scalar_names'][i])
                    axes[i].legend()
                    axes[i].set_yscale('log')
                fig.tight_layout(pad=-0.5)
                fig, axes = plt.subplots(2,2)
                axes = axes.flatten()
                for nr, i in enumerate(range(4,8)):
                    # if i == 2:
                    #     range_values = (0,0.05)
                    # else:
                    range_values = (np.quantile(df[:,i], 0.01), np.quantile(df[:,i], 0.99))
                    style={'bins':100, 'alpha': 0.6, 'density': True,
                           'linewidth': 1, 'range': range_values}
                    axes[nr].hist(df[:, i], label='Data', **style, color='blue')
                    axes[nr].hist(data[:, i], label='MC', **style, color = 'red') 
                    axes[nr].set_title(data_params['scalar_names'][i])
                    axes[nr].legend()
                    axes[nr].set_yscale('log')
                fig.tight_layout(pad=-0.5)
                fig, axes = plt.subplots(2,2)
                axes = axes.flatten()
                for nr, i in enumerate(range(8,12)):
                    # if i == 2:
                    #     range_values = (0,0.05)
                    # else:
                    range_values = (np.quantile(df[:,i], 0.01), np.quantile(df[:,i], 0.99))
                    style={'bins':100, 'alpha': 0.6, 'density': True,
                           'linewidth': 1, 'range': range_values}
                    axes[nr].hist(df[:, i], label='MC', **style, color='blue')
                    axes[nr].hist(data[:, i], label='Data', **style, color = 'red') 
                    axes[nr].set_title(data_params['scalar_names'][i])
                    axes[nr].legend()
                    axes[nr].set_yscale('log')
                fig.tight_layout(pad=-0.5)
                fig, axes = plt.subplots(2,2)
                axes = axes.flatten()
                for nr, i in enumerate(range(12,16)):
                    # if i == 2:
                    #     range_values = (0,0.05)
                    # else:
                    range_values = (np.quantile(df[:,i], 0.01), np.quantile(df[:,i], 0.99))
                    style={'bins':100, 'alpha': 0.6, 'density': True,
                           'linewidth': 1, 'range': range_values}
                    axes[nr].hist(df[:, i], label='MC', **style, color='blue')
                    axes[nr].hist(data[:, i], label='Data', **style, color = 'red') 
                    axes[nr].set_title(data_params['scalar_names'][i])
                    axes[nr].legend()
                    axes[nr].set_yscale('log')
                fig.tight_layout(pad=-0.5)
