import os

import numpy as np
import keras as ks
import keras.backend as K
import tensorflow as tf
import time
import h5py
import sys
from importlib import import_module
from tqdm import tqdm
from numba import njit
from numba.typed import Dict, List
from os import listdir
from os.path import isfile, join
import sys

try:
    from .utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel
except (ModuleNotFoundError, ImportError):
    from utils import load_atlas_data, boolify, standardize, load_atlas_data_parallel
# from .utils import load_atlas_data, boolify
import concurrent.futures
import tensorflow_datasets as tfds
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

def decode(string: [str, list]):
    if type(string) == bytes:
        return string.decode('utf-8')
    else:
        print([i for i in string])
        return [str(i, 'utf-8') for i in string]

def n_point(set_name, n_points):
    if set_name == 'train':
        return {'train':n_points,
                    'val':0,
                    'test':0}
    elif set_name == 'val':
        return {'train':0,
                    'val':n_points,
                    'test':0}
    else:
        return {'train':0,
                    'val':0,
                    'test':n_points}
def chunks(lst, n):
    if type(lst) == int:
        lst = range(lst)
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def standardization(data: dict, data_params: dict,
                    sample_name: str, files: list) -> dict:
    # Standardize scalars and tracks
    # print(data['train']['scalars'])
    # print(sample_name)
    # print(files)
    scalars_to_quantile = ['p_pt_track', 'p_R12', 'p_deltaPhiRescaled2', 'p_deltaEta2',
                            'p_f0Cluster', 'p_eAccCluster', 'p_photonConversionRadius',]
    for dataset_name in ['scalars', 'tracks']:
        if dataset_name=='scalars':
            data_param_name = 'scalar_names'
        elif dataset_name=='tracks':
            data_param_name = 'track_names'
        if data_params[data_param_name] is None:
            continue

        for name in data_params[data_param_name]:
            path = files[name.split('_')[-1]]
            # print(name)
            # Get index of variable
            var_ind = data_params[data_param_name].index(name)
    
            # Get which scaler to use
            scaler_name = 'Quantile' if name in scalars_to_quantile else 'Robust'
            # print(data)
            # Standardize in-place
            # print(sample_name)
            standardize(data,
                        dataset_name,
                        variable_index=var_ind,
                        scaler_name=scaler_name,
                        sample_name = sample_name,
                        load_path = path)
            if False:
                print(f'Standardizing {name} with {scaler_name}Scaler')
                for set_name in data:
                    print(f'Min and max of {name} in {set_name} set after standardization: '
                          f'{data[set_name][dataset_name][:,var_ind].min(), data[set_name][dataset_name][:,var_ind].max()}')
    return data

def import_preprocessed_data(all_input):
    path = all_input['path']
    set_name_overall = all_input['set_name']
    sample_idx = all_input['n_points']
    files = all_input['files']
    data_params = all_input['data_params']
    with h5py.File(path, 'r') as df:
        data = {'train': {'images':{}},
              'test': {'images':{}},
              'val': {'images':{}}}

        for set_name in [set_name_overall]:
            for keys in df[set_name]:
                if keys == 'images': 
                    # print(set_name, keys, sample_idx)
                    data[set_name][keys]['em_barrel'] = df[set_name][keys]['em_barrel'][np.min(sample_idx):np.max(sample_idx)+1]
                else:
                    data[set_name][keys] = df[set_name][keys][np.min(sample_idx):np.max(sample_idx)+1]
    data = standardization(data = data, data_params = data_params,
                        sample_name = set_name, files = files)

    return data

def run_parallel_loading(processes, sample_idx, path, set_name,
                         data_params, files):
    data_chunks = chunks(sample_idx, int(len(sample_idx)/processes))
    
    args = []
    for i in data_chunks:
        args.append({'path':path,'set_name': set_name, 'n_points': i,
                     'data_params': data_params, 'files':files})
    data ={'train':{}, 'test':{}, 'val':{}}
    with concurrent.futures.ProcessPoolExecutor(max_workers=processes) as executor:
        results = executor.map(import_preprocessed_data, tuple(args))
        # results = [executor.submit(import_preprocessed_data, i) for i in args]
    data_list = []
    for r in results:
        data_list.append(r)

    for set_name in data_list[0].keys():
        for keys in data_list[0][set_name].keys():
            values = []
            for i in data_list:
                if keys == 'images': # if there are more keys in images - not correct for more barrels
                    for second_keys in i[set_name][keys].keys():
                        values.extend(i[set_name][keys][second_keys])
                else:
                    values.extend(i[set_name][keys])
                    
            values = np.array(values)
            if keys == 'images':
                data[set_name][keys] = {}
                data[set_name][keys]['em_barrel'] = values
            else:
                data[set_name][keys] = values
    return data

class DataPipeline(tf.data.Dataset):
    def _generator(path,target_name,img_names,gate_img_prefix,scalar_names,
                   track_names,max_tracks,multiply_output_name,
                   sample_weight_name,set_name, n_points,
                   chuck_size, dirs):
        'Generates data containing batch_size samples'
        start = time.time()
        scalar_names= list(scalar_names)
        scalars = []
        for i in list(scalar_names):
            scalars.append(decode(i))
        
        ScalarNames = {}
        ScalarNames['scalar_names'] = scalars
        dirs = decode(dirs)
        files = []
        for file in os.listdir(dirs+'scalers'):
            files.append(dirs+'scalers/'+file)
        files = {path.split('_')[-1].split('.')[0]:path for path in files}
        ScalarNames['track_names'] = None
        img_names = [decode(img_names[0])] ## not optimal
        target_name = decode(target_name)
        lst_splits = chunks(np.arange(0, n_points), chuck_size)
        path = decode(path)
        set_name = decode(set_name)
        
        for sample_idx in lst_splits:
            # print(n_points)
            if set_name=='train':
                n_points={'train':sample_idx, 'val':[], 'test':[]}
            elif set_name=='test':
                n_points={'train':[], 'val':[], 'test':sample_idx}
            elif set_name=='val':
                n_points={'train':[], 'val':sample_idx, 'test':[]}
            else:
                print('set_name is not train, test or valid')
                sys.exit()
            start = time.time()
            if False:
                data = run_parallel_loading(processes=4, sample_idx=sample_idx, path=path, set_name=set_name,
                                            data_params = ScalarNames, files = files)
            else:
                args = {'path':path,'set_name': set_name, 'n_points': n_points[set_name],
                             'data_params': ScalarNames, 'files':files}
                data = import_preprocessed_data(args)
                # data = load_atlas_data(path=path, n_points=n_points,
                #                         target_name=target_name, 
                #                         img_names=img_names, scalar_names=ScalarNames['scalar_names'],
                #                         verbose=False
                #                         )
            print('\n','Preprocess data', time.time()-start,'\n')     
            yield ({'em_barrel': data[set_name]['images'][img_names[0]],
                   'scalars': data[set_name]['scalars']}, data[set_name]['targets']) 
    
    def __new__(cls, set_name, load_kwargs, n_points, dirs = 3, chuck_size=3):
        return tf.data.Dataset.from_generator(
            cls._generator,
            output_types=({'em_barrel': tf.float64, 'scalars': tf.float64}, tf.float64),
            output_shapes=({'em_barrel': (None, 56, 11, 4),
                           'scalars' : (None, len(load_kwargs['scalar_names']))},
                           (None, )),
            args=(load_kwargs['path'], load_kwargs['target_name'],
                  load_kwargs['img_names'],load_kwargs['gate_img_prefix'],
                  load_kwargs['scalar_names'],load_kwargs['track_names'],
                  load_kwargs['max_tracks'],load_kwargs['multiply_output_name'],
                  load_kwargs['sample_weight_name'],set_name, n_points,
                  chuck_size, dirs)
        )
import tensorflow as tf

# @njit
def format_data(data_list, data, all_set_name):
    for set_name in [all_set_name]:
        for keys in data_list[set_name].keys():
            values = []
            if keys == 'images': # if there are more keys in images - not correct for more barrels
                for second_keys in data_list[set_name][keys].keys():
                    values.extend(data_list[set_name][keys][second_keys])
            else:
                values.extend(data_list[set_name][keys])
                    
            values = np.array(values)
            if keys == 'images':
                data[set_name][keys] = {}
                data[set_name][keys]['em_barrel'] = values
            else:
                data[set_name][keys] = values
    return data

class generator:
    def __init__(self, target_name,img_names,gate_img_prefix,scalar_names,
                   track_names,max_tracks,multiply_output_name,
                   sample_weight_name,set_name, n_points,
                   chuck_size, dirs, processes):
        # print(path)
        # self.path = path
        self.target_name = target_name
        self.img_names=img_names
        self.gate_img_prefix=gate_img_prefix
        self.scalar_names=scalar_names
        self.track_names=track_names
        self.max_tracks=max_tracks
        self.multiply_output_name=multiply_output_name
        self.sample_weight_name=sample_weight_name
        self.set_name=set_name
        self.n_points=n_points
        self.chuck_size=chuck_size
        self.dirs=dirs
        self.processes = processes
        print('Number of cores used to import data: ', processes)
        # for i in self.path.as_numpy_iterator():
        # print(path.numpy())
        # print(tf.read_file(path))
        ScalarNames={}
        files=[]
        for file in os.listdir(self.dirs+'scalers'):
            files.append(self.dirs+'scalers/'+file)
        self.files = {path.split('_')[-1].split('.')[0]:path for path in files}
        ScalarNames['track_names'] = None
        ScalarNames['scalar_names']=self.scalar_names
        self.ScalarNames = ScalarNames
        self.loop_keys=[]
        names = ['multiply_output_with', 'sample_weights', 'scalars', 'targets', 'tracks']
        for i, name in zip([multiply_output_name,sample_weight_name, scalar_names, target_name, track_names], names):
             if i != -999:
                 self.loop_keys.append(name)

        
        
    def import_preprocessed_data(self, sample_idx, path):
        start_time=time.time()
        data = {self.set_name: {'images':{}}}
        with h5py.File(path, 'r') as df:
            for set_name in [self.set_name]:
                for keys in df[set_name]:
                    if keys == 'images': 
                        data[set_name][keys]['em_barrel'] = df[set_name][keys]['em_barrel'][np.min(sample_idx):np.max(sample_idx)+1]
                    else:
                        data[set_name][keys] = df[set_name][keys][np.min(sample_idx):np.max(sample_idx)+1]
        data = standardization(data = data, data_params = self.ScalarNames,
                                sample_name = self.set_name, files = self.files)
        # print('Time to sort', time.time()-start)
        return data
          
    def run_parallel_loading(self, sample_idx, path):
        data_chunks = chunks(sample_idx, int(len(sample_idx)/self.processes))
        args = []
        for i in data_chunks:
            args.append({'path':path,'set_name': self.set_name, 'n_points': i,
                         'data_params': self.ScalarNames, 'files':self.files})
        data ={self.set_name:{}}
        with concurrent.futures.ProcessPoolExecutor(max_workers=self.processes) as executor:
            results = [executor.submit(import_preprocessed_data, i) for i in args]
            for r in concurrent.futures.as_completed(results):
                data_list= r.result()
                data = format_data(data_list,data, self.set_name)
        return data
    
    def __call__(self, path):
        path = decode(path)
        lst_splits = chunks(np.arange(0, self.n_points), self.chuck_size)
        'Generates data containing batch_size samples'
        with h5py.File(path, 'r') as df:
            for sample_idx in lst_splits:   
                # print(path)
                # data = self.import_preprocessed_data(sample_idx=sample_idx, path=path)
                # data = self.run_parallel_loading(sample_idx=sample_idx, path=path)
                
                start=time.time()
                data = {self.set_name: {}}
                for keys in self.loop_keys:
                        data[self.set_name][keys] = df[self.set_name][keys][np.min(sample_idx):np.max(sample_idx)+1]
                # print(data)
                data = standardization(data = data, data_params = self.ScalarNames,
                                        sample_name = self.set_name, files = self.files)
                
                # print('\n Preprocessc data', time.time()-start, '\n')
                print('\n' ,self.set_name, np.min(sample_idx), np.max(sample_idx), path.split('/')[-1].split('_')[0], '\n')
                yield ({'em_barrel': df[self.set_name]['images']['em_barrel'][np.min(sample_idx):np.max(sample_idx)+1],
                       'scalars': data[self.set_name]['scalars']}, data[self.set_name]['targets']) 
        
if __name__ =='__main__':
    if os.getcwd() == '/home/malte/hep/gpulab/work/Master/deepcalo':
        # path = '../../../../gpulab-data/Master/preprocessed/Zee_mc_preprocessed_without_tile_scalar.h5'
        # path = '../run/train_model_malteal/preprocessed_events/old_files/Zee_image_mc_500_events.h5'
        path = '../../../../gpulab-data/Master/preprocessed/single_data_files/'
    else:
        # path = '/media/data/malteal/Master/MC_ER_abseta_0.0_2.5_et_0.0_5000000.0_processes_zee_added_images.h5'
        # path = '../run/train_model_malteal/preprocessed_events/old_files/Zee_image_mc_500_events.h5'
        # path = '/media/data/malteal/Master/preprocessed/preprocessed_events_Zee_mc_image_preprocessed_without_tile_in_image_large_64.h5'
        path = '/media/data/malteal/Master/preprocessed/single_data_files/'
    dirs = '../run/train_model_malteal/logs/1/Zee_2020-11-24T22:30:00.427895_1000_epochs_sigma/'

    set_name='val'
    chuck_size = 1_000
    n_points = 10_000
    batch_size =10
    args1={}
    load_kwargs= {
                  # 'path': path, 
                  'processes':8,
                        'target_name': 'p_truth_e', 'img_names': ['em_barrel'], 'gate_img_prefix': -999,
                        'scalar_names': ['p_eAccCluster',
                                          'p_cellIndexCluster',
                                          'p_f0Cluster',
                                          'p_R12',
                                          'p_pt_track',
                                          'p_nTracks',
                                          'p_eta',
                                          'p_deltaPhiRescaled2',
                                          'p_etaModCalo',
                                          'p_deltaEta2',
                                          'NvtxReco',
                                          'averageInteractionsPerCrossing',
                                          'p_poscs2',
                                          'p_dPhiTH3',
                                          'p_fTG3'],
                        'track_names': -999, 'max_tracks': -999, 'multiply_output_name': -999, 'sample_weight_name': -999}
    
    # df = h5py.File('/media/data/malteal/Master/preprocessed/Zee_mc_preprocessed_without_tile_scalar.h5', 'r')    
    # points = 100_000
    # start_time=time.time()
    # data= df['train']['scalars'][:points]
    # print('time:', time.time()-start_time)
    # start_time=time.time()
    # data= df['train/scalars'][:points]
    # print('time:', time.time()-start_time)
    # df.close()
    
    # start_time=time.time()    
    # def func(points):
    #     with h5py.File('/media/data/malteal/Master/preprocessed/Zee_mc_preprocessed_without_tile_scalar.h5', 'r') as df:
    #         # print(1)
    #         data= {'scalars': df['train']['scalars'][:points], 'target':df['train']['targets'][:points],
    #                 'images': df['train']['images']['em_barrel'][:points]
    #                 }

    #     return data
    # with concurrent.futures.ProcessPoolExecutor() as executor:
    #     results = [executor.submit(func,i) for i in [1_000]*100]
    #     # results = executor.map(func, [50_000]*2)
    #     for i in concurrent.futures.as_completed(results):
    #         # print(i.result()['images'].shape)
    #         pass
    # print('time:', time.time()-start_time)
    
    # start_time=time.time()    
    # func(points)
    # print('time:', time.time()-start_time)
    
    # gena = DataPipeline(set_name=set_name,
    #                       chuck_size = chuck_size,
    #                       load_kwargs=load_kwargs,
    #                       n_points=n_points,
    #                       dirs = dirs)
    # gena2 = generator(set_name=set_name,
    #                       chuck_size = chuck_size,
    #                       **load_kwargs,
    #                       n_points=n_points,
    #                       dirs = dirs)
    # gena2 = tf.data.Dataset.from_generator(
    #                     generator(set_name=set_name,
    #                       chuck_size = chuck_size,
    #                       **load_kwargs,
    #                       n_points=n_points,
    #                       dirs = dirs),
    #                     output_types=({'images': tf.float64, 'scalars': tf.float64}, tf.float64),
    #                     output_shapes=({'images': (None, 56, 11, 4),
    #                                     'scalars' : (None, len(load_kwargs['scalar_names']))},
    #                                     (None, )))
    
    from os import listdir
    from os.path import isfile, join
    filenames = [path+f for f in listdir(path) if isfile(join(path, f))]
    ds = tf.data.Dataset.from_tensor_slices(filenames[:1])
    ds = tf.data.Dataset.list_files(filenames)
    import tensorflow_io as tfio
    gena = ds.interleave(lambda filename: tf.data.Dataset.from_generator(
                        generator(
                            set_name=set_name,
                            chuck_size = chuck_size,
                            **load_kwargs,
                            n_points=n_points,
                            dirs = dirs
                            ),
                        output_types=({'em_barrel': tf.float64, 'scalars': tf.float64}, tf.float64),
                        output_shapes=({'em_barrel': (None, 56, 11, 4),
                                        'scalars' : (None, len(load_kwargs['scalar_names']))},
                                        (None, )),
                        args=(filename, )),
        num_parallel_calls=tf.data.experimental.AUTOTUNE
        )
    def func(filename):
        data = tf.io.read_file(filename)
        return data
        # print(data)
        # # path = tf.strings.split(filename, '/')
        # # print(path)
        # with h5py.File(filename, 'r') as df:
        #     data = {set_name: {}}
        #     for keys in ['targets', 'scalars']:
        #             data[set_name][keys] = df[set_name][keys][:]
        #     # print(data)
        #     # data = standardization(data = data, data_params = load_kwargs['scalar_names'],
        #     #                         sample_name = set_name, files = load_kwargs['files'])
            
        #     # print('\n Preprocessc data', time.time()-start, '\n')
        #     # print(sample_idx, path.split('/')[-1])
        # return df[set_name]['images']['em_barrel'][:], data[set_name]['scalars'], data[set_name]['targets']
    train_ds = ds.map(func, num_parallel_calls=5)
    t = h5py.File(filenames[0], 'r')['val']['targets']

    #create the dataset
    # for i in iter(gena):
    #     print(i)
    # iterator = dataset.make_one_shot_iterator()
    # next_element = iterator.get_next()
    # with tf.Session() as sess:
    #   val = sess.run(next_element)
    #   print(val)
    # start = time.time()
    # # data = standardization(data = data, data_params = self.ScalarNames,
    # #                        sample_name = self.set_name, files = self.files)
    # for i in gena:
    #     # _, tag = i
    #     # target.append(tag)
    #     pass
    # # # print(len(np.ravel(target)))
    # # benchmark = tfds.core.benchmark(gena)
    # print('First G time:', time.time()-start)
    
#     # print(list(gena2.as_numpy_iterator()))
#     # for i in tqdm(gena()):
#     #     pass
#     for i, j in zip(load_kwargs.values(), load_kwargs):
#         k = i if i != None else -999
#         args1[j] = k
#     args1['path'] = '/media/data/malteal/Master/preprocessed/Zee_mc_preprocessed_without_tile_scalar.h5'
#     datagen = DataPipeline(set_name=set_name,
#                           chuck_size = 400_000,
#                           load_kwargs=args1,
#                           n_points=400_000,
#                           dirs = dirs,
# )
#     datagen = datagen.prefetchtch(buffer_size=tf.data.experimental.AUTOTUNE)

    # start = time.time()
    # for i in datagen:
    #     print(len(i[1]))
    # # benchmark = tfds.core.benchmark(gena, batch_size=batch_size);
    # print('First G time:', time.time()-start)
    
    # start = time.time()
    # benchmark = tfds.core.benchmark(gena);
    # print(benchmark['raw']['end_time']-benchmark['raw']['start_time'])    
    # benchmark = tfds.core.benchmark(datagen);
    # print('second G time:', time.time()-start)
    # print(benchmark['raw']['end_time']-benchm+ark['raw']['start_time'])
#     data_wrong = []
#     for i in datagen.as_numpy_iterator():
#         data_wrong.append(i)
#     # with h5py.File('../run/train_model_malteal/preprocessed_events/Zee_image_mc_500_events.h5', 'r') as df:
        
#     #     data = {'train': {'images':{}},
#     #           'test': {'images':{}},
#     #           'val': {'images':{}}}

#     #     for set_name in df.keys():
#     #         for keys in df[set_name]:
#     #             if keys == 'images': 
#     #                 data[set_name][keys]['em_barrel'] = df[set_name][keys]['em_barrel'][:n_points]
#     #             else:
#     #                 data[set_name][keys] = df[set_name][keys][:n_points]
# #%%
#     target = data_wrong[0][1]
#     target.shape = (len(data_wrong[0][1]),1)
#     data_wrong = np.c_[ target, data_wrong[0][0]['scalars'] ]
#     # data = np.c_[data['train']['targets'], data['train']['scalars']]

    # train = DataPipeline(set_name='train',
    #                               chuck_size = self.params['data_generator']['chuck_size'],
    #                               load_kwargs=self.args,
    #                               n_points=self.params['data_generator']['n_points']['train'],
    #                               dirs = self.dirs['log']
    #                               )
    # from os import listdir
    # from os.path import isfile, join
    # path = '/media/data/malteal/Master/preprocessed/single_data_files/'
    # del self.args['path']
    # filenames = [path+f for f in listdir(path) if isfile(join(path, f))]
    # ds = tf.data.Dataset.from_tensor_slices(filenames)
    
    # train = ds.interleave(lambda filename: tf.data.Dataset.from_generator(
    #         generator(
    #             set_name='train',
    #             chuck_size = self.params['data_generator']['chuck_size'],
    #             n_points=self.params['data_generator']['n_points']['train'],
    #             dirs = self.dirs['log'],
    #             **self.args,
    #             ),
    #         output_types=({'em_barrel': tf.float32, 'scalars': tf.float32}, tf.float32),
    #         output_shapes=({'em_barrel': (None, 56, 11, 4),
    #                         'scalars' : (None, len(self.args['scalar_names']))},
    #                         (None, )),
    #         args=(filename, )),
    #     num_parallel_calls=tf.data.experimental.AUTOTUNE,
    #     )
    
    # train = self.configure_for_performance(train.unbatch(), repeat=False)
    # # datagen_val = DataPipeline(set_name='val',
    # #                               chuck_size = self.params['data_generator']['chuck_size'],
    # #                               load_kwargs=self.args,
    # #                               n_points=self.params['data_generator']['n_points']['val'],
    # #                               dirs = self.dirs['log']
    # #                               )
    # val = ds.interleave(lambda filename: tf.data.Dataset.from_generator(
    #         generator(
    #             set_name='val',
    #             chuck_size = self.params['data_generator']['chuck_size'],
    #             n_points=self.params['data_generator']['n_points']['val'],
    #             dirs = self.dirs['log'],
    #             **self.args,
    #             ),
    #         output_types=({'em_barrel': tf.float32, 'scalars': tf.float32}, tf.float32),
    #         output_shapes=({'em_barrel': (None, 56, 11, 4),
    #                         'scalars' : (None, len(self.args['scalar_names']))},
    #                         (None, )),
    #         args=(filename, )),
    #     num_parallel_calls=tf.data.experimental.AUTOTUNE
    #     )
    
    # val = self.configure_for_performance(val.unbatch(), repeat=False, cache=True)
    # # print(train)
    # options = tf.data.Options()
    # options.experimental_distribute.auto_shard_policy = tf.data.experimental.AutoShardPolicy.OFF
    # val = val.with_options(options)
    # train = train.with_options(options)

