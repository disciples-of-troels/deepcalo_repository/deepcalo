#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 13:44:49 2021

@author: malte
"""
import h5py
import matplotlib.pyplot as plt
import pandas as pd
plt.close('all')
import sys
from tqdm import tqdm
import time
from tensorboard.plugins.hparams import api as hp
import numpy as np
import json
import tensorflow as tf
import keras
import itertools
from keras import layers
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import keras as ks
from glob import glob
from os import listdir
from os.path import isfile, join
import os
from keras.models import Sequential
from keras.layers import (LSTM, Dense, TimeDistributed, SimpleRNN,
                          Embedding, Conv2D, BatchNormalization, Input, Masking,
                          Flatten, ReLU, Multiply, LeakyReLU, Reshape, Conv1D,
                          MaxPooling1D)
import argparse
import numpy as np

def track_net(shape, rows, output_size=1, multiple_layer=None, cnn=True,
              verbose=0, compile_model=False, nr_neurons=[64],
              conv1d=3):
    def use_dense(neurons, previous):
        dense_1 = Dense(neurons, use_bias=True)(previous)
        batch_1 = BatchNormalization()(dense_1)
        activ_1 = LeakyReLU()(batch_1)
        return activ_1
    
    model_in = ks.layers.Input(shape=(rows, shape, 1), dtype='float32', name='track_input')
    reshape_1 = Reshape(( rows, shape), input_shape=(rows, shape, 1))(model_in)
    conv2d_2 = Conv1D(conv1d,conv1d)(reshape_1)
    batch_1 = BatchNormalization()(conv2d_2)
    activ_2 = LeakyReLU()(batch_1)
    flatten_1 = Flatten()(activ_2)
    for i, neurons in zip(range(len(nr_neurons)), nr_neurons):
        if i == 0:
            activ_1 = use_dense(neurons, flatten_1)            
        else:
            activ_1 = use_dense(neurons, activ_1)
    dense_3 = Dense(output_size)(activ_1)
    batch_3 = BatchNormalization()(dense_3)
    dense_2 = LeakyReLU()(batch_3)
        
    if multiple_layer != None:
        output = Multiply()([multiple_layer, dense_2])
        model = ks.models.Model(inputs=[model_in, multiple_layer],
                                outputs=output, name='track_net')
    else:
        model = ks.models.Model(inputs=[model_in], outputs=dense_2,
                                name='track_net')
    if verbose: 
        model.summary()
    if compile_model: 
        return model
    else:
        return dense_2
    
def run(*, run_dir, hparams, data, session_num, verbose=1):
    with tf.summary.create_file_writer(run_dir).as_default():
        hp.hparams(hparams)  # record the values used in this trial
        model_in_multiply = Input(shape=(None, ), dtype='float32')
        values = list(hparams.values())
        NUM_dense = np.log([int(i) for i in values[0].replace('[', '').replace(']', '').split(', ')])/np.log(2)
        NUM_dense = [2**i for i in np.arange(NUM_dense[1], NUM_dense[0]+1)[::-1]]
        score =  []
        for i in range(4):
            model = track_net(shape=len(names), rows=rows, multiple_layer=model_in_multiply,
                              compile_model=True, verbose=True, output_size=1,
                              nr_neurons=NUM_dense,
                              conv1d = values[1])
            model.compile(loss='logcosh',
                          metrics=['mean_absolute_error'],
                          optimizer='Nadam')
            print('#'*10, run_dir)
            model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(filepath=run_dir,#+'/{epoch:04d}-{val_loss:.4f}',
                                                                           save_best_only=True,
                                                                           save_weights_only=True,
                                                                           monitor='val_loss',
                                                                           )
            
            earlystopping = ks.callbacks.EarlyStopping(min_delta=0.00005, patience=20)
            
            model.fit(data[0], validation_data=data[1],epochs=200,
                      verbose=1, callbacks=[model_checkpoint_callback, earlystopping,
                                            tf.keras.callbacks.TensorBoard(run_dir),
                                            hp.KerasCallback(run_dir, hparams)])
            model.load_weights(run_dir)
            score.append(model.evaluate(data[2]))
        score = np.mean(np.array(score),0)
        print('################ ', score)
        tf.summary.scalar('mean_absolute_error', score[1], step=1)
        tf.summary.scalar('logcosh', score[0], step=1)
        # result = 0.1
    return score
    
class generator():
    def __init__(self, data, multiple_with, truth=[], zero_columns=None):
        self.data = data
        self.truth = truth
        self.multiple_with=multiple_with
        self.zero_columns = zero_columns
        
    def __call__(self):
        if len(self.truth) != 0:
            for d, j, i in zip(self.data, self.truth, self.multiple_with):
                j = np.array(j)
                i = np.array(i)
                j.shape = (1,)
                i.shape = (1,)
                d = np.array(d)
                d.shape = (d.shape[0], self.data.shape[-1])
                if self.zero_columns != None:
                    # d[:,self.zero_columns] = 0
                    np.random.shuffle(d[:,self.zero_columns])
                # print(j.shape, d.shape, i.shape)
                yield (d, i), j
        else:
            for d, i in zip(self.data, self.multiple_with):
                d = np.array(d)
                d.shape = (d.shape[0], self.data.shape[-1])
                i = np.array(i)
                i.shape = (1,)
                if self.zero_columns != None:
                    d[:,self.zero_columns] = 0
                # d.shape = (None, shape)
                yield (d, i)
def plot_nr_track(df):
    fig, ax1 = plt.subplots()
    shape = []
    values = df['train']['tracks_phi'][:100_000]
    for i in tqdm(range(100_000)):
        shape.append(len(values[i]))
    color = 'cornflowerblue'
    ax1.hist(shape, bins = np.max(shape), color=color, 
             label='# Track distribution of 100.000 Zee MC', range = (0, np.max(shape)))

    ax1.set_xlabel('Numbe of tracks')
    ax1.set_ylabel('#', color=color)
    ax1.tick_params(axis='y', labelcolor=color)
    
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    
    color = 'green'
    ax2.set_ylabel('Ratio of tracks', color=color)  # we already handled the x-label with ax1
    counts, bins= np.histogram(shape, bins = np.max(shape),  range = (0, np.max(shape)))
    ax2.plot(bins[:-1], np.cumsum(counts)/np.sum(counts), label='Norm cumulative sum', color=color)
    ax2.tick_params(axis='y', labelcolor=color)

    ax1.vlines(15, 0, 11500, label='Number of track selected', color='black', linestyle='dotted', linewidth = 3)
    # plt.xlabel('Numbe of tracks')
    # plt.ylabel('#')
    fig.legend(loc ='center right',
              bbox_to_anchor=(0.85,0.2))        
    plt.tight_layout()  
    
def import_data(*,set_name, n, rows, names, df):
    data=pd.DataFrame()
    lengths = [len(i) for i in df[set_name][names[0]][:n]]
    p_truth_e = df[set_name]['p_truth_e'][:n]
    p_e = df[set_name]['p_e'][:n]
    mask_remove_bad_events = p_truth_e != -999 # properly no events
    p_truth_e = p_truth_e[mask_remove_bad_events]
    p_e = p_e[mask_remove_bad_events]
    ratio = p_e/p_truth_e   
    for i in names:
        
        if 'tracks_d0' in i:
            d = df[set_name][i][:n]/df[set_name]['tracks_sigmad0'][:n]
        elif 'tracks_pt' in i:
            d = df[set_name][i][:n]/df[set_name]['tracks_charge'][:n]
        else:
            d = df[set_name][i][:n]
        d = (d-np.mean(np.hstack(d)))/np.std(np.hstack(d))
        data[i] = d
    # preprocessing removing bad event 
    data = data.values[mask_remove_bad_events]
    mask_remove_empty = np.array([len(i[0])!=0 for i in data])
    data = data[mask_remove_empty]
    ratio = ratio[mask_remove_empty]
    p_truth_e = p_truth_e[mask_remove_empty]
    p_e = p_e[mask_remove_empty]
    all_data = []
    # rows=np.max(lengths)
    for i in tqdm(range(len(data))):
        d = np.vstack(data[i]).T
        value = np.exp(-d[:,0]/0.1)*d[:,1] # exp(deltaR/0.1)*p_t
        d = d[value.argsort()[::-1]]
        # all_data.append(np.pad(t, (0,max_len-len(t))))
        d = d[:rows]
        d = np.pad(d, ((0, rows-len(d)), (0,0)), constant_values=0) # padding data.
        all_data.append(d)
    all_data = np.array(all_data)
    mask_bad_ratio = ratio<5  # p_e/truth
    return all_data[mask_bad_ratio], ratio[mask_bad_ratio], p_truth_e[mask_bad_ratio], p_e[mask_bad_ratio]

def hyperparameter_tuning(df, names, rows, n):
    HP_NUM_dense_values = np.sort([[2**i for i in range(4,7)][::-1],
                           [2**i for i in range(4,6)][::-1],
                            [2**i for i in range(5,7)][::-1],
                            [2**i for i in range(4,8)][::-1],
                            [2**i for i in range(5,9)][::-1],
                            [2**i for i in range(5,8)][::-1],
                            [2**i for i in range(6,8)][::-1],
                            [2**i for i in range(7,9)][::-1],
                            [2**i for i in range(6,9)][::-1],
                            [2**i for i in range(7,10)][::-1],
                            [2**i for i in range(8,10)][::-1],
                            [2**i for i in range(5,10)][::-1]
                            ])
    HP_NUM_dense_values = [[i[0], i[-1]]for i in HP_NUM_dense_values]
    HP_NUM_dense = hp.HParam('num_dense', hp.Discrete([str(i) for i in HP_NUM_dense_values]))
                                                      
    HP_NUM_CONV = hp.HParam('num_conv', hp.Discrete([1,
                                                      2,3,4,5
                                                     ]))
    
    train_data, train_ratio, train_truth, train_p_e = import_data(set_name='train', n=n, rows=rows, names=names)
    val_data, val_ratio, val_truth, val_p_e = import_data(set_name='val', n=n, rows=rows, names=names)
    test_data, test_ratio, test_truth, test_p_e = import_data(set_name='test', n=n, rows=rows, names=names)
    train_generator = tf.data.Dataset.from_generator(
                                             generator(train_data,train_p_e, train_truth, -1),
                                             output_types=((tf.float32, tf.float32), tf.float32), 
                                             output_shapes=(((None, len(names)), (1,)), (None,))
                                             ).batch(1024).cache().prefetch(tf.data.experimental.AUTOTUNE) # .padded_batch(64, padding_values=-999.0)
    valid_generator = tf.data.Dataset.from_generator(
                                             generator(val_data, val_p_e, val_truth, -1),
                                             output_types=((tf.float32, tf.float32), tf.float32), 
                                             output_shapes=(((None, len(names)), (1, )), (None,))
                                             ).batch(1024).cache().prefetch(tf.data.experimental.AUTOTUNE) #.shuffle(buffer_size=512) .padded_batch(64, padding_values=-999.0)
    test_generator = tf.data.Dataset.from_generator(
                                             generator(test_data, test_p_e, test_truth, -1),
                                             output_types=((tf.float32, tf.float32), tf.float32), 
                                             output_shapes=(((None,len(names)), (1,)), (None,))
                                             ).batch(1024).cache().prefetch(tf.data.experimental.AUTOTUNE) #.shuffle(buffer_size=512) .padded_batch(64, padding_values=-999.0)
    #%%      
    
    checkpoint_filepath = f'models/hyperparameter_{time.time()}/'
    os.mkdir(checkpoint_filepath)
    with tf.summary.create_file_writer(checkpoint_filepath).as_default():
      hp.hparams_config(
        hparams=[
                HP_NUM_dense,
                HP_NUM_CONV
                ],
        metrics=[hp.Metric('mean_absolute_error', display_name='mean_absolute_error'),
                 hp.Metric('logcosh', display_name='logcosh')],
      )
    
    permutations = itertools.product(
                                     HP_NUM_CONV.domain.values,
                                     HP_NUM_dense.domain.values
                                     )
    session_num = 0
    for batch in permutations: # permutations
        print(batch)
        hparams = {
            HP_NUM_dense: batch[1],
            HP_NUM_CONV: batch[0]
        }
        run_name = "run-%d" % session_num
        print('--- Starting trial: %s' % run_name)
        print({h.name: hparams[h] for h in hparams})
        run(run_dir=checkpoint_filepath+f'{run_name}/', hparams=hparams,
            data=[train_generator, valid_generator, test_generator],
            verbose=1, session_num=session_num
            )
        session_num += 1



if __name__ == '__main__':
    # imports
    parser = argparse.ArgumentParser()
    parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='-1', type=str)
    args = parser.parse_args()
    gpu_ids = args.gpu
    os.environ["CUDA_VISIBLE_DEVICES"] = str(gpu_ids)
    
    scaler = StandardScaler()
    new_data = '../root2hdf5/output/root2hdf5/Zee/Zee_mc_time_gain_combine/small_Zee_mc_image_1611693005.3976343.h5'
    df = h5py.File(new_data, "r")

    for i in df.keys():
        print(f'{i}:', len(df[i]['p_e']))

    names = ['tracks_dR',
             'tracks_pt',
             'tracks_d0',
             # 'tracks_sigmad0',
             'tracks_theta',
             'tracks_trthits',
             'tracks_vertex',
             'tracks_z0',
              # 'tracks_charge',
             'tracks_eta',
             'tracks_phi',
             'tracks_pixhits',
             'tracks_scthits']
    n=None
    rows = 15
    #%%
    number_of_runs=5
    if False:
        hyperparameter_tuning(df, names, rows, n)
    
    elif False:
        train_data, train_ratio, train_truth, train_p_e = import_data(set_name='train', n=n, rows=rows, names=names)
        val_data, val_ratio, val_truth, val_p_e = import_data(set_name='val', n=n, rows=rows, names=names)
        test_data, test_ratio, test_truth, test_p_e = import_data(set_name='test', n=n, rows=rows, names=names)
        
        #%%
        # =============================================================================
        # Model HUSK AT FJENE -999
        # =============================================================================
        shape = len(names) # all_data.shape[1]
    
    
        #%%
        checkpoint_filepath = f'models/{time.time()}/'
        columns = [None]+names # +names
        for nr, name in enumerate(columns):
            print(name)
            performance = {}
            folder_path = checkpoint_filepath + f'{name}/'
            # model_in = Input(shape=(rows, shape, 1), dtype='float32')
            model_in_multiply = Input(shape=(None, ), dtype='float32')

            # if n is not None:
            #     sys.exit()
            
            train_generator = tf.data.Dataset.from_generator(
                                                     generator(train_data,train_p_e, train_truth, nr-1),
                                                     output_types=((tf.float32, tf.float32), tf.float32), 
                                                     output_shapes=(((None, shape), (1,)), (None,))
                                                     ).batch(1024).cache().prefetch(tf.data.experimental.AUTOTUNE) # .padded_batch(64, padding_values=-999.0)
            valid_generator = tf.data.Dataset.from_generator(
                                                     generator(val_data, val_p_e, val_truth, nr-1),
                                                     output_types=((tf.float32, tf.float32), tf.float32), 
                                                     output_shapes=(((None, shape), (1, )), (None,))
                                                     ).batch(1024).cache().prefetch(tf.data.experimental.AUTOTUNE) #.shuffle(buffer_size=512) .padded_batch(64, padding_values=-999.0)
            test_generator = tf.data.Dataset.from_generator(
                                                     generator(test_data, test_p_e, [], nr-1),
                                                     output_types=(tf.float32, tf.float32), 
                                                     output_shapes=((None,shape), (1,))
                                                     ).batch(1024).cache().prefetch(tf.data.experimental.AUTOTUNE) #.shuffle(buffer_size=512) .padded_batch(64, padding_values=-999.0)
            #%%
            values = []
            for i in range(number_of_runs):
                model = track_net(shape=shape, rows=rows, multiple_layer=model_in_multiply,
                                  compile_model=True, verbose=True, output_size=1,
                                  nr_neurons=[256, 128, 64, 32])
                model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(filepath=folder_path,#+'/{epoch:04d}-{val_loss:.4f}',
                                                                               save_best_only=True,
                                                                               save_weights_only=True,
                                                                               monitor='val_loss',
                                                                               )
                earlystopping = ks.callbacks.EarlyStopping(min_delta=0.00005, patience=20)
                model.compile(loss='logcosh',
                              metrics=['mean_absolute_error'],
                              optimizer='Nadam')
                try:
                    model.fit(train_generator, validation_data=valid_generator,epochs=100,
                                        verbose=1, callbacks=[model_checkpoint_callback, earlystopping])
                except KeyboardInterrupt:
                    print('Testing!')
                    sys.exit()
                #%%
                # folders = os.listdir(checkpoint_filepath)
                # best_model = np.argmin([float(folder.split('-')[1]) for folder in folders])
                model.load_weights(folder_path)
                pred=[]
                for i in test_generator:
                    pred.append(model.predict(i))
                pred=np.ravel(pred)
                pred = np.vstack(pred).T
                values.append(np.mean(np.abs(pred-test_truth)))
                print('p_e:', np.mean(np.abs(test_p_e-test_truth)))
                print('DeepCalo', np.mean(np.abs(pred-test_truth)))
                
            out_file = open(f"{folder_path}track_number_performance_{name}.json", "w") 
      
            performance['name'] = name
            performance['DeepCalo'] = str(np.mean(values))
            performance['error_DeepCalo'] = str(np.std(values))
            performance['values_DeepCalo'] = str(values)
            performance['p_e'] = str(np.mean(np.abs(test_p_e-test_truth)))
            print(values)
            json.dump(performance, out_file, indent = 6) 
            out_file.close() 
                    
    else:
        #%% '1621506539.476215', '1621517548.689952', 
        plt.figure(figsize=(17, 5))
        lw=3
        color = ['r', 'b', 'g', 'y', 'b']
        name = ['gamle', 'cov1d', 'T', '']
        for nr, test in enumerate([
                                    # '1626613559.5134408',
                                    # '1626624825.3969836',
                                    # '1626635591.5899222',
                                    '1626646032.200011',
                                    # '1626656547.5551395'
                                    ]):
            mypath = f'./models/{test}/*/*performance*'
            onlyfiles = glob(mypath)
            data =[]
            for i in onlyfiles:
                with open(i) as f:
                    data.append(json.load(f))
            name=[]
            MAE=[]
            MAE_error=[]
            for i in data:
                if i['name'] == None:
                    benchmark = float(i['DeepCalo'])
                else:
                    name.append(i['name'])
                    MAE.append(float(i['DeepCalo']))
                    MAE_error.append(np.std([float(i) for i in
                                             i['values_DeepCalo'].split('[')[-1].split(']')[0].split(', ')]))
            plt.errorbar([i.split('_')[-1] for i in name], np.array(MAE)-np.array([benchmark]*len(MAE)), MAE_error, fmt=color[nr]+'*', label='Permutation feature importance', lw=lw)
            # plt.plot([i.split('_')[-1] for i in name], np.ones(len(name))*benchmark, color[nr]+':', label='Benchmark: '+str(benchmark), lw=lw)
        plt.plot([i.split('_')[-1] for i in name], np.zeros(len(name)), '--', label='Zero line', lw=lw)
        plt.legend(fontsize=15, loc=2)
        plt.yticks(fontsize=15)
        plt.xticks(fontsize=20)
        plt.tight_layout()
